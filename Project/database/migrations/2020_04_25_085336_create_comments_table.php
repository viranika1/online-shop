<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCommentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('comments', function (Blueprint $table) {
            $table->id();
            $table->foreignId('productId')->comment('ایدی محصولی که کامنت گذاشته شده از جدول(products) ');
            $table->foreignId('userId')->comment('ایدی شخص کامنت گذاشته از جدول (users)');
            $table->foreignId('parentId')->comment('ایدی نظر پاسخ داده شده');
            $table->mediumText('comment')->comment('متن کامنت');
            $table->boolean('confirmComment')->comment('تایید کامنت');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('comments');
    }
}
