<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product_categories', function (Blueprint $table) {
            $table->id();
            $table->string("name")->comment("نام دسته");
            $table->string("slug")->unique()->comment("لینک دسته");
            $table->foreignId("parentId")->nullable()->comment("آیدی دسته پدر");
            $table->string("metaTitle")->nullable();
            $table->string("metaKeyword")->nullable();
            $table->mediumText("metaDescription")->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product_categories');
    }
}
