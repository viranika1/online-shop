<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddRelationshipsToUserDiscountCodesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('user_discount_codes', function (Blueprint $table) {
            $table->foreign("userId")->references("id")->on("users")->onDelete("CASCADE")->onUpdate("CASCADE");
            $table->foreign("discountCodeId")->references("id")->on("discount_codes")->onDelete("CASCADE")->onUpdate("CASCADE");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('user_discount_codes', function (Blueprint $table) {
            $table->dropForeign(["userId"]);
            $table->dropForeign(["discountCodeId"]);
        });
    }
}
