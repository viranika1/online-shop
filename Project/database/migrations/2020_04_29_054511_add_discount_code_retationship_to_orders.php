<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddDiscountCodeRetationshipToOrders extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('orders', function (Blueprint $table) {
            $table->foreign('discountCodeId')->references('id')->on('discount_codes')->onDelete('CASCADE')->onUpdate('CASCADE');
            $table->foreign('addressId')->references('id')->on('addresses')->onDelete('RESTRICT')->onUpdate('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('orders', function (Blueprint $table) {
            $table->dropForeign(["discountCodeID"]);
            $table->dropForeign(["addressId"]);
        });
    }
}
