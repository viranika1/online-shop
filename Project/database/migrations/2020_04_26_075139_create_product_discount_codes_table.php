<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductDiscountCodesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product_discount_codes', function (Blueprint $table) {
            $table->id();
            $table->foreignId("productId")->comment("آیدی محصول از جدول (products)");
            $table->foreignId("discountCodeId")->comment("آیدی کد تخفیف از جدول (discount_codes)");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product_discount_codes');
    }
}
