<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFestivalsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('festivals', function (Blueprint $table) {
            $table->id();
            $table->string('cover')->nullable();
            $table->string('name')->comment('نام جشنواره');
            $table->string('slug')->comment('لینک جشنواره');
            $table->timestamp('startDate')->nullable()->comment('تاریخ شروع جشنواره');
            $table->timestamp('stopDate')->nullable()->comment('تاریخ پایان جشنواره');
            $table->string("metaTitle")->nullable();
            $table->string("metaKeyword")->nullable();
            $table->mediumText("metaDescription")->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('festivals');
    }
}
