<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductPropertiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product_properties', function (Blueprint $table) {
            $table->id();
            $table->foreignId("productId")->comment("آیدی محصول از جدول (products)");
            $table->foreignId("propertyId")->comment("آیدی ویژگی و متغیر از جدول (properties)");
            $table->string("group")->nullable()->comment("گروه متغیر ها");
            $table->mediumText("value")->nullable()->comment("مقدار ویژگی برای محصول");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product_properties');
    }
}
