<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->string('avatar')->nullable();
            $table->string('name')->nullable()->comment('نام کاربر');
            $table->string('lastName')->nullable()->comment('نام خانوادگی کاربر');
            $table->string('email')->unique()->nullable()->comment('ایمیل کاربر' );
            $table->char('phone','11')->nullable()->comment('شماره تلفن کاربر');
            $table->char('nationalCode','10')->nullable()->comment('کد ملی کاربر');
            $table->string('password');
            $table->rememberToken();
            $table->char('code')->nullable()->comment('کد معرف');
            $table->foreignId('introducingId')->nullable()->comment('ایدی کد معرف');
            $table->set('level',['user','seller'])->default('user')->comment('انتخاب سطح کاربر یا فروشنده');
            $table->integer('score')->default('0')->comment('امتیاز کاربر');
            $table->boolean('newsletter')->default(0)->comment('تمایل عضویت در خبرنامه');
            $table->char('cardNumber','16')->nullable()->comment('شماره کارت بانکی');
            $table->timestamp('confirmPhone')->nullable()->comment('تایید شماره موبایل');
            $table->timestamp('email_verified_at')->nullable()->comment('تایید ایمیل کاربر');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
