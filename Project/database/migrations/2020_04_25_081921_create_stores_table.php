<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStoresTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('stores', function (Blueprint $table) {
            $table->id();
            $table->foreignId('userId')->comment('ایدی کاربر از جدول (USERS)');
            $table->string('cover')->nullable();
            $table->string('logo')->nullable();
            $table->string('name')->comment('نام فروشگاه');
            $table->string('location')->comment('موقعیت فروشگاه');
            $table->mediumText('description')->comment('توضیحات ');
            $table->string("metaTitle")->nullable();
            $table->string("metaKeyword")->nullable();
            $table->mediumText("metaDescription")->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('stores');
    }
}
