<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddRelationshipsToCommentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('comments', function (Blueprint $table) {
            $table->foreign("userId")->references("id")->on("users")->onDelete("CASCADE")->onUpdate("CASCADE");
            $table->foreign("productId")->references("id")->on("products")->onDelete("CASCADE")->onUpdate("CASCADE");
            $table->foreign("parentId")->references("id")->on("comments")->onDelete("CASCADE")->onUpdate("CASCADE");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('comments', function (Blueprint $table) {
            $table->dropForeign(["userId"]);
            $table->dropForeign(["productId"]);
            $table->dropForeign(["parentId"]);
        });
    }
}
