<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddRelationshipsToTableCategoryDiscountCodes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('category_discount_codes', function (Blueprint $table) {
            $table->foreign('categoryId')->references('id')->on('product_categories')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('discountCodeId')->references('id')->on('discount_codes')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('category_discount_codes', function (Blueprint $table) {
            $table->dropForeign(["categoryId"]);
            $table->dropForeign(["discountCodeId"]);
        });
    }
}
