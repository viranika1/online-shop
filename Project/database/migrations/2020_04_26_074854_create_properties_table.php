<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePropertiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('properties', function (Blueprint $table) {
            $table->id();
            $table->char("name",50)->comment("نام ویژگی");
            $table->text("value")->comment("مقدار ویژگی");
            $table->char("typeProperty",20)->nullable()->comment("نوع متغیر");
            $table->boolean("linked")->default(0)->comment("ویژگی دارای لینک باشد یا نه پیشفرض لینک ندارد");
            $table->string("slug")->nullable()->comment("لینک ویژگی در صورتی که قابلیت لینک شدن را داشته باشد");
            $table->boolean("variable")->default(0)->comment("ویژگی متغیر است یا خیر");
            $table->char("typeVariable",20)->nullable()->comment("نوع نمایش متغیر");
            $table->string("metaTitle")->nullable();
            $table->string("metaKeyword")->nullable();
            $table->mediumText("metaDescription")->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('properties');
    }
}
