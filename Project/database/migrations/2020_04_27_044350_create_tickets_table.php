<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTicketsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tickets', function (Blueprint $table) {
            $table->id();
            $table->string("title")->comment("عنوان تیکت");
            $table->longText("description")->comment("توضیحات تیکت");
            $table->foreignId("parentId")->comment("آیدی تیکت پاسخ داده شده");
            $table->foreignId("userId")->comment("آیدی کاربر ارسال کننده تیکت از جدول (users)");
            $table->foreignId("supportAdminID")->nullable()->comment("آیدی ادمین پشتیبان از جدول (admins)");
            $table->mediumText('files')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tickets');
    }
}
