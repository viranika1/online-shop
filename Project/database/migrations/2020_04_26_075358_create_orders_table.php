<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->id();
            $table->foreignId("userId")->comment("آیدی کاربر از جدول (users)");
            $table->string("condition")->comment("وضعیت سفارش");
            $table->timestamp("postDate")->nullable()->comment("تاریخ پست شده");
            $table->string("postalTrackingCode")->nullable()->comment("کد رهگیری پست");
            $table->integer("totalSellingPrices")->nullable()->comment("جمع قیمت های فروش در زمان پرداخت");
            $table->integer("totalBuyingPrices")->nullable()->comment("جمع قیمت های خرید در زمان پرداخت");
            $table->integer("totalAppliedPrices")->nullable()->comment("جمع قیمت های اعمال شده در زمان پرداخت");
            $table->integer("packagingCost")->nullable()->comment("هزینه بسته بندی");
            $table->integer("postage")->nullable()->comment("هزینه اصلی پست");
            $table->integer("postagePaid")->nullable()->comment("هزینه پست پرداخت شده توسط کاربر");
            $table->integer("totalWeight")->nullable()->comment("وزن کل سفارش");
            $table->mediumText("description")->nullable()->comment("توضحات سفارش");
            $table->foreignId("addressId")->nullable()->comment("آیدی آدرس سفارش از جدول (addresses)");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
