<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrderProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('order_products', function (Blueprint $table) {
            $table->id();
            $table->foreignId("orderId")->comment("آیدی سفارش از جدول (orders)");
            $table->foreignId("productId")->comment("آیدی محصول از جدول (products)");
            $table->foreignId("productDetailsId")->comment("آیدی متغیر انتخاب شده محصول از جدول (productDetails)");
            $table->integer("count")->comment("تعداد خرید");
            $table->integer("sellingPrice")->comment("قیمت فروش محصول در زمان پرداخت");
            $table->integer("buyingPrice")->comment("قیمت خرید محصول در زمان پرداخت");
            $table->string("appliedPriceType")->comment("نوع قیمت اعمال شده برای پرداخت");
            $table->integer("appliedPrice")->comment("قیمت اعمال شده برای پرداخت");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('order_products');
    }
}
