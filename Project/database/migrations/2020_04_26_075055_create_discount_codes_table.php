<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDiscountCodesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('discount_codes', function (Blueprint $table) {
            $table->id();
            $table->char("code",10)->unique()->comment("کد تخفیف");
            $table->set("type",["Percent","Fixed Amount","Free Post"])->comment("نوع کد تخفیف انواع");
            $table->integer("value")->nullable()->comment("مقدار تخفیف");
            $table->integer("minPrice")->nullable()->comment("حداقل مبلغ سبد برای اعمام کد تخفیف");
            $table->integer("maxPrice")->nullable()->comment("حداکثر مبلغ سبد برای اعمام کد تخفیف");
            $table->timestamp("startDate")->nullable()->comment("تاریخ شروع برای فعال شدن کد تخفیف");
            $table->timestamp("stopDate")->nullable()->comment("تاریخ پایان برای فعال شدن کد تخفیف");
            $table->integer("totalUse")->comment("حداکثر استفاده از کد تخفیف");
            $table->integer("score")->nullable()->comment("امتیاز لازم برای گرفتم کد تخفیف");
            $table->boolean("majorPrice")->default(0)->comment("قابل اعمال بر قیمت کلی");
            $table->boolean("sellPrice")->default(1)->comment("قابل اعمال بر قیمت اصلی");
            $table->boolean("festivalPrice")->default(0)->comment("قابل اعمال بر قیمت جشنواره");
            $table->boolean("specialPrice")->default(0)->comment("قابل اعمال بر قیمت ویژه");
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('discount_codes');
    }
}
