<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddRelationshipsToProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('products', function (Blueprint $table) {
            $table->foreign("categoryId")->references("id")->on("product_categories")->onDelete("RESTRICT")->onUpdate("CASCADE");
            $table->foreign("storeId")->references("id")->on("stores")->onDelete("SET NULL")->onUpdate("CASCADE");
            $table->foreign("registrarId")->references("id")->on("admins")->onDelete("RESTRICT")->onUpdate("CASCADE");
            $table->foreign("festivalId")->references("id")->on("festivals")->onDelete("SET NULL")->onUpdate("CASCADE");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('products', function (Blueprint $table) {
            $table->dropForeign(["storeId"]);
            $table->dropForeign(["categoryId"]);
            $table->dropForeign(["festivalId"]);
            $table->dropForeign(["registrarId"]);
        });
    }
}
