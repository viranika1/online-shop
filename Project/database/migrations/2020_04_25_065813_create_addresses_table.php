<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAddressesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('addresses', function (Blueprint $table) {
            $table->id();
            $table->foreignId('userId')->comment('ایدی کاربر از جدول (USERS)');;
            $table->char('postCode','10')->comment('کد پستی گیرنده');
            $table->char('mobile','11')->comment('تلفن همراه گیرنده');
            $table->char('phone','11')->nullable()->comment('تلفن ثابت گیرنده');
            $table->string('country')->nullable()->comment('کشور');
            $table->string('state')->comment('استان');
            $table->string('city')->comment('شهر');
            $table->mediumText('address')->comment('ادرس محل تحویل');
            $table->string('receiver')->comment('نام شخص گیرنده');
            $table->mediumText('description')->nullable()->comment('توضیحات اضافه');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('addresses');
    }
}
