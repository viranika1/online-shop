<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddRelationshipsToProductPropertiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('product_properties', function (Blueprint $table) {
            $table->foreign("productId")->references("id")->on("products")->onDelete("CASCADE")->onUpdate("CASCADE");
            $table->foreign("propertyId")->references("id")->on("properties")->onDelete("CASCADE")->onUpdate("CASCADE");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('product_properties', function (Blueprint $table) {
            $table->dropForeign(["productId"]);
            $table->dropForeign(["propertyId"]);
        });
    }
}
