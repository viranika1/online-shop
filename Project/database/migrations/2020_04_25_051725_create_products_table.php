<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->id();
            $table->string("name")->comment("نام محصول");
            $table->string("slug")->unique()->index()->comment("آدرس محصول انگلیسی بدون فاصله");
            $table->char("code",10)->unique()->comment("کد محصول انگلیسی");
            $table->mediumText("description")->comment("توضیحات کوتاه محصول");
            $table->longText("study")->comment("بررسی محصول");
			$table->mediumText('images')->nullable();
            $table->foreignId("categoryId")->default(0)->comment("آیدی دسته محصول از جدول (categories)");
            $table->foreignId("registrarId")->default(0)->comment("آیدی کاربر ادمین ثبت کننده محصول از جدول (admins)");
            $table->foreignId("storeId")->nullable()->comment("آیدی فروشگاه محصول از جدول (stores)");
            $table->foreignId("festivalId")->nullable()->comment("آیدی جشنواره از جدول (festivals)");
            $table->boolean("condition")->default(1)->comment("وضعیت محصول فعال یا غیر فعال");
            $table->timestamp("startDate")->nullable();
            $table->timestamp("stopDate")->nullable();
            $table->string("tags");
            $table->string("metaTitle")->nullable();
            $table->string("metaKeyword")->nullable();
            $table->mediumText("metaDescription")->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
