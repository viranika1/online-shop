<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserDiscountCodesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_discount_codes', function (Blueprint $table) {
            $table->id();
            $table->timestamps();
            $table->foreignId("userId")->comment("آیدی کاربر از جدول (users)");
            $table->foreignId("discountCodeId")->comment("آیدی کد تخفیف از جدول (discount_codes)");
            $table->integer("count")->comment("حداکثر تعداد استفاده از کد تخفیف");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_discount_codes');
    }
}
