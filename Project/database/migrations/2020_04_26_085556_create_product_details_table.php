<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product_details', function (Blueprint $table) {
            $table->id();
            $table->foreignId("productId")->comment("آیدی محصول از جدول (products)");
            $table->mediumText('images')->nullable();
            $table->char("variableMix",5)->comment("ترکیب آیدی متغیر ها");
            $table->integer("sellingPrice")->comment("قیمت فروش");
            $table->integer("buyingPrice")->comment("قیمت خرید");
            $table->integer("majorPrice")->nullable()->comment("قیمت کلی");
            $table->integer("majorCount")->nullable()->comment("حداقل تعداد برای محاسبه قیمت کلی");
            $table->integer("specialPrice")->nullable()->comment("قیمت ویژه");
            $table->timestamp("specialStartDate")->nullable()->comment("تاریخ شروع قیمت ویژه");
            $table->timestamp("specialStopDate")->nullable()->comment("تاریخ پایان قیمت ویژه");
            $table->integer("festivalPrice")->nullable()->comment("قیمت جشنواره");
            $table->boolean("available")->default(1)->comment("در دسترس بودن یا فعال بودن");
            $table->integer("inventory")->comment("موجودی کالا");
            $table->integer("length")->comment("طول محصول");
            $table->integer("width")->comment("عرض محصول");
            $table->integer("height")->comment("ارتفاع محصول");
            $table->integer("weight")->comment("وزن محصول");
            $table->boolean("order")->default(0)->comment("محصول قابل سفارش باشد در صورت موجود نبودن");
            $table->integer("orderTime")->nullable()->comment("حداکثر زمان برای آماده سازی سفارش");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product_details');
    }
}
