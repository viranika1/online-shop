<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCategoryDiscountCodesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('category_discount_codes', function (Blueprint $table) {
            $table->id();
            $table->foreignId("categoryId")->comment("آیدی دسته محصول از جدول (categories)");
            $table->foreignId("discountCodeId")->comment("آیدی کد تخفیف از جدول (discount_codes)");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('category_discount_codes');
    }
}
