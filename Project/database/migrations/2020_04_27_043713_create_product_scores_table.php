<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductScoresTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product_scores', function (Blueprint $table) {
            $table->id();
            $table->foreignId('productId')->comment('ایدی محصول امتیاز گرفته از جدول(product)');
            $table->foreignId('userId')->comment('ایدی کاربر امتیاز دهنده به محصول از جدول(user)');
            $table->integer('score')->comment('تعداد امتیازات');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product_scores');
    }
}
