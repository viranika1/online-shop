<?php

use Faker\Factory;
use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	\App\User::create([
    		'name' => 'Admin',
			'avatar' => 'http://store/Upload/Koala.jpg',
			'email' => 'admin@admin.com',
			'phone' => '09123456789',
			'nationalCode' => '1234567890',
			'password' => Hash::make('123456789'),
			'Code' => Factory::create()->bothify('User-*#*?#'),
			'score' => 0,
			'newsletter' => 0,
		]);
    }
}
