<?php

use Illuminate\Database\Seeder;

class PropertySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        \App\Property::create([
        	'name' => 'رنگ',
			'value' => 'آبی',
			'variable' => 1
		]);
		\App\Property::create([
			'name' => 'رنگ',
			'value' => 'قرمز',
			'variable' => 1
		]);
		\App\Property::create([
			'name' => 'جنس',
			'value' => 'چوبی'
		]);
		\App\Property::create([
			'name' => 'فلزی',
			'value' => 'چوبی'
		]);
    }
}
