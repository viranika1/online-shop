<?php

use Faker\Factory;
use Illuminate\Database\Seeder;

class ProductSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Product::create([
        	'name' => 'ماشین سواری چوبی دستساز',
			'slug' => 'car-livestock-good',
			'code' => Factory::create()->bothify('Pro-*#*?#'),
			'description' => Faker\Factory::create('fa_IR')->text,
			'study' => Faker\Factory::create('fa_IR')->text,
			'categoryId' => \App\ProductCategory::firstWhere('slug','=' , 'toys')->toArray()['id'],
            'condition' => '1',
            'tags'=>Faker\Factory::create('fa_IR')->word,
            'registrarId'=>'1',
		]);
    }
}
