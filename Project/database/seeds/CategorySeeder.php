<?php

use Illuminate\Database\Seeder;

class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\ProductCategory::create([
        	'name' => 'اسباب‌بازی و سرگرمی',
        	'slug' => 'toys',
		]);
    }
}
