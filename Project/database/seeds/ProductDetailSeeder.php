<?php

use Illuminate\Database\Seeder;

class ProductDetailSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\ProductDetail::create([
                'sellingPrice'=>'650000',
                'buyingPrice'=>'7000000',
                'majorPrice'=>'6000000',
                'majorCount'=>'5',
                'specialPrice'=>'500000',
                'available'=>'1',
                'productId'=>'1',
                'variableMix'=>'scd',
                'inventory'=>'20',
                'length'=>'60',
                'height'=>'50',
                'width'=>'40',
                'weight'=>'20'

        ]);
    }
}
