<?php

use Faker\Factory;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::group(['before' => 'auth','prefix' => '/admin','as'=>'admin.' , 'namespace' => 'Admin'], function () {
	Route::get('/' , 'DashboardController@index')->name('dashboard');
	Route::resource('categories' , 'CategoryController');
	Route::resource('properties' , 'PropertyController');
});




Route::get('/', 'HomeController@index')->name('home');


Auth::routes(['verify' => false]);


Route::name('profile.')->middleware(['auth'])->prefix('profile')->group(function (){
    Route::get('/' , 'App\UserController@profileIndex')->name('index');
    Route::patch('/avatar/update' , 'App\UserController@avatarUpdate')->name('avatar.update');
    Route::get('/edit' , 'App\UserController@edit')->name('edit');
    Route::patch('/edit/update' , 'App\UserController@updateUser')->name('updateUser');
    Route::get('/info' , 'App\UserController@profileInfo')->name('info');
    Route::resource('address' , 'App\AddressController');
    Route::get('orders' , 'App\UserOrderController@index')->name('orders');
    Route::get('wishlist' , 'App\WishListController@index')->name('wishlist');
    Route::get('like/{product}','App\UserController@ProductLiked')->name('like');
    Route::get('password' , 'App\UserController@showChangePassword')->name('change.password');
    Route::resource('store' , 'App\StoreController')->only(['create','show']);
});









Route::get('/test' , function (){

});
