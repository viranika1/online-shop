
<!-- cart-NOT-empty -->
<ul class="cart-tooltip text-center">
    <li class="product-sty1">
        <a href="#">
            <img  class="img-sty1" src="images/accessory/minicart01.jpg" alt="هفت سین">
            <h3 class="header-sty1">سرویس هفت سین طرح شیرین و فرهاد</h3>
        </a>
        <div class="price-product-sty1">
            <span class="price-sty1">2400</span>
            <span class="unitmoney"> تومان</span>
            <br>
            <span class="Lquantity-sty1">تعداد : </span>
            <span class="quantity-sty1">2</span>
        </div>
        <a class="remove-item" href="#">
            <i class="fal fa-trash-alt"></i>
        </a>
    </li>
    <hr>
    <div class="area-btn">
        <button type="button" class="btn btn-round btn-info">تسویه حساب</button>
        <button type="button" class="btn btn-round btn-success">سبدخرید</button>
    </div>
</ul>
<!-- cart-empty -->
<!--<ul class="tooltip-empty cart-tooltip text-center">
    <div class="img-empty">
            <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                 viewBox="0 0 84 133" style="enable-background:new 0 0 84 133;" xml:space="preserve">
                <filter  filterUnits="objectBoundingBox" height="250%" id="a" width="111.8%" x="-5.9%" y="-75%">
                    <feGaussianBlur  in="SourceGraphic" stdDeviation="1"></feGaussianBlur>
                </filter>
                <g transform="translate(-3 -8)">
                    <path style="fill:none;stroke:#616161;stroke-width:3;stroke-linecap:round;" d="M51.1,14.6L29.5,9.9c-3.4-0.7-6.8,1.1-8,4.3
                        L5.1,56.8c-1.2,3.2,0.1,6.9,3.1,8.6L47,87.6c1.4,0.8,3.3,0.3,4.1-1.1c0.1-0.1,0.1-0.3,0.2-0.4l24.1-62.7c0.6-1.5-0.2-3.3-1.7-3.9
                        c-0.1-0.1-0.3-0.1-0.4-0.1l-15-3.3"/>
                    <path style="fill:none;stroke:#616161;stroke-width:3;stroke-linecap:round;" d="M50.6,67.5l6.7,2.6c8.5,3.3,18-1,21.3-9.5
                        c3.3-8.5-1-18-9.5-21.3L55.4,34 M53.6,68.6l-3.5-1.3 M46.2,65.8l-2.6-1"/>
                    <g style="opacity:0.499;filter:url(#a);enable-background:new    ;">
                        <ellipse style="fill-rule:evenodd;clip-rule:evenodd;fill:#E0E0E0;" cx="46.5" cy="137" rx="25.5" ry="2"/>
                    </g>
                    <path style="fill:none;stroke:#616161;stroke-linecap:round;stroke-dasharray:3,3;" d="M60,80c17.3,3.8,30.8,36.9,6.5,30
                        c-12.1-3.4-3.2-19.2,6.8-10.7c4.2,3.6,4,10.8-0.6,21.6 M73.5,77c4.7,1.1,9,4.3,12.4,8.3"/>
                </g>
            </svg>

    </div>
    <p>سبد خرید شما خالی است</p>
</ul>-->
