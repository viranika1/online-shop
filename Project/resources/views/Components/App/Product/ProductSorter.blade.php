<div class="title-products d-inline">
    <h3 class="d-none d-lg-inline-block pro-title fs-18 dastnevis header-gradient text-center px-3 px-md-4 mb-0 ">دسته بندی براساس :</h3>
    <ul class="orderBy d-inline-block">
        <li class="d-inline-block">
            <a class="fs-12 clr-61 py-1 px-2" href="#">محبوب ترین</a>
        </li>
        <li class="d-inline-block">
            <a class="fs-12 clr-61 py-1 px-2" href="#">جدیدترین</a>
        </li>
        <li class="d-inline-block">
            <a class="fs-12 clr-61 py-1 px-2" href="#">پرفروش ترین</a>
        </li>
        <li class="d-inline-block">
            <a class="fs-12 clr-61 py-1 px-2 active" href="#">ارزان ترین</a>
        </li>
        <li class="d-inline-block">
            <a class="fs-12 clr-61 py-1 px-2" href="#">گران ترین</a>
        </li>
        <li class="d-inline-block">
            <a class="fs-12 clr-61 py-1 px-2" href="#">سریع ترین ارسال</a>
        </li>

    </ul>
    <hr class="under-title">
</div>