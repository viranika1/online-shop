<div class="sidebar">
    <div class="title-products text-center">
        <h3 class="pro-title fs-18 dastnevis header-gradient text-center d-inline-block px-3 px-md-4 mb-0 ">دسته بندی ها</h3>
        <hr class="under-title">
    </div>
    <div class="content-bio p-2 p-md-3 pt-4 bg-ff bx-sh text-right position-relative">
        <div class="form-check">
            <label class="form-check-label d-block mb-3 clr-75 fs-12">
                <input class="form-check-input" type="checkbox" value="">
                سفال
                <span class="form-check-sign">
													<span class="check"></span>
											</span>
            </label>
            <label class="form-check-label d-block mb-3 clr-75 fs-12">
                <input class="form-check-input" type="checkbox" value="">
                سرامیک
                <span class="form-check-sign">
													<span class="check"></span>
											</span>
            </label>
            <label class="form-check-label d-block mb-3 clr-75 fs-12">
                <input class="form-check-input" type="checkbox" value="">
                چرم دوزی
                <span class="form-check-sign">
												 <span class="check"></span>
										 </span>
            </label>
            <label class="form-check-label d-block mb-3 clr-75 fs-12">
                <input class="form-check-input" type="checkbox" value="">
                سوزن دوزی
                <span class="form-check-sign">
												 <span class="check"></span>
										 </span>
            </label>
        </div>
    </div>
</div>
