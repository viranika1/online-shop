@if($product !== 'test')
<div class="content-product">
    <div class="img-product">
        <a href="#">
{{--            <img src="{{$product->thumbnail->src}}" alt="{{$product->thumbnail->alt}}">--}}
        </a>
    </div>
    <div class="title-product px-2">
        <a href="#">
            <h3 class="iransans fs-16 clr-42 text-center">{{$product->name}}</h3>
            <h4 class="iransans fs-12 clr-61 text-center">کد: {{$product->code}}</h4>
        </a>
    </div>
    <div class="badge-product">
{{--        <span class="percentage fs-14 bg-red">{{$product->discount->percet}} %</span>--}}
    </div>
    <div class="wishlist-product">
        <a href="{{route('profile.like',['product'=>$product->id])}}">
            <span class="wishlist fs-14 clr-75 text-right">دوسش دارم</span>
            <i class="fal fa-heart clr-{{$product->liked() === true ? 'red' : 'f2' }} fs-20"></i>
        </a>
    </div>
    <div class="price-product px-2">
        @isset($product->discount)
            <del class="d-block text-center clr-b2 mb-1 fs-14">
                <span>{{$product->sellingPrice}}</span>
                <span class="unitmoney"> تومان</span>
            </del>
            <ins  class="d-block text-center clr-red fs-14">
                <span>{{$product->discount->price}}</span>
                <span class="unitmoney"> تومان</span>
            </ins>
        @else
            <ins class="d-block text-center clr-b2 mb-1 fs-14">
                <span>{{$product->sellingPrice}}</span>
                <span class="unitmoney"> تومان</span>
            </ins>
            <ins  class="d-block text-center clr-red fs-14">
                &nbsp;
            </ins>
        @endif
    </div>
    <div class="add-to-cart-product position-absolute">
        <a class="add-to-cart-link clr-ff bg-green rounded-circle" href="#">
            <i class="fal fa-shopping-cart clr-ff"></i>
        </a>
    </div>
</div>
@else
    <div class="content-product">
        <div class="img-product">
            <a href="#"><img src="images/accessory/04.png" alt=""></a>
        </div>
        <div class="title-product px-2">
            <a href="#">
                <h3 class="iransans fs-16 clr-42 text-center">کیف مردانه بته چقه ای</h3>
                <h4 class="iransans fs-12 clr-61 text-center">کد 235126</h4>
            </a>
        </div>
        <div class="badge-product">
            <span class="percentage fs-14 bg-red">20 %</span>
        </div>
        <div class="wishlist-product">
            <a href="#">
                <span class="wishlist fs-14 clr-75 text-right">دوسش دارم</span>
                <i class="fal fa-heart clr-red fs-20"></i>
            </a>
        </div>
        <div class="price-product px-2">
            <del class="d-block text-center clr-b2 mb-1 fs-14">
                <span>15000</span>
                <span class="unitmoney">
											تومان
										</span>
            </del>
            <ins  class="d-block text-center clr-red fs-14">
                <span>13000</span>
                <span class="unitmoney">
											تومان
										</span>
            </ins>
        </div>
        <div class="add-to-cart-product position-absolute">
            <a class="add-to-cart-link clr-ff bg-green rounded-circle" href="#">
                <i class="fal fa-shopping-cart clr-ff"></i>
            </a>
        </div>
    </div>
@endif
