<div class="sidebar">
    <div class="title-products text-center">
        <h3 class="pro-title fs-18 dastnevis header-gradient text-center d-inline-block px-3 px-md-4 mb-0 ">محدوده قیمت</h3>
        <hr class="under-title">
    </div>
    <div class="content-bio p-2 p-md-3 pt-4 bg-ff bx-sh text-right position-relative">
        <div id="sliderDouble" class="slider slider-rose"></div>
        <div class="row amount-slider text-center clr-61 fs-14">
            <div class="col-12 col-lg-6 border-left ">
                <p class="fs-13">	از</p>
                <span>23000</span>
                <p class="fs-13">	تومان</p>
            </div>
            <div class="col-12 col-lg-6">
                <p class="fs-13">تا</p>
                <span>123000</span>
                <p class="fs-13">تومان</p>
            </div>
        </div>
        <div class="btn-filter text-center mt-4">
            <button class="btn btn-info btn-round dastnevis btn-circle position-relative fs-16">
                <i class="fal fa-filter clr-gold fs-18 position-relative"></i>
                اعمال فیلتر
            </button>
        </div>
    </div>
</div>
