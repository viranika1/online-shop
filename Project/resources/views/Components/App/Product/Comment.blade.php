<div class="form-group bmd-form-group text-right">
    <label class="bmd-label-floating mr-2">نام و نام خانوادگی</label>
    <input type="text" class="form-control position-relative ">
</div>
<div class="form-group bmd-form-group text-right">
    <label class="bmd-label-floating mr-2">پست الکترونیک</label>
    <input type="email" class="form-control position-relative ">
</div>
<div class="form-group bmd-form-group text-right">
    <label class="bmd-label-floating mr-2">متن دیدگاه</label>
    <textarea class="form-control position-relative " rows="8"></textarea>
</div>
<div class="btn-review text-center">
    <button class="btn btn-secondary btn-round dastnevis btn-circle position-relative fs-16">
        <i class="fal fa-comment-smile clr-gold fs-20 position-relative"></i>
        ارسال دیدگاه
    </button>
</div>