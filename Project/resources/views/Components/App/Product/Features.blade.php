
<h3 class="dastnevis text-right ">
    مشخصات کلی
</h3>
<ul>
    <li class="row text-right mt-1">
        <div class="col-3 pl-1">
            <div class="bg-ee px-2 py-3">
                <span>عنوان ویژگی اول</span>
            </div>
        </div>
        <div class="col-9 pr-0">
            <div class="bg-f5 px-2 py-3">
                <span>زیبا و جادار</span>
            </div>
        </div>
    </li>
    <li class="row text-right mt-1">
        <div class="col-3 pl-1">
            <div class="bg-ee px-2 py-3">
                <span>عنوان ویژگی اول</span>
            </div>
        </div>
        <div class="col-9 pr-0">
            <div class="bg-f5 px-2 py-3">
                <span>زیبا و جادار</span>
            </div>
        </div>
    </li>
    <li class="row text-right mt-1">
        <div class="col-3 pl-1">
            <div class="bg-ee px-2 py-3">
                <span>عنوان ویژگی اول</span>
            </div>
        </div>
        <div class="col-9 pr-0">
            <div class="bg-f5 px-2 py-3">
                <span>زیبا و جادار</span>
            </div>
        </div>
    </li>
    <li class="row text-right mt-1">
        <div class="col-3 pl-1">
            <div class="bg-ee px-2 py-3">
                <span>عنوان ویژگی اول</span>
            </div>
        </div>
        <div class="col-9 pr-0">
            <div class="bg-f5 px-2 py-3">
                <span>زیبا و جادار</span>
            </div>
        </div>
    </li>
    <li class="row text-right mt-1">
        <div class="col-3 pl-1">
            <div class="bg-ee px-2 py-3">
                <span>عنوان ویژگی اول</span>
            </div>
        </div>
        <div class="col-9 pr-0">
            <div class="bg-f5 px-2 py-3">
                <span>زیبا و جادار</span>
            </div>
        </div>
    </li>
</ul>
<h3 class="dastnevis text-right ">
    سایر مشخصات
</h3>
<ul>
    <li class="row text-right mt-1">
        <div class="col-3 pl-1">
            <div class="bg-ee px-2 py-3">
                <span>عنوان ویژگی اول</span>
            </div>
        </div>
        <div class="col-9 pr-0">
            <div class="bg-f5 px-2 py-3">
                <span>زیبا و جادار</span>
            </div>
        </div>
    </li>
    <li class="row text-right mt-1">
        <div class="col-3 pl-1">
            <div class="bg-ee px-2 py-3">
                <span>عنوان ویژگی اول</span>
            </div>
        </div>
        <div class="col-9 pr-0">
            <div class="bg-f5 px-2 py-3">
                <span>زیبا و جادار</span>
            </div>
        </div>
    </li>
    <li class="row text-right mt-1">
        <div class="col-3 pl-1">
            <div class="bg-ee px-2 py-3">
                <span>عنوان ویژگی اول</span>
            </div>
        </div>
        <div class="col-9 pr-0">
            <div class="bg-f5 px-2 py-3">
                <span>زیبا و جادار</span>
            </div>
        </div>
    </li>
    <li class="row text-right mt-1">
        <div class="col-3 pl-1">
            <div class="bg-ee px-2 py-3">
                <span>عنوان ویژگی اول</span>
            </div>
        </div>
        <div class="col-9 pr-0">
            <div class="bg-f5 px-2 py-3">
                <span>زیبا و جادار</span>
            </div>
        </div>
    </li>
    <li class="row text-right mt-1">
        <div class="col-3 pl-1">
            <div class="bg-ee px-2 py-3">
                <span>عنوان ویژگی اول</span>
            </div>
        </div>
        <div class="col-9 pr-0">
            <div class="bg-f5 px-2 py-3 mb-1">
                <span>زیبا و جادار</span>
            </div>
            <div class="bg-f5 px-2 py-3 mb-1">
                <span>زیبا و جادار</span>
            </div>
            <div class="bg-f5 px-2 py-3 mb-1">
                <span>زیبا و جادار</span>
            </div>
        </div>
    </li>
</ul>