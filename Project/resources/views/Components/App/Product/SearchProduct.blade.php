<div class="sidebar">
    <div class="title-products text-center">
        <h3 class="pro-title fs-18 dastnevis header-gradient text-center d-inline-block px-3 px-md-4 mb-0 ">جستجوی محصول</h3>
        <hr class="under-title">
    </div>
    <div class="content-bio p-2 p-md-3 pt-4 bg-ff bx-sh text-right position-relative">
        <div class="input-group ">
            <div class="form-group bmd-form-group w-100">
                <label class="bmd-label-floating">دنبال چی می گردی؟</label>
                <input type="search" class="form-control">
            </div>
        </div>
        <div class="btn-review text-center mt-4">
            <button class="btn btn-secondary btn-round dastnevis btn-circle position-relative fs-16">
                <i class="fal fa-search clr-gold fs-20 position-relative"></i>
                جستجو کن
            </button>
        </div>
    </div>
</div>
