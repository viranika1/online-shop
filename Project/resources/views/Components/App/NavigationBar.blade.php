<section class="container-fluid header bg-f5 py-3 d-none d-lg-block">
    <div class="container">
        <div class="row">
            <div class="col-12 mainmenu-area text-right">
                <ul class="mainmenu ">
                    <li class="menu-items">
                        <a class="clr-26 fs-18 px-2" href="#">زیورآلات و اکسسوری</a>
                    </li>
                    <li class="menu-items">
                        <a class="clr-26 fs-18 px-2" href="#">خانه و آشپزخانه</a>
                    </li>
                    <li class="menu-items">
                        <a class="clr-26 fs-18 px-2" href="#">مد و پوشاک</a>
                        <div class="sub-menu container" style="background : url(images/accessory/mm-01.png) #fff ;background-repeat: no-repeat;background-position: left bottom;background-size: contain;" >
                            <div class="row">
                                <div class="col-9 sub-col">
                                    <div class="sub-items ">
                                        <ul class="row px-3">
                                            <li class="col-3 sub-menu-list">
                                                <ul>
                                                    <li>
                                                        <h6 class="sub-menu-title">
                                                            <i class="fal fa-atom"></i>
                                                            لباس زنانه
                                                        </h6>
                                                    </li>
                                                    <li>
                                                        <a href="">
                                                            شال و روسری
                                                        </a>
                                                    </li>
                                                    <li>
                                                        <a href="">
                                                            بلوز
                                                        </a>
                                                    </li>
                                                    <li>
                                                        <a href="">
                                                            پیراهن/پیراهن شلوار
                                                        </a>
                                                    </li>
                                                    <li>
                                                        <a href="">
                                                            تی شرت
                                                        </a>
                                                    </li>
                                                    <li>
                                                        <a href="">
                                                            دامن
                                                        </a>
                                                    </li>
                                                    <li>
                                                        <a href="">
                                                            شلوار و شلوارک
                                                        </a>
                                                    </li>
                                                </ul>
                                            </li>
                                            <li class="col-3 sub-menu-list">
                                                <ul>
                                                    <li>
                                                        <h6 class="sub-menu-title">
                                                            <i class="fal fa-bullhorn"></i>
                                                            کیف و کوله پشتی زنانه
                                                        </h6>
                                                    </li>
                                                    <li>
                                                        <a href="">
                                                            ساک خرید
                                                        </a>
                                                    </li>
                                                    <li>
                                                        <a href="">
                                                            کیف
                                                        </a>
                                                    </li>
                                                    <li>
                                                        <a href="">
                                                            کیف پول و کارت
                                                        </a>
                                                    </li>
                                                    <li>
                                                        <h6 class="sub-menu-title">
                                                            <i class="fal fa-axe"></i>
                                                            کودکان
                                                        </h6>
                                                    </li>
                                                    <li>
                                                        <a href="">
                                                            مد و پوشاک
                                                        </a>
                                                    </li>
                                                </ul>
                                            </li>
                                            <li class="col-3 sub-menu-list">
                                                <ul>
                                                    <li>
                                                        <h6 class="sub-menu-title">
                                                            <i class="fal fa-balance-scale"></i>
                                                            کف و کوله پشتی مردانه
                                                        </h6>
                                                    </li>
                                                    <li>
                                                        <a href="">
                                                            کیف
                                                        </a>
                                                    </li>
                                                    <li>
                                                        <a href="">
                                                            کیف پول و کارت
                                                        </a>
                                                    </li>
                                                    <li>
                                                        <a href="">
                                                            ساک خرید
                                                        </a>
                                                    </li>
                                                    <li>
                                                        <h6 class="sub-menu-title">
                                                            <i class="fal fa-axe"></i>
                                                            لباس مردانه
                                                        </h6>
                                                    </li>
                                                    <li>
                                                        <a href="">
                                                            به زودی ...
                                                        </a>
                                                    </li>
                                                </ul>
                                            </li>
                                            <li class="col-3 sub-menu-list">
                                                <ul>
                                                    <li>
                                                        <h6 class="sub-menu-title">
                                                            <i class="fal fa-acorn"></i>
                                                            کفش و صندل زنانه
                                                        </h6>
                                                    </li>
                                                    <li>
                                                        <a href="">
                                                            به زودی ...
                                                        </a>
                                                    </li>
                                                    <li>
                                                        <h6 class="sub-menu-title">
                                                            <i class="fal fa-alarm-clock"></i>
                                                            کفش و صندل مردانه
                                                        </h6>
                                                    </li>
                                                    <li>
                                                        <a href="">
                                                            به زودی ...
                                                        </a>
                                                    </li>
                                                </ul>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="col-3 sub-col">
                                    <div class="sub-mod">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </li>
                    <li class="menu-items">
                        <a class="clr-26 fs-18 px-2" href="#">اسباب بازی و سرگرمی</a>
                    </li>
                    <li class="menu-items">
                        <a class="clr-26 fs-18 px-2" href="#">نقاشی و طراحی</a>
                    </li>
                    <li class="menu-items">
                        <a class="clr-26 fs-18 px-2" href="#">عتیقه جات</a>
                    </li>
                    <li class="menu-items">
                        <a class="clr-26 fs-18 px-2" href="#">مواداولیه وابزار</a>
                    </li>
                    <a class="p-2 p-md-0 float-left" href="#">
                        <i class="clr-red fs-25 fal fa-gift" data-toggle="tooltip" data-placement="bottom" title="دنبال خرید کادویی ؟ "></i>
                    </a>
                </ul>
            </div>
        </div>
    </div>
</section>
<section class="container-fluid header bg-f5 py-3 px-2 d-block d-lg-none">
    <div class="mainmenu-mobile">
        <div class="owl-carousel owl-theme">
            <div class="item">
                <a class="btn btn-primary btn-round" href="#">زیورآلات و اکسسوری</a>
            </div>
            <div class="item">
                <a class="btn btn-primary btn-round" href="#">زیورآلات و اکسسوری</a>
            </div>
            <div class="item">
                <a class="btn btn-primary btn-round" href="#">زیورآلات و اکسسوری</a>
            </div>
            <div class="item">
                <a class="btn btn-primary btn-round" href="#">زیورآلات و اکسسوری</a>
            </div>
            <div class="item">
                <a class="btn btn-primary btn-round" href="#">زیورآلات و اکسسوری</a>
            </div>
            <div class="item">
                <a class="btn btn-primary btn-round" href="#">زیورآلات و اکسسوری</a>
            </div>
        </div>
    </div>
</section>
