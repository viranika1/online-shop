<section class="container-fluid row-category">
    <div class="container">
        <h3 class="header-gradient fs-25 text-center dastnevis mb-5">لذت انتخاب دست ساخته های ایرانی در :</h3>
        <div class="row">
            <div class="col-lg-6 mb-4 content-category">
                <div class="sty-cat bx-sh bg-ff iransans">
                    <a href="#">
                        <h3 class="title-cat header-gradient iransans pr-5 fs-20 mt-1  mb-4">چیدمان متفاوت هفت سین</h3>
                        <h4 class="subtitle-cat clr-42 iransans fs-14 pr-5">سرویس سرامیک و سفال</h4>
                        <img class="img-cat" src="images/accessory/01.png" alt="">
                    </a>
                </div>
            </div>
            <div class="col-lg-6 mb-4 content-category">
                <div class="sty-cat bx-sh bg-ff iransans">
                    <a href="#">
                        <h3 class="title-cat header-gradient  iransans pr-5 fs-20 mt-1 mb-4">چیدمان متفاوت هفت سین</h3>
                        <h4 class="subtitle-cat clr-42 iransans fs-14 pr-5">سرویس سرامیک و سفال</h4>
                        <img class="img-cat" src="images/accessory/02.png" alt="">
                    </a>
                </div>
            </div>
            <div class="col-lg-6 mb-4 content-category">
                <div class="sty-cat bx-sh bg-ff iransans">
                    <a href="#">
                        <h3 class="title-cat header-gradient iransans pr-5 fs-20 mt-1 mb-4">چیدمان متفاوت هفت سین</h3>
                        <h4 class="subtitle-cat clr-42 iransans fs-14 pr-5">سرویس سرامیک و سفال</h4>
                        <img class="img-cat" src="images/accessory/03.png" alt="">
                    </a>
                </div>
            </div>
            <div class="col-lg-6 mb-4 content-category ">
                <div class="sty-cat bx-sh bg-ff iransans">
                    <a href="#">
                        <h3 class="title-cat header-gradient  iransans pr-5 fs-20 mt-1 mb-4">چیدمان متفاوت هفت سین</h3>
                        <h4 class="subtitle-cat clr-42 iransans fs-14 pr-5">سرویس سرامیک و سفال</h4>
                        <img class="img-cat" src="images/accessory/01.png" alt="">
                    </a>
                </div>
            </div>
            <div class="col-lg-6 mb-4 content-category">
                <div class="sty-cat bx-sh bg-ff iransans">
                    <a href="#">
                        <h3 class="title-cat header-gradient  iransans pr-5 fs-20 mt-1 mb-4">چیدمان متفاوت هفت سین</h3>
                        <h4 class="subtitle-cat clr-42 iransans fs-14 pr-5">سرویس سرامیک و سفال</h4>
                        <img class="img-cat" src="images/accessory/02.png" alt="">
                    </a>
                </div>
            </div>
            <div class="col-lg-6 mb-4 content-category">
                <div class="sty-cat bx-sh bg-ff iransans">
                    <a href="#">
                        <h3 class="title-cat header-gradient iransans pr-5 fs-20 mt-1 mb-4">چیدمان متفاوت هفت سین</h3>
                        <h4 class="subtitle-cat clr-42 iransans fs-14 pr-5">سرویس سرامیک و سفال</h4>
                        <img class="img-cat" src="images/accessory/03.png" alt="">
                    </a>
                </div>
            </div>
        </div>
    </div>
</section>