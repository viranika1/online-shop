<section class="container-fluid row-slider pt-0 pb-4">
    <div class="container">
        <div class="row">
            <div class="col-12 col-lg-3 banner-slider ">
                <a href="#">
                    <img class="bx-sh d-none d-lg-block" src="images/honarchiplus.png" alt="">
                    <img class="bx-sh d-block d-lg-none mb-3" src="images/honarchiplus-m.png" alt="">
                </a>
            </div>
            <div class="col-12 col-lg-9 Ho-slider">
                <div class="owl-carousel owl-theme bx-sh">
                    <div class="item">
                        <a href="#"><img src="images/accessory/slider03.jpg" alt=""></a>
                    </div>
                    <div class="item">
                        <a href="#"><img src="images/accessory/slider02.jpg" alt=""></a>
                    </div>
                    <div class="item">
                        <a href="#"><img src="images/accessory/slider04.jpg" alt=""></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>