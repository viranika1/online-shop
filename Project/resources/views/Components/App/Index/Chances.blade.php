<div class="col-chanse bx-sh pt-5 pb-4">
    <div class="row m-0">
        <div class="d-none d-lg-block col-lg-3 tabnav">
            <ul class="nav nav-pills nav-pills-rose flex-column p-0">
                <li class="nav-item"><a class="nav-link active" href="#tab1" data-toggle="tab">کوله شولبا گوئش</a></li>
                <li class="nav-item"><a class="nav-link" href="#tab2" data-toggle="tab">کیف پول ترکیبی بته جقه</a></li>
                <li class="nav-item"><a class="nav-link" href="#tab3" data-toggle="tab">کیف پول زنانه بته جقه</a></li>
                <li class="nav-item"><a class="nav-link" href="#tab4" data-toggle="tab">کیف پول کتی مردانه</a></li>
                <li class="nav-item"><a class="nav-link" href="#tab5" data-toggle="tab">کیف دوشی دخترانه</a></li>
            </ul>
            <button class="btn btn-danger btn-round btn-chanse btn-circle position-relative fs-16">
                <i class="fal fa-arrow-left clr-red fs-20 position-relative"></i>
                مشاهده ی همه ی شانس ها
            </button>
        </div>
        <div class="col-lg-9 tabcontent">
            <h3 class="clr-b2 fs-20">شانس های واقعی</h3>
            <div class="tab-content text-right">
                <div class="tab-pane active" id="tab1">
                    <div class="row prouct-sty2">
                        <div class="col-6">
                            <a class="header-sty2" href="#">
                                <h3 class="mb-0 header-gradient">کوله شولبا گوئش</h3>
                            </a>
                            <div class="price-sty2">
                                <del class="d-block text-left clr-b2 mb-2 px-5">
                                    <span>15000</span>
                                    <span class="unitmoney">
																	تومان
																</span>
                                </del>
                                <ins class="d-block text-left clr-red px-5">
                                    <span>13000</span>
                                    <span class="unitmoney">
																	تومان
																</span>
                                </ins>
                            </div>

                        </div>
                        <div class="col-6">
                            <div class="badge-sty2">
                                <span class="percentage fs-14">20 %</span>
                                <span class="lable-percentage">تخفیف</span>
                            </div>
                            <a class="thumnail-sty2" href="#">
                                <img src="images/accessory/01.png" alt="">
                            </a>
                        </div>
                    </div>
                </div>
                <div class="tab-pane" id="tab2">
                    <div class="row prouct-sty2">
                        <div class="col-6">
                            <a class="header-sty2" href="#">
                                <h3 class="mb-0 header-gradient">کیف پول ترکیبی بته جقه</h3>
                            </a>
                            <div class="price-sty2">
                                <del class="d-block text-left clr-b2 mb-2 px-5">
                                    <span>15000</span>
                                    <span class="unitmoney">
																	تومان
																</span>
                                </del>
                                <ins class="d-block text-left clr-red px-5">
                                    <span>13000</span>
                                    <span class="unitmoney">
																	تومان
																</span>
                                </ins>
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="badge-sty2">
                                <span class="percentage fs-14">20 %</span>
                                <span class="lable-percentage">تخفیف</span>
                            </div>
                            <a class="thumnail-sty2" href="#">
                                <img src="images/accessory/01.png" alt="">
                            </a>
                        </div>
                    </div>
                </div>
                <div class="tab-pane" id="tab3">
                    <div class="row prouct-sty2">
                        <div class="col-6">
                            <a class="header-sty2" href="#">
                                <h3 class="mb-0 header-gradient">کوله شولبا گوئش</h3>
                            </a>
                            <div class="price-sty2">
                                <del class="d-block text-center clr-b2 mb-2 pr-5">
                                    <span>15000</span>
                                    <span class="unitmoney">
																	تومان
																</span>
                                </del>
                                <ins class="d-block text-center clr-red pr-5">
                                    <span>13000</span>
                                    <span class="unitmoney">
																	تومان
																</span>
                                </ins>
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="badge-sty2">
                                <span class="percentage fs-14">20 %</span>
                                <span class="lable-percentage">تخفیف</span>
                            </div>
                            <a class="thumnail-sty2" href="#">
                                <img src="images/accessory/01.png" alt="">
                            </a>
                        </div>
                    </div>
                </div>
                <div class="tab-pane" id="tab4">
                    <div class="row prouct-sty2">
                        <div class="col-6">
                            <a class="header-sty2" href="#">
                                <h3 class="mb-0 header-gradient">کوله شولبا گوئش</h3>
                            </a>
                            <div class="price-sty2">
                                <del class="d-block text-center clr-b2 mb-2 pr-5">
                                    <span>15000</span>
                                    <span class="unitmoney">
																	تومان
																</span>
                                </del>
                                <ins class="d-block text-center clr-red pr-5">
                                    <span>13000</span>
                                    <span class="unitmoney">
																	تومان
																</span>
                                </ins>
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="badge-sty2">
                                <span class="percentage fs-14">20 %</span>
                                <span class="lable-percentage">تخفیف</span>
                            </div>
                            <a class="thumnail-sty2" href="#">
                                <img src="images/accessory/01.png" alt="">
                            </a>
                        </div>
                    </div>
                </div>
                <div class="tab-pane" id="tab5">
                    <div class="row prouct-sty2">
                        <div class="col-6">
                            <a class="header-sty2" href="#">
                                <h3 class="mb-0 header-gradient">کوله شولبا گوئش</h3>
                            </a>
                            <div class="price-sty2">
                                <del class="d-block text-left clr-b2 mb-2 px-5">
                                    <span>15000</span>
                                    <span class="unitmoney"> تومان</span>
                                </del>
                                <ins class="d-block text-left clr-red px-5">
                                    <span>13000</span>
                                    <span class="unitmoney"> تومان</span>
                                </ins>
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="badge-sty2">
                                <span class="percentage fs-14">20 %</span>
                                <span class="lable-percentage">تخفیف</span>
                            </div>
                            <a class="thumnail-sty2" href="#">
                                <img src="images/accessory/01.png" alt="">
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            <button class="d-block d-lg-none btn btn-danger btn-round btn-chanse btn-circle position-relative fs-16">
                <i class="fal fa-arrow-left clr-red fs-20 position-relative"></i>
                مشاهده ی همه ی شانس ها
            </button>
        </div>
    </div>
</div>