<section class="container-fluid row-services pb-5">
    <div class="container">
        <div class="row pb-1 m-0 bg-ff bx-sh">
            <div class="col-6 col-lg-3 service">
                <figure class="content-service">
                    <img src="images/uni-product.svg" alt="">
                    <h4 class="fs-16 p-2 text-center">محصولات خاص و منحصر به فرد</h4>
                </figure>
            </div>
            <div class="col-6 col-lg-3 service">
                <figure class="content-service">
                    <img src="images/support24.svg" alt="">
                    <h4 class="fs-16 p-2 text-center">پشتیبان تلفنی و خرید امن</h4>
                </figure>
            </div>
            <div class="col-6 col-lg-3 service">
                <figure class="content-service">
                    <img src="images/handicraft.svg" alt="">
                    <h4 class="fs-16 p-2 text-center">کالاهای دست سازه و اصیل</h4>
                </figure>
            </div>
            <div class="col-6 col-lg-3 service">
                <figure class="content-service">
                    <img src="images/iranian.svg" alt="">
                    <h4 class="fs-16 p-2 text-center">کمک به رونق اقتصادی کشور</h4>
                </figure>
            </div>
        </div>
    </div>
</section>