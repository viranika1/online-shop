<div class="row">
    <div class="col-6 col-lg-6 mb-4">
        <a href="#">
            <div class="content-gift bx-sh bg-ff p-3">
                <div class="gift-img text-right">
                    <img src="images/01.svg" alt="">
                </div>
                <div class="gift-title position-absolute text-center">
                    <h3 class="dastnevis fs-18 clr-75">برای کودکان</h3>
                </div>
            </div>
        </a>
    </div>
    <div class="col-6 col-lg-6 mb-4">
        <a href="#">
            <div class="content-gift bx-sh bg-ff p-3">
                <div class="gift-img text-right">
                    <img src="images/04.svg" alt="">
                </div>
                <div class="gift-title position-absolute text-center">
                    <h3 class="dastnevis fs-18 clr-75">برای آقایان</h3>
                </div>
            </div>
        </a>
    </div>
    <div class="col-6 col-lg-6 mb-4">
        <a href="#">
            <div class="content-gift bx-sh bg-ff p-3">
                <div class="gift-img text-right">
                    <img src="images/02.svg" alt="">
                </div>
                <div class="gift-title position-absolute text-center">
                    <h3 class="dastnevis fs-18 clr-75">برای خانم ها</h3>
                </div>
            </div>
        </a>
    </div>
    <div class="col-6 col-lg-6 mb-4">
        <a href="#">
            <div class="content-gift bx-sh bg-ff p-3">
                <div class="gift-img text-right">
                    <img src="images/03.svg" alt="">
                </div>
                <div class="gift-title position-absolute text-center">
                    <h3 class="dastnevis fs-18 clr-75">برای سالکرد</h3>
                </div>
            </div>
        </a>
    </div>
</div>
            