<?php
$user = Auth::user();
?>

<div class="profile-box p-info-box bg-ff bx-sh">
    <div class="profile-image px-3 pt-5 pb-2">
        <div class="p-img">
            <img id="avatar" src="{{$user->avatar}}" alt="{{auth()->user()->name}} {{auth()->user()->lastName}}">
            <a id="selectAvatar">
                <button class="bg-ff p-1">
                    <i class="fal fa-edit"></i>
                </button>
            </a>
            <script>
                var fmSetLink = function ($url) {
                    let xhttp = new XMLHttpRequest();
                    xhttp.onreadystatechange = function() {
                        if (this.readyState == 4 && this.status == 200) {
                            document.getElementById("avatar").setAttribute('src' , $url);
                        }
                    };
                    xhttp.open("POST" , "{{route('profile.avatar.update')}}" ,true);
                    xhttp.setRequestHeader("X-CSRF-TOKEN" , '{{csrf_token()}}');
                    xhttp.setRequestHeader("Content-Type", "application/json");
                    xhttp.send(JSON.stringify({urlAvatar:$url,_method:'PATCH'}));
                }
                document.addEventListener("DOMContentLoaded", function() {
                    document.getElementById('selectAvatar').addEventListener('click', (event) => {
                        event.preventDefault();
                        window.open('/file-manager/fm-button', 'fm', 'width=900,height=600');
                    });
                });
            </script>
        </div>
        <p class="dastnevis fs-18">
            {{$user->name}} {{$user->lastName}}
        </p>
    </div>
    <div class="profile-pe py-3">
        <div class="pass-change pe-box">
            <a href="{{route('profile.change.password')}}">
                <i class="fal fa-lock-alt"></i>
                <p>
                    تغییر رمز عبور
                </p>
            </a>
        </div>
        <div class="exit-profile pe-box">
            <form action="{{route('logout')}}" method="post">
                @csrf
                <button type="submit" style="display: contents">
                    <a href="{{route('logout')}}">
                        <i class="fal fa-sign-out-alt"></i>
                        <p>
                            خروج از سیستم
                        </p>
                    </a>
                </button>
            </form>
        </div>
    </div>
</div>
