<div class="profile-box">
    <div class="title-products text-right">
        <h3 class="pro-title fs-18 dastnevis header-gradient d-inline-block px-4 px-md-4 mb-0 pb-2">لیست آخرین علاقه مندی ها</h3>
        <hr class="under-title">
    </div>
    <div class="pbox p-list-box bg-ff bx-sh px-3 py-3">
        <div class="row text-center">
            <div class="col-lg-6 col-md-12 col-sm-12 my-1 my-md-0 my-lg-3">
                <a href="">
                    <div class="wl-img bx-sh p-2 ml-3">
                        <img src="images/accessory/01.png">
                    </div>
                    <div class="wl-txt text-right">
                        <p class="wl-title clr-42">
                            عنوان تستی برای محصول
                        </p>
                        <p class="wl-price clr-red">
                            1198000 تومان
                        </p>
                    </div>
                </a>
                <div class="wl-btn">
                    <button class="clr-75">
                        <i class="fal fa-trash-alt"></i>
                    </button>
                </div>
            </div>
            <div class="col-lg-6 col-md-12 col-sm-12 my-1 my-md-0 my-lg-3">
                <a href="">
                    <div class="wl-img bx-sh p-2 ml-3">
                        <img src="images/accessory/01.png">
                    </div>
                    <div class="wl-txt text-right">
                        <p class="wl-title clr-42">
                            عنوان تستی برای محصول
                        </p>
                        <p class="wl-price clr-red">
                            1198000 تومان
                        </p>
                    </div>
                </a>
                <div class="wl-btn">
                    <button class="clr-75">
                        <i class="fal fa-trash-alt"></i>
                    </button>
                </div>
            </div>
            <div class="col-lg-6 col-md-12 col-sm-12 my-1 my-md-0 my-lg-3">
                <a href="">
                    <div class="wl-img bx-sh p-2 ml-3">
                        <img src="images/accessory/01.png">
                    </div>
                    <div class="wl-txt text-right">
                        <p class="wl-title clr-42">
                            عنوان تستی برای محصول
                        </p>
                        <p class="wl-price clr-red">
                            1198000 تومان
                        </p>
                    </div>
                </a>
                <div class="wl-btn">
                    <button class="clr-75">
                        <i class="fal fa-trash-alt"></i>
                    </button>
                </div>
            </div>
            <div class="col-lg-6 col-md-12 col-sm-12 my-1 my-md-0 my-lg-3">
                <a href="">
                    <div class="wl-img bx-sh p-2 ml-3">
                        <img src="images/accessory/01.png">
                    </div>
                    <div class="wl-txt text-right">
                        <p class="wl-title clr-42">
                            عنوان تستی برای محصول
                        </p>
                        <p class="wl-price clr-red">
                            1198000 تومان
                        </p>
                    </div>
                </a>
                <div class="wl-btn">
                    <button class="clr-75">
                        <i class="fal fa-trash-alt"></i>
                    </button>
                </div>
            </div>
        </div>
        <div class="text-center">
            <button class="btn btn-info btn-round btn-circle circle-1 position-relative fs-16 dastnevis py-2">
                <i class="fal fa-file-edit clr-info fs-20 position-relative"></i>
                نمایش لیست علاقه مندی ها
            </button>
        </div>
    </div>
</div>
