<div class="profile-box">
    <div class="title-products text-center">
        <h3 class="pro-title fs-18 dastnevis header-gradient d-inline-block px-3 px-md-3 mb-0 pb-2">حساب کاربری من</h3>
        <hr class="under-title">
    </div>
    <div class="pbox p-list-box bg-ff bx-sh px-3 py-3">
        <ul>
            <li class="py-2">
                <a href="{{route('profile.index')}}" class="clr-75">
                    <i class="fal fa-user-alt fs-25"></i>
                    <span class="px-2">پروفایل من </span>
                </a>
            </li>
            <li class="py-2">
                <a href="{{route('profile.orders')}}" class="clr-75">
                    <i class="fal fa-shopping-basket fs-25"></i>
                    <span class="px-2">همه سفارش ها</span>
                </a>
            </li>
            <li class="py-2">
                <a href="{{route('profile.wishlist')}}" class="clr-75">
                    <i class="fal fa-grin-stars fs-25"></i>
                    <span class="px-2">لیست علاقه مندی ها</span>
                </a>
            </li>
            <li class="py-2">
                <a href="{{route('profile.address.index')}}" class="clr-75">
                    <i class="fal fa-map-marked-alt fs-25"></i>
                    <span class="px-2">آدرس ها</span>
                </a>
            </li>
            <li class="py-2">
                <a href="{{route('profile.info')}}" class="clr-75">
                    <i class="fal fa-user-circle fs-25"></i>
                    <span class="px-2">اطلاعات شخصی</span>
                </a>
            </li>
            <li class="py-2">
                <a href="{{route('profile.store.create')}}" class="clr-75">
                    <i class="fal fa-store fs-25"></i>
                    <span class="px-2">ایجاد فروشگاه</span>
                </a>
            </li>
        </ul>
    </div>
</div>
