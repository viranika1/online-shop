@extends("Layouts.Admin.Admin")
@section("Content")
	<?php
	global $cat;
	$cat = $category;
	function CatData($fild){
		global $cat;
		if (old($fild) == null){
			switch ($fild){
				case 'parentCat':
					$fild = 'parentId';
					break;
				case 'meta-keyword':
					$fild = 'metaKeyword';
					break;
				case 'meta-title':
					$fild = 'metaTitle';
					break;
				case 'meta-description':
					$fild = 'metaDescription';
					break;
			}
			return $cat[$fild];
		}
		return old($fild);
	}

	function categoriesTreeView(array $cats , int $level){
		foreach ($cats as $cat){
			$option = '<option value="';
			$option .= $cat['id'].'" ';
			$option .=  ($cat['parentId'] !== null)? 'data-pup="'.$cat['parentId'].'" ':'';
			$option .= 'class="l'.$level;
			$option .= isset($cat['childes']) && is_array($cat['childes'])? ' non-leaf"' : '"';
			$option .= (CatData('parentCat') == $cat['id'])? 'selected >' : '>';
			$option	.= $cat['name'].'</option>';
			echo $option;
			if (isset($cat['childes']) && is_array($cat['childes'])){
				categoriesTreeView($cat['childes'] , $level+1);
			}
		}
	}
	?>
	<div class="portlet box blue">
		<div class="portlet-title">
			<div class="caption">
				<i class="fa fa-gift"></i>افزودن دسته</div>
		</div>
		<div class="portlet-body form">
			<!-- BEGIN FORM-->
			<form action="{{route('admin.categories.update' , $category)}}" class="horizontal-form" method="post" role="form">
				@csrf @method("Patch")
				<div class="form-body">
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<label class="control-label">نام دسته</label>
								<input type="text" id="name" class="form-control" name="name" placeholder="نام دسته" value="{{CatData('name')}}">
								<span class="help-block"></span>
							</div>
						</div>
						<!--/span-->
						<div class="col-md-6">
							<div class="form-group">
								<label class="control-label">نامک</label>
								<input type="text" id="slug" class="form-control" name="slug" placeholder="نامک" value="{{CatData('slug')}}">
							</div>
						</div>
						<!--/span-->
					</div>
					<!--/row-->
					@if ($categories)
						<div class="row">
							<div class="col-md-12">
								<div class="form-group">
									<label class="control-label">دسته مادر</label>
									<select name="parentCat" id="parentCat" class="form-control" multiple>
										<?php categoriesTreeView($categories , 1) ?>
									</select>
								</div>
							</div>
						</div>
					@endif

					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<label for="meta-title" class="control-label">متای عنوان</label>
								<input type="text" name="meta-title" id="meta-title" class="form-control" value="{{CatData('meta-title')}}">
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<label for="meta-keyword" class="control-label">متای کلمات کلیدی</label>
								<input type="text" name="meta-keyword" id="meta-keyword" class="form-control" value="{{CatData('meta-keyword')}}">
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
							<div class="form-group">
								<label for="meta-description" class="control-label">متای توضیحات</label>
								<textarea class="form-control" name="meta-description" id="meta-description" cols="30" rows="10">{{CatData('meta-description')}}</textarea>
							</div>
						</div>
					</div>
				</div>
				<div class="form-actions right">
					<button type="submit" class="btn blue"><i
								class="fa fa-check"></i>ذخیره</button>
				</div>
			</form>
			<!-- END FORM-->
		</div>
	</div>
@endsection
@section("PagePluginsStyle")
	<link rel="stylesheet" href="/AdminDashboard/plugins/select2/select2.min.css">
	<link rel="stylesheet" href="/AdminDashboard/plugins/select2totree/select2totree.css">
@endsection
@section("PagePlugins")
	<script type="text/javascript" src="/AdminDashboard/plugins/select2/select2.min.js"></script>
	<script type="text/javascript" src="/AdminDashboard/plugins/select2totree/select2totree.js"></script>
@endsection

@section("PageScripts")
	<script>
        $(document).ready(function () {
            $("#parentCat").select2ToTree({
                dir:'rtl',
                maximumSelectionLength : 1
            })
        })
	</script>
@endsection