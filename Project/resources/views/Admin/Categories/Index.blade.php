@extends('Layouts.Admin.Admin')
@section('PagePluginsStyle')
	<link rel="stylesheet" type="text/css" href="/AdminDashboard/plugins/select2/select2.css"/>
	<link rel="stylesheet" type="text/css" href="/AdminDashboard/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css"/>
	<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/dt/dt-1.10.21/datatables.min.css"/>

@stop

@section('Content')
	<?php
	function categoriesTreeView(array $cats , int $level){
		foreach ($cats as $cat){
			?>
			<tr>
				<td><input type="checkbox" value="{{$cat['id']}}"></td>
				<td>
					@for ($i = 0; $i < $level; $i++)
						_
					@endfor
					{{$cat['name']}}
				</td>
				<td>{{$cat['slug']}}</td>
				<td>
					<form action="{{route('admin.categories.destroy' , $cat['id'] )}}" method="post">
						@csrf @method('DELETE')
						<div class="btn-group">
							<a href="{{route('admin.categories.edit' , $cat['id'] )}}" class="btn btn-success">ویرایش</a>
							<button type="submit" class="btn btn-warning">حذف</button>
						</div>

					</form>
				</td>
			</tr>

			<?php
			if (isset($cat['childes']) && is_array($cat['childes'])){
				categoriesTreeView($cat['childes'] , $level+1);
			}
		}
	}
	?>

	<div class="portlet light">
		<div class="portlet-body">
			@if (session('errorDelete'))
				<div class="alert alert-danger">
					{{	session('errorDelete') }}
				</div>
			@endif

			<table id="example" class="display" width="100%" cellspacing="0">
				<thead>
				<tr>
					<th>||</th>
					<th>نام دسته</th>
					<th>آدرس دسته</th>
					<th>عملیات</th>
				</tr>
				</thead>
				<tbody>
				<?php
				if (isset($categories)) {
					categoriesTreeView($categories , 0);
				}
				?>
				</tbody>
			</table>
		</div>
	</div>
@stop
@section('PagePlugins')

	<script type="text/javascript" src="/AdminDashboard/plugins/select2/select2.min.js"></script>
	<script type="text/javascript" src="http://vitalets.github.io/x-editable/assets/mockjax/jquery.mockjax.js"></script>

	<script type="text/javascript" src="https://cdn.datatables.net/v/dt/dt-1.10.21/datatables.min.js"></script>

	<script type="text/javascript" src="/AdminDashboard/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js"></script>
@stop
@section('PageScripts')

@stop