@extends("Layouts.Admin.Admin")
@section("Content")
	<div class="row">
		<div class="col-md-12">
			<form action="{{route('admin.properties.store')}}" method="post">
				@csrf
				<div class="row">
					<div class="col-lg-6">
						<div class="portlet box blue">
							<div class="portlet-title">
								<div class="caption">
									<i class="fa fa-gift"></i>اطلاعات کلی ویژگی
								</div>
							</div>
							<div class="portlet-body form">
								<!-- BEGIN FORM-->
								<div class="form-body">
									<div class="row">
										<div class="col-md-12">
											<div class="form-group">
												<label class="control-label">نام ویژگی</label>
												<input class="form-control" id="slug" name="slug" placeholder="نام ویژگی" type="text">
												<span class="help-block">نامی یکتا برای مدیریت ویژگی ها</span>
											</div>
										</div>
										<div class="col-md-12">
											<div class="form-group">
												<label class="control-label">نام نمایشی</label>
												<input class="form-control" id="name" name="name" placeholder="نام نمایشی" type="text">
												<span class="help-block">نام نمایشی در سایت</span>
											</div>
										</div>
									</div>
									<div class="row">
										<div class="col-md-6">
											<div class="form-group">
												<label class="control-label">نوع ویژگی</label>
												<select class="form-control" id="typeOption" name="typeOption">
													<option value="1">ساده</option>
													<option value="2">متن بزرک</option>
													<option value="3">لیست</option>
													<option value="4">رنگ</option>
												</select>
												<span class="help-block">نوع مقدار ویژگی را انتخاب کنید.</span>
											</div>
										</div>
										<div class="col-md-6">
											<div class="form-group">
												<label style="margin-top: 30px;">
													<input class="icheck" data-checkbox="icheckbox_line-blue"
														   data-label="افزودن لینک" id="link" id="link"
														   name="link"
														   type="checkbox">
												</label>
											</div>
										</div>
									</div>
									<div id="linked-checked" style="display: none;">
										<div class="row">
											<div class="col-md-6">
												<div class="form-group">
													<label class="control-label" for="meta-title">متای عنوان</label>
													<input class="form-control" id="meta-title" name="meta-title" type="text">
												</div>
											</div>
											<div class="col-md-6">
												<div class="form-group">
													<label class="control-label" for="meta-keyword">متای کلمات کلیدی</label>
													<input class="form-control" id="meta-keyword" name="meta-keyword" type="text">
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-md-12">
												<div class="form-group">
													<label class="control-label" for="meta-description">متای توضیحات</label>
													<textarea class="form-control" id="meta-description" name="meta-description" rows="5" style="resize:vertical"></textarea>
												</div>
											</div>
										</div>
									</div>
								</div>
								<!-- END FORM-->
							</div>
						</div>
					</div>
					<div class="col-lg-6">
						<div class="portlet box blue" style="margin-bottom: 0px!important" >
							<div class="portlet-title">
								<div class="caption">
									<i class="fa fa-gift"></i>مقدار های ویژگی
								</div>
							</div>
							<div class="portlet-body form">
								<!-- BEGIN FORM-->
								<div class="form-body">
									<div class="row" id="headValues">
										<div class="col-md-10">
											<label for="">مقدار</label>
											<hr>
										</div>
										<div class="col-md-2">
											<label for="">عملیات</label>
											<hr>
										</div>
									</div>
									<div class="row">
										<div class="col-md-12" id="valuesOption">

										</div>
									</div>
								</div>
								<div class="form-actions">
									<hr>
									<button class="btn blue right" id="addValue" type="button">
										<i class="fa fa-plus-circle"></i>افزودن
									</button>
									<button class="btn green right" id="" type="submit">
										<i class="fa fa-floppy-o"></i>ذخیره
									</button>
								</div>
								<!-- END FORM-->
							</div>
						</div>
					</div>
				</div>
			</form>
		</div>
		<div class="row margin-top-10" id="templateValue" hidden dataKey="" >
			<div class="col-sm-10">
				<div class="row">
					<div class="input-option col-sm-8">
						<input type="text" name="variables[]" class="form-control">
					</div>
					<div class="color-option col-sm-4">
						<div class="input-group color colorpicker-default" data-color="#fff" data-color-format="rgb">
							<input class="form-control spectrum_color" type="text" name="colorVal[]" value="#000">
						</div>
					</div>
				</div>
			</div>
			<div class="col-sm-2">
				<button class="btn red remove-value" type="button">حذف</button>
			</div>
		</div>
	</div>
	</div>
@endsection
@section("PagePluginsStyle")
	<link rel="stylesheet" href="/AdminDashboard/plugins/spectrum/spectrum.css"  type="text/css">
	<link rel="stylesheet" href="/AdminDashboard/plugins/icheck/skins/all.css" type="text/css"/>
@endsection

@section('PagePlugins')
	<script type="text/javascript" src="/AdminDashboard/plugins/spectrum/spectrum.js"></script>
	<script type="text/javascript" src="/AdminDashboard/plugins/icheck/icheck.min.js"></script>
@endsection

@section("PageScripts")
	<script>
        jQuery(document).ready(function () {
            $(".iCheck-helper").click(function () {
                if ($("#link").is(":checked")) {
                    $("#linked-checked").slideDown();
                } else {
                    $("#linked-checked").slideUp();
                }
            });
            $("#addValue").click()
            $(".remove-value").click(function(){
                $(this).parent().parent().fadeOut(function () {$(this).remove()});
            });
            $("#addValue").click(function () {
                let template = $("#templateValue").clone().fadeIn();
                $("#valuesOption").append(template)
				template.find('.spectrum_color').spectrum({
                    type: "text",
                    showPaletteOnly: "true",
                    togglePaletteOnly: "true",
                    hideAfterPaletteSelect: "true",
                    showInput: "true",
                    showInitial: "true"
				});
                $("#valuesOption")[0].lastChild.removeAttribute("id");
                $("#valuesOption")[0].lastChild.removeAttribute("hidden");
                $(".remove-value").click(function(){
                    $(this).parent().parent().fadeOut(function () {$(this).remove()});
                });

            })
        });


        $("#typeOption").change(function () {
            if ($(this).val() == "4") {
                $(".input-option").removeClass("col-sm-12").addClass("col-sm-8")
                $(".color-option").addClass("col-sm-4").show()
            } else {
                $(".input-option").removeClass("col-sm-8").addClass("col-sm-12")
                $(".color-option").removeClass("col-sm-4").hide()
            }
        })
	</script>
@endsection