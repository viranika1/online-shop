        <!-- DOC: Apply "page-sidebar-menu-light" class right after "page-sidebar-menu" to enable light sidebar menu style(without borders) -->
        <!-- DOC: Apply "page-sidebar-menu-hover-submenu" class right after "page-sidebar-menu" to enable hoverable(hover vs accordion) sub menu mode -->
        <!-- DOC: Apply "page-sidebar-menu-closed" class right after "page-sidebar-menu" to collapse("page-sidebar-closed" class must be applied to the body element) the sidebar sub menu mode -->
        <!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing -->
        <!-- DOC: Set data-keep-expand="true" to keep the submenues expanded -->
        <!-- DOC: Set data-auto-speed="200" to adjust the sub menu slide up/down speed -->
        <ul class="page-sidebar-menu " data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200">
            <li class="start active ">
                <a href="{{route("admin.dashboard")}}">
                    <i class="icon-home"></i>
                    <span class="title">داشبورد</span>
                </a>
            </li>
            <li>
                <a href="javascript:;">
                    <i class="icon-basket"></i>
                    <span class="title">محصولات</span>
                    <span class="arrow "></span>
                </a>
                <ul class="sub-menu">
                    <li>
                        <a href="ecommerce_index.html">

                            نمایش محصولات</a>
                    </li>
                    <li>
                        <a href="ecommerce_orders.html">

                            افزودن محصول</a>
                    </li>
                    <li>
                        <a href="ecommerce_orders_view.html">

                            ویژگی </a>
                    </li>
                    <li>
                        <a href="{{route("admin.categories.index")}}">

                            دسته</a>
                    </li>

                </ul>
            </li>
            <li>
                <a href="javascript:;">
                    <i class="icon-user"></i>
                    <span class="title">کاربران</span>
                    <span class="arrow "></span>
                </a>
                <ul class="sub-menu">
                    <li>
                        <a href="layout_sidebar_reversed.html">
                            <span class="badge badge-warning">new</span>لیست کاربران</a>
                    </li>
                    <li>
                        <a href="layout_sidebar_fixed.html">
                            افزودن کاربر</a>
                    </li>
                    <li>
                        <a href="layout_sidebar_closed.html">
                            پروفایل کاربر</a>
                    </li>

                </ul>
            </li>
            <li>
                <a href="javascript:;">
                    <i class="icon-bag"></i>
                    <span class="title">سفارشات</span>
                    <span class="arrow "></span>
                </a>
                <ul class="sub-menu">
                    <li>
                        <a href="ui_general.html">
                            همه سفارشات</a>
                    </li>
                    <li>
                        <a href="ui_buttons.html">
                            لیست سبدهای خرید</a>
                    </li>
                </ul>
            </li>
            <li>
                <a href="javascript:;">
                    <i class="icon-present"></i>
                    <span class="title">تخفیف</span>
                    <span class="arrow "></span>
                </a>
                <ul class="sub-menu">
                    <li>
                        <a href="components_pickers.html">
                            لیست تخفیفات</a>
                    </li>
                    <li>
                        <a href="components_context_menu.html">
                            ثبت کد تخفیف</a>
                    </li>
                </ul>
            </li>
            <li>
                <a href="javascript:;">
                    <i class="icon-bubble"></i>
                    <span class="title">نظرات</span>
                    <span class="arrow "></span>
                </a>
                <ul class="sub-menu">
                    <li>
                        <a href="form_controls_md.html">
                            لیست نظرات</a>
                    </li>
                    <li>
                        <a href="form_controls.html">
                            تاییدنظر</a>
                    </li>

                </ul>
            </li>
            <li>
                <a href="javascript:;">
                    <i class="icon-docs"></i>
                    <span class="title">مدیریت فایل</span>
                    <span class="arrow "></span>
                </a>
                <ul class="sub-menu">
                    <li>
                        <a href="table_basic.html">
                            لیست فایل ها</a>
                    </li>
                    <li>
                        <a href="table_tree.html">
                            افزودن فایل</a>
                    </li>

                </ul>
            </li>
            <!-- BEGIN ANGULARJS LINK -->
            <li class="tooltips" data-container="body" data-placement="right" data-html="true" data-original-title="AngularJS version demo">
                <a href="angularjs" target="_blank">
                    <i class="icon-paper-plane"></i>
                    <span class="title">
					گزارشات</span>
                </a>
            </li>
        </ul>
