<!-- Start Header -->
<section class="container-fluid top-bar"></section>
<section class="container-fluid top-header bg-ff">
    <div class="container">
        <div class="row">
            <div class="col-12 col-md-4 col-lg-3 logo-area">
                <a href="/#">
                    <img src="/images/logo.svg" alt="هنرچی">
                </a>
            </div>
            <div class="col-12 col-md-8 col-lg-9 user-menu text-center text-md-left text-lg-left">
                <ul class="user-menu-ul">
                    <li id="search-form" class="search-form w-50">
                        <div class="input-group ">
                            <input type="search" class="form-control" placeholder="محصول موردنظرت رو پیدا کن...">
                            <div class="input-group-prepend">
                                <span class="input-group-text">
                                     <i class="fal fa-search"></i>
                                </span>
                            </div>
                        </div>
                    </li>
                    <li id="profile" class="profile">
                        <a class="profile-link" href="{{route('profile.index')}}">
                            <i class="fal fa-user"></i>
                        </a>
                        @auth()
                        <!-- is-login -->
                        <ul class="tooltip-profile cart-tooltip p-2">
                            <li class="d-block text-right mb-1">
                                <a class="d-block" href="{{route('profile.index')}}">
                                    <i class="fal fa-user ml-2 "></i>
                                    پروفایل
                                </a>
                            </li>
                            <li class="d-block text-right mb-1">
                                <a class="d-block" href="{{route('profile.orders')}}">
                                    <i class="fal fa-shopping-basket ml-2"></i>
                                    سفارش های من
                                </a>
                            </li>
                            <li class="d-block text-right  mb-1">
                                <form action="{{route('logout')}}" method="post">
                                    @csrf
                                    <button type="submit" style="display: contents; text-align: right">
                                        <a class="d-block" href="#">
                                            <i class="fal fa-sign-out ml-2">
                                            </i>
                                            <d>
                                                خروج از حساب کاربری
                                            </d>
                                        </a>
                                    </button>
                                </form>

                            </li>
                        </ul>
                        @endauth
                        @guest()
                        <ul class="tooltip-profile cart-tooltip p-2">
                            <div class="login-link text-center">
                                <a href="{{ route('login') }}">
                                    <button class="btn btn-primary btn-round">
                                        <i class="fal fa-sign-in ml-2"></i>
                                        ورود به هنرچی
                                    </button>
                                </a>
                            </div>
                            <br>
                            <hr>
                            <div class="register-link text-center mt-2">
                                <p>کاربر جدید هستید ؟
                                    <a class="clr-blue2" href="{{route('register')}}">ثبت نام</a>
                                </p>
                            </div>
                        </ul>
                        @endguest
                    </li>
                    <li id="wishlist" class="wishlist">
                        <a class="wishlist-link" href="/#">
                            <i class="fal fa-heart"></i>
                        </a>
                    </li>
                    <li id="heder-cart" class="header-cart">
                        <a class="p-0 mr-2 header-cart-link" href="#">
                            <div class="cart-count-area">
                                <span class="cart-count">0</span>
                            </div>
                            <i class="fal fa-shopping-cart"></i>
                        </a>
                        @include('Components.App.HeaderCart')
                    </li>
                </ul>
            </div>
        </div>
    </div>
    </div>
</section>
@include('Components.App.NavigationBar')
<section class="container-fluid svg-bg p-0">
    <div class="svg-area">
        <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
             viewBox="0 0 500 31.5" style="enable-background:new 0 0 500 31.5;" xml:space="preserve">
			<style type="text/css">
                .st0{opacity:0.33;fill:#F5F5F5;enable-background:new    ;}
                .st1{opacity:0.66;fill:#F5F5F5;enable-background:new    ;}
                .st2{fill:#F5F5F5;}
            </style>
            <path class="st0" d="M236.5,21.2c-101.9,27.7-131.6-10.6-160.1,0C33,37.4,0,18.8,0,18.8V0.1h500v18.7c0,0-31,8.2-47.5,9.2
				c-16.4,1-31.4-3.9-37.9-6.9c-11.6-5.4-42-18.2-67.2-19.5S246.2,18.6,236.5,21.2z"/>
            <path class="st1" d="M367,21.2c-22.8,0-38.6-7.3-64.5-12.2c-14.3-2.7-75.1-3.2-127,12.2s-45.9-10.8-74.6,0C57.8,37.2,0,12.6,0,12.6
				V0.1h500v11.4c0,0-14.1-5.8-46-5.8C405.1,5.8,387.9,21.2,367,21.2z"/>
            <path class="st2" d="M383,9.2c-100-18-133,20.5-197.6,6.1C121,0.7,121,1.8,92.4,6.6C64,11.3,66.2,14.2,45,16.6
			C14.3,20.1,0,0.1,0,0.1h500c0,0-5,12.8-41.8,15.1S414.8,14.8,383,9.2z"/>
		</svg>

    </div>
</section>
<!-- End Header -->
