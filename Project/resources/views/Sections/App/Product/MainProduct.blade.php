<section class="container-fluid main-product pb-5">
    <div class="container">
        <div class="row content-main-product bg-ff bx-sh">
            <div class="col-12 col-lg-6">
                <div class="socials sticky-top">
                    <ul class="share-menu position-absolute">
                        <li class="main-share">
                            <a href="#">
									<span>
										<i class="fal fa-share-alt fs-25 clr-92"></i>
									</span>
                            </a>
                            <ul>
                                <li>
                                    <a href="#">
											<span>
												<i class="fab fa-telegram-plane fs-25 clr-92"></i>
											</span>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
											<span>
												<i class="fab fa-twitter fs-25 clr-92"></i>
											</span>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
											<span>
												<i class="fab fa-instagram fs-25 clr-92"></i>
											</span>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
											<span>
												<i class="fab fa-linkedin-in fs-25 clr-92"></i>
											</span>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
											<span>
												<i class="fab fa-google-plus-g fs-25 clr-92"></i>
											</span>
                                    </a>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </div>
                @include('Components.App.Product.ImageGallery')
            </div>
            <div class="col-12 col-lg-6 text-right position-relative">
                <div class="bg-svg-article position-absolute">
                    <svg version="1.1" id="Isolation_Mode" xmlns="http://www.w3.org/2000/svg"
                         xmlns:xlink="http://www.w3.org/1999/xlink" x="0px"
                         y="0px" viewBox="0 0 233 96" style="enable-background:new 0 0 233 96;" xml:space="preserve">
										<g style="opacity:0.41;">
                                            <path style="fill:#EAEAEA;" d="M172.8,0c-6.2-0.2-17.8-1.2-22.5,2.9c-17.2,14.7-24,38.7-40.3,54.4C93.7,72.9,71,81,48.3,82.1
								C25.7,83.2,5.4,71.9,0,67.3L-0.2,0H172.8z"/>
                                            <g>
                                                <path style="fill:#DBDBDB;"
                                                      d="M218.8,0.4h-219v63c3.6-10.8,10-21.1,21-29.2c20-14.6,52.5-15.1,79.7-13.3c31.2,2,58.4,18.7,88.3,11.5c19.2-4.6,31.3-21.5,30-31.8C218.8,0.5,218.9,0.4,218.8,0.4z"/>
                                            </g>
                                        </g>
						</svg>
                </div>
                <div class="badge-sty2">
                    <span class="percentage fs-14">20 %</span>
                    <span class="lable-percentage">تخفیف</span>
                </div>
                <div class="title-product">
                    <h1 class="dastnevis header-gradient fs-20 p-2">کاسه خوشگل موشگل</h1>
                </div>
                <div class="price-single">
                    <h4 class="d-inline-block iransans fs-16">قیمت محصول :</h4>
                    <del class="text-center clr-b2 mb-1 fs-14">
                        <span>15000</span>
                        <span class="unitmoney">
								تومان
							</span>
                    </del>
                    <ins class="text-center clr-red fs-14">
                        <span>13000</span>
                        <span class="unitmoney">
								تومان
							</span>
                    </ins>
                </div>
                <div class="price-wholesale">
                    <h4 class="d-inline-block iransans fs-16">قیمت عمده محصول :</h4>
                    <del class="text-center clr-b2 mb-1 fs-14">
                        <span>15000</span>
                        <span class="unitmoney">
								تومان
							</span>
                    </del>
                    <ins class="text-center clr-red fs-14">
                        <span>13000</span>
                        <span class="unitmoney">
								تومان
							</span>
                    </ins>
                    <div class="porduct-variable">
                        <h4 class="d-inline-block iransans fs-16">رنگ محصول را انتخاب کنید : </h4>
                        <ul class="color-variable">
                            <a href="">
                                <li class="blue">

                                </li>
                            </a>
                            <a href="">
                                <li class="red">

                                </li>
                            </a>
                            <a href="">
                                <li class="green">

                                </li>
                            </a>
                        </ul>
                        <h4 class="d-inline-block iransans fs-16">سایز محصول را انتخاب کنید : </h4>
                        <ul class="size-variable">
                            <a href="">
                                <li class="Small">
                                    S
                                </li>
                            </a>
                            <a href="">
                                <li class="Medium">
                                    M
                                </li>
                            </a>
                            <a href="">
                                <li class="Larg">
                                    L
                                </li>
                            </a>
                            <a href="">
                                <li class="XLarg">
                                    XL
                                </li>
                            </a>
                        </ul>
                    </div>
                    <div class="addtocart-area position-relative">
                        <div class="qty position-absolute">
                            <span class="minus bg-dd">-</span>
                            <input type="number" class="count clr-75" name="qty" value="1">
                            <span class="plus bg-dd">+</span>
                        </div>
                        <button class="position-absolute btn btn-success bg-green btn-round btn-circle circle-1 dastnevis fs-16">
                            <i class="fal fa-cart-plus clr-green fs-20 position-relative"></i>
                            افزودن به سبد خرید
                        </button>
                    </div>
                    <div class="space-height">
                    </div>
                    <div class="storage my-3 text-right clr-red">
                        <span>ناموجود</span>
                    </div>
                    <div class="modal-buy">
                        <!-- Button trigger modal -->
                        <button type="button"
                                class="position-relative btn btn-default bg-green btn-round btn-circle circle-1 dastnevis fs-16"
                                data-toggle="modal" data-target="#buy">
                            <i class="fal fa-shopping-bag clr-75 fs-20 position-relative"></i>
                            قصد پیش خرید محصول را دارم
                        </button>
                        <!-- Modal -->
                        <div class="modal fade" id="buy" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
                             aria-hidden="true">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title dastnevis clr-blue2" id="m-title">پیش خرید</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                        <p>این محصول اکنون در انبار موجود نیست </p>
                                        <p>اما امکان سفارش این محصول میباشد تا هنرمند هنرچی برایتان آماده کند</p>
                                        <p>مدت زمان آماده شدن محصول از
                                            <span class="clr-red">15</span>
                                            روز تا
                                            <span class="clr-red">30</span>
                                            روز می باشد
                                        </p>
                                        <div class="addtocart-area popup position-relative">
                                            <div class="qty d-inline">
                                                <span class="minus bg-dd">-</span>
                                                <input type="number" class="count clr-75" name="qty" value="1">
                                                <span class="plus bg-dd">+</span>
                                            </div>
                                            <button class=" btn btn-success bg-green btn-round btn-circle circle-1 dastnevis fs-16">
                                                <i class="fal fa-cart-plus clr-green fs-20 position-relative"></i>
                                                افزودن به سبد خرید
                                            </button>
                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="meta-product mt-3">
                        <div class="content-meta category">
                            <i class="fal fa-folder-open"></i>
                            <span class="dastnevis clr-blue2">دسته ها :</span>
                            <a class="dastnevis clr-61" href="#">تزئین</a>
                            <span>،</span>
                            <a class="dastnevis clr-61" href="#">دکور وتزئینات</a>
                            <span>،</span>
                            <a class="dastnevis clr-61" href="#">تزئین</a>
                            <span>،</span>
                            <a class="dastnevis clr-61" href="#">دکور وتزئینات</a>
                        </div>
                        <div class="content-meta brand">
                            <i class="fal fa-signature"></i>
                            <span class="dastnevis clr-blue2">نام برند / هنرمند :</span>
                            <a class="dastnevis clr-61" href="#">دستان پرتوان</a>
                        </div>
                        <div class="content-meta attentions">
                            <i class="fal fa-exclamation-triangle clr-red"></i>
                            <p class="dastnevis d-inline-block">به ازای خرید هر
                                <span class="clr-red">12 عدد</span>
                                قیمت یه صورت عمده برایتان محصوب می شود.
                            </p>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>