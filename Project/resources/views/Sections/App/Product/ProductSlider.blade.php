<section class="container-fluid row-product px-4 pb-5 bg-product {{$slider->cssClass}}">
    <div class=" container">
        <div class="row">
            <div class="col-12 title-products p-0">
                <div class="row align-items-end">
                    <div class="col-7 col-lg-9 col-xl-10 text-right">
                        <h3 class="pro-title fs-18 dastnevis header-gradient text-center d-inline-block px-4 mb-0 pb-2">{{$slider->name}}</h3>
                    </div>
                    <div class="col-5 col-lg-3 col-xl-2">
                        <a href="{{$slider->link}}">
                            <button class="btn btn-round btn-title dastnevis fs-18 bg-ff btn-outline-secondary py-2 m-0">مشاهده همه</button>
                        </a>
                    </div>
                </div>
                <hr class="under-title">
            </div>
        </div>
        <div class="row">
            <div class="col-12 row-products px-0">
                <div class="owl-carousel products owl-theme">
                    @foreach($slider->products as $product)
                    <div class="item">
                        @include('Components.App.Product.ProductCard')
                    </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
    </div>
</section>