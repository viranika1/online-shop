<section class="container-fluid row-gift">
    <div class="container">
        <div class="row">
            <div class="col-lg-6 col-xl-5">
                <img src="images/mandegaar.png" alt="">
            </div>
            <div class="d-none d-xl-block col-xl-1">
            </div>
            <div class="col-lg-6 col-xl-6 m-auto">
                @include('Components.App.Index.GiftsLink')
            </div>
        </div>
    </div>
</section>