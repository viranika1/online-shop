<section class="bg-img services-chanse">
    @include('Components.App.Index.Services')
    <section class="container-fluid row-chanse pt-0 pb-4">
        <div class="container">
            <div class="row">
                <div class="col-12 col-lg-9 order-2 order-lg-1 column-chanse">
                    @include('Components.App.Index.Chances')
                </div>
                <div class="col-12 col-lg-3 order-1 order-lg-2 banner-chanse">
                    <a href="#">
                        <img class="bx-sh d-none d-lg-block" src="images/wholesale.png" alt="">
                        <img class="bx-sh d-block d-lg-none mb-3" src="images/wholesale-m.png" alt="">
                    </a>
                </div>
            </div>
        </div>
    </section>
    @include('Components.App.Index.CategoryLinks')
</section>