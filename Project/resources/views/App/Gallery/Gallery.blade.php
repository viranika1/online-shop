@extends('Layouts.App.App')
@section('content')

    <section class="container-fluid info-box">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="breadcrumb-area text-center bx-sh bg-ff py-2">
                        <nav class="d-inline-block" aria-label="breadcrumb" role="navigation">
                            <ol class="breadcrumb bg-ff dastnevis">
                                <li class="breadcrumb-item"><a class="clr-61" href="#">صفحه اصلی</a></li>
                                <li class="breadcrumb-item active" aria-current="page">لیست گالری ها</li>
                            </ol>
                        </nav>
                        <div class="title-article">
                            <h1 class="dastnevis header-gradient fs-25">لیست گالری ها</h1>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="container-fluid cont-archive pt-3 pb-5">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 product-archive mt-5 mt-lg-0">
                    <div class="row">
                        <div class="col-lg-12 text-right">
                            <div class="title-products d-inline">
                                <h3 class="d-none d-lg-inline-block pro-title fs-18 dastnevis header-gradient text-center px-3 px-md-4 mb-0 ">رسته هنری :</h3>
                                <ul class="orderBy d-inline-block">
                                    <li class="d-inline-block">
                                        <a class="fs-12 clr-61 py-1 px-2" href="#">سفال و سرامیک</a>
                                    </li>
                                    <li class="d-inline-block">
                                        <a class="fs-12 clr-61 py-1 px-2" href="#">چرم</a>
                                    </li>
                                    <li class="d-inline-block">
                                        <a class="fs-12 clr-61 py-1 px-2" href="#">نسوجات</a>
                                    </li>
                                    <li class="d-inline-block">
                                        <a class="fs-12 clr-61 py-1 px-2 active" href="#">طراحی و نقاشی</a>
                                    </li>
                                    <li class="d-inline-block">
                                        <a class="fs-12 clr-61 py-1 px-2" href="#">سنگ و فلز قیمتی</a>
                                    </li>
                                    <li class="d-inline-block">
                                        <a class="fs-12 clr-61 py-1 px-2" href="#">چوب</a>
                                    </li>

                                </ul>
                                <hr class="under-title">
                            </div>
                        </div>
                    </div>
                    <div class="row mt-3">
                        <div class="col-12 col-md-6 col-lg-4 mb-4">
                            <div class="content-gallery bg-ff bx-sh position-relative">
                                <div class="inner-content-gallery">
                                    <div class="img-gallery">
                                        <a href="#">
                                            <img src="images/accessory/hashimian.jpg" alt="">
                                        </a>
                                    </div>
                                    <div class="inner-gallery pr-3 position-absolute">
                                        <div class="name-gallery text-right">
                                            <a href="#">
                                                <h3 class="dastnevis clr-ff fs-18">ریحانه هاشمیان</h3>
                                            </a>
                                        </div>
                                        <div class="rate-gallery text-right">
                                            <i class="fal fa-star clr-f5"></i>
                                            <i class="fal fa-star clr-f5"></i>
                                            <i class="fal fa-star clr-yellow"></i>
                                            <i class="fal fa-star clr-yellow"></i>
                                            <i class="fal fa-star clr-yellow"></i>
                                        </div>
                                        <div class="location-gallery text-right">
                                            <p class="dastnevis clr-ff">خراسان جنوبی - نوفرست</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="thumnail-shop position-absolute">
                                    <a href="#">
                                        <img class="bx-sh" src="images/accessory/shop-auther.jpg" alt="">
                                    </a>
                                </div>
                                <div class="about-shopping text-center position-relative">
                                    <button class="btn btn-secondary btn-round dastnevis fs-16 py-2  my-2 te-sh">نمایش فروشگاه</button>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-md-6 col-lg-4 mb-4">
                            <div class="content-gallery bg-ff bx-sh position-relative">
                                <div class="inner-content-gallery">
                                    <div class="img-gallery">
                                        <a href="#">
                                            <img src="images/accessory/hashimian.jpg" alt="">
                                        </a>
                                    </div>
                                    <div class="inner-gallery pr-3 position-absolute">
                                        <div class="name-gallery text-right">
                                            <a href="#">
                                                <h3 class="dastnevis clr-ff fs-18">ریحانه هاشمیان</h3>
                                            </a>
                                        </div>
                                        <div class="rate-gallery text-right">
                                            <i class="fal fa-star clr-f5"></i>
                                            <i class="fal fa-star clr-f5"></i>
                                            <i class="fal fa-star clr-yellow"></i>
                                            <i class="fal fa-star clr-yellow"></i>
                                            <i class="fal fa-star clr-yellow"></i>
                                        </div>
                                        <div class="location-gallery text-right">
                                            <p class="dastnevis clr-ff">خراسان جنوبی - نوفرست</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="thumnail-shop position-absolute">
                                    <a href="#">
                                        <img class="bx-sh" src="images/accessory/shop-auther.jpg" alt="">
                                    </a>
                                </div>
                                <div class="about-shopping text-center position-relative">
                                    <button class="btn btn-secondary btn-round dastnevis fs-16 py-2  my-2 te-sh">نمایش فروشگاه</button>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-md-6 col-lg-4 mb-4">
                            <div class="content-gallery bg-ff bx-sh position-relative">
                                <div class="inner-content-gallery">
                                    <div class="img-gallery">
                                        <a href="#">
                                            <img src="images/accessory/hashimian.jpg" alt="">
                                        </a>
                                    </div>
                                    <div class="inner-gallery pr-3 position-absolute">
                                        <div class="name-gallery text-right">
                                            <a href="#">
                                                <h3 class="dastnevis clr-ff fs-18">ریحانه هاشمیان</h3>
                                            </a>
                                        </div>
                                        <div class="rate-gallery text-right">
                                            <i class="fal fa-star clr-f5"></i>
                                            <i class="fal fa-star clr-f5"></i>
                                            <i class="fal fa-star clr-yellow"></i>
                                            <i class="fal fa-star clr-yellow"></i>
                                            <i class="fal fa-star clr-yellow"></i>
                                        </div>
                                        <div class="location-gallery text-right">
                                            <p class="dastnevis clr-ff">خراسان جنوبی - نوفرست</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="thumnail-shop position-absolute">
                                    <a href="#">
                                        <img class="bx-sh" src="images/accessory/shop-auther.jpg" alt="">
                                    </a>
                                </div>
                                <div class="about-shopping text-center position-relative">
                                    <button class="btn btn-secondary btn-round dastnevis fs-16 py-2  my-2 te-sh">نمایش فروشگاه</button>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-md-6 col-lg-4 mb-4">
                            <div class="content-gallery bg-ff bx-sh position-relative">
                                <div class="inner-content-gallery">
                                    <div class="img-gallery">
                                        <a href="#">
                                            <img src="images/accessory/hashimian.jpg" alt="">
                                        </a>
                                    </div>
                                    <div class="inner-gallery pr-3 position-absolute">
                                        <div class="name-gallery text-right">
                                            <a href="#">
                                                <h3 class="dastnevis clr-ff fs-18">ریحانه هاشمیان</h3>
                                            </a>
                                        </div>
                                        <div class="rate-gallery text-right">
                                            <i class="fal fa-star clr-f5"></i>
                                            <i class="fal fa-star clr-f5"></i>
                                            <i class="fal fa-star clr-yellow"></i>
                                            <i class="fal fa-star clr-yellow"></i>
                                            <i class="fal fa-star clr-yellow"></i>
                                        </div>
                                        <div class="location-gallery text-right">
                                            <p class="dastnevis clr-ff">خراسان جنوبی - نوفرست</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="thumnail-shop position-absolute">
                                    <a href="#">
                                        <img class="bx-sh" src="images/accessory/shop-auther.jpg" alt="">
                                    </a>
                                </div>
                                <div class="about-shopping text-center position-relative">
                                    <button class="btn btn-secondary btn-round dastnevis fs-16 py-2  my-2 te-sh">نمایش فروشگاه</button>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-md-6 col-lg-4 mb-4">
                            <div class="content-gallery bg-ff bx-sh position-relative">
                                <div class="inner-content-gallery">
                                    <div class="img-gallery">
                                        <a href="#">
                                            <img src="images/accessory/hashimian.jpg" alt="">
                                        </a>
                                    </div>
                                    <div class="inner-gallery pr-3 position-absolute">
                                        <div class="name-gallery text-right">
                                            <a href="#">
                                                <h3 class="dastnevis clr-ff fs-18">ریحانه هاشمیان</h3>
                                            </a>
                                        </div>
                                        <div class="rate-gallery text-right">
                                            <i class="fal fa-star clr-f5"></i>
                                            <i class="fal fa-star clr-f5"></i>
                                            <i class="fal fa-star clr-yellow"></i>
                                            <i class="fal fa-star clr-yellow"></i>
                                            <i class="fal fa-star clr-yellow"></i>
                                        </div>
                                        <div class="location-gallery text-right">
                                            <p class="dastnevis clr-ff">خراسان جنوبی - نوفرست</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="thumnail-shop position-absolute">
                                    <a href="#">
                                        <img class="bx-sh" src="images/accessory/shop-auther.jpg" alt="">
                                    </a>
                                </div>
                                <div class="about-shopping text-center position-relative">
                                    <button class="btn btn-secondary btn-round dastnevis fs-16 py-2  my-2 te-sh">نمایش فروشگاه</button>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-md-6 col-lg-4 mb-4">
                            <div class="content-gallery bg-ff bx-sh position-relative">
                                <div class="inner-content-gallery">
                                    <div class="img-gallery">
                                        <a href="#">
                                            <img src="images/accessory/hashimian.jpg" alt="">
                                        </a>
                                    </div>
                                    <div class="inner-gallery pr-3 position-absolute">
                                        <div class="name-gallery text-right">
                                            <a href="#">
                                                <h3 class="dastnevis clr-ff fs-18">ریحانه هاشمیان</h3>
                                            </a>
                                        </div>
                                        <div class="rate-gallery text-right">
                                            <i class="fal fa-star clr-f5"></i>
                                            <i class="fal fa-star clr-f5"></i>
                                            <i class="fal fa-star clr-yellow"></i>
                                            <i class="fal fa-star clr-yellow"></i>
                                            <i class="fal fa-star clr-yellow"></i>
                                        </div>
                                        <div class="location-gallery text-right">
                                            <p class="dastnevis clr-ff">خراسان جنوبی - نوفرست</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="thumnail-shop position-absolute">
                                    <a href="#">
                                        <img class="bx-sh" src="images/accessory/shop-auther.jpg" alt="">
                                    </a>
                                </div>
                                <div class="about-shopping text-center position-relative">
                                    <button class="btn btn-secondary btn-round dastnevis fs-16 py-2  my-2 te-sh">نمایش فروشگاه</button>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-md-6 col-lg-4 mb-4">
                            <div class="content-gallery bg-ff bx-sh position-relative">
                                <div class="inner-content-gallery">
                                    <div class="img-gallery">
                                        <a href="#">
                                            <img src="images/accessory/hashimian.jpg" alt="">
                                        </a>
                                    </div>
                                    <div class="inner-gallery pr-3 position-absolute">
                                        <div class="name-gallery text-right">
                                            <a href="#">
                                                <h3 class="dastnevis clr-ff fs-18">ریحانه هاشمیان</h3>
                                            </a>
                                        </div>
                                        <div class="rate-gallery text-right">
                                            <i class="fal fa-star clr-f5"></i>
                                            <i class="fal fa-star clr-f5"></i>
                                            <i class="fal fa-star clr-yellow"></i>
                                            <i class="fal fa-star clr-yellow"></i>
                                            <i class="fal fa-star clr-yellow"></i>
                                        </div>
                                        <div class="location-gallery text-right">
                                            <p class="dastnevis clr-ff">خراسان جنوبی - نوفرست</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="thumnail-shop position-absolute">
                                    <a href="#">
                                        <img class="bx-sh" src="images/accessory/shop-auther.jpg" alt="">
                                    </a>
                                </div>
                                <div class="about-shopping text-center position-relative">
                                    <button class="btn btn-secondary btn-round dastnevis fs-16 py-2  my-2 te-sh">نمایش فروشگاه</button>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-md-6 col-lg-4 mb-4">
                            <div class="content-gallery bg-ff bx-sh position-relative">
                                <div class="inner-content-gallery">
                                    <div class="img-gallery">
                                        <a href="#">
                                            <img src="images/accessory/hashimian.jpg" alt="">
                                        </a>
                                    </div>
                                    <div class="inner-gallery pr-3 position-absolute">
                                        <div class="name-gallery text-right">
                                            <a href="#">
                                                <h3 class="dastnevis clr-ff fs-18">ریحانه هاشمیان</h3>
                                            </a>
                                        </div>
                                        <div class="rate-gallery text-right">
                                            <i class="fal fa-star clr-f5"></i>
                                            <i class="fal fa-star clr-f5"></i>
                                            <i class="fal fa-star clr-yellow"></i>
                                            <i class="fal fa-star clr-yellow"></i>
                                            <i class="fal fa-star clr-yellow"></i>
                                        </div>
                                        <div class="location-gallery text-right">
                                            <p class="dastnevis clr-ff">خراسان جنوبی - نوفرست</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="thumnail-shop position-absolute">
                                    <a href="#">
                                        <img class="bx-sh" src="images/accessory/shop-auther.jpg" alt="">
                                    </a>
                                </div>
                                <div class="about-shopping text-center position-relative">
                                    <button class="btn btn-secondary btn-round dastnevis fs-16 py-2  my-2 te-sh">نمایش فروشگاه</button>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-md-6 col-lg-4 mb-4">
                            <div class="content-gallery bg-ff bx-sh position-relative">
                                <div class="inner-content-gallery">
                                    <div class="img-gallery">
                                        <a href="#">
                                            <img src="images/accessory/hashimian.jpg" alt="">
                                        </a>
                                    </div>
                                    <div class="inner-gallery pr-3 position-absolute">
                                        <div class="name-gallery text-right">
                                            <a href="#">
                                                <h3 class="dastnevis clr-ff fs-18">ریحانه هاشمیان</h3>
                                            </a>
                                        </div>
                                        <div class="rate-gallery text-right">
                                            <i class="fal fa-star clr-f5"></i>
                                            <i class="fal fa-star clr-f5"></i>
                                            <i class="fal fa-star clr-yellow"></i>
                                            <i class="fal fa-star clr-yellow"></i>
                                            <i class="fal fa-star clr-yellow"></i>
                                        </div>
                                        <div class="location-gallery text-right">
                                            <p class="dastnevis clr-ff">خراسان جنوبی - نوفرست</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="thumnail-shop position-absolute">
                                    <a href="#">
                                        <img class="bx-sh" src="images/accessory/shop-auther.jpg" alt="">
                                    </a>
                                </div>
                                <div class="about-shopping text-center position-relative">
                                    <button class="btn btn-secondary btn-round dastnevis fs-16 py-2  my-2 te-sh">نمایش فروشگاه</button>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-md-6 col-lg-4 mb-4">
                            <div class="content-gallery bg-ff bx-sh position-relative">
                                <div class="inner-content-gallery">
                                    <div class="img-gallery">
                                        <a href="#">
                                            <img src="images/accessory/hashimian.jpg" alt="">
                                        </a>
                                    </div>
                                    <div class="inner-gallery pr-3 position-absolute">
                                        <div class="name-gallery text-right">
                                            <a href="#">
                                                <h3 class="dastnevis clr-ff fs-18">ریحانه هاشمیان</h3>
                                            </a>
                                        </div>
                                        <div class="rate-gallery text-right">
                                            <i class="fal fa-star clr-f5"></i>
                                            <i class="fal fa-star clr-f5"></i>
                                            <i class="fal fa-star clr-yellow"></i>
                                            <i class="fal fa-star clr-yellow"></i>
                                            <i class="fal fa-star clr-yellow"></i>
                                        </div>
                                        <div class="location-gallery text-right">
                                            <p class="dastnevis clr-ff">خراسان جنوبی - نوفرست</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="thumnail-shop position-absolute">
                                    <a href="#">
                                        <img class="bx-sh" src="images/accessory/shop-auther.jpg" alt="">
                                    </a>
                                </div>
                                <div class="about-shopping text-center position-relative">
                                    <button class="btn btn-secondary btn-round dastnevis fs-16 py-2  my-2 te-sh">نمایش فروشگاه</button>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-md-6 col-lg-4 mb-4">
                            <div class="content-gallery bg-ff bx-sh position-relative">
                                <div class="inner-content-gallery">
                                    <div class="img-gallery">
                                        <a href="#">
                                            <img src="images/accessory/hashimian.jpg" alt="">
                                        </a>
                                    </div>
                                    <div class="inner-gallery pr-3 position-absolute">
                                        <div class="name-gallery text-right">
                                            <a href="#">
                                                <h3 class="dastnevis clr-ff fs-18">ریحانه هاشمیان</h3>
                                            </a>
                                        </div>
                                        <div class="rate-gallery text-right">
                                            <i class="fal fa-star clr-f5"></i>
                                            <i class="fal fa-star clr-f5"></i>
                                            <i class="fal fa-star clr-yellow"></i>
                                            <i class="fal fa-star clr-yellow"></i>
                                            <i class="fal fa-star clr-yellow"></i>
                                        </div>
                                        <div class="location-gallery text-right">
                                            <p class="dastnevis clr-ff">خراسان جنوبی - نوفرست</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="thumnail-shop position-absolute">
                                    <a href="#">
                                        <img class="bx-sh" src="images/accessory/shop-auther.jpg" alt="">
                                    </a>
                                </div>
                                <div class="about-shopping text-center position-relative">
                                    <button class="btn btn-secondary btn-round dastnevis fs-16 py-2  my-2 te-sh">نمایش فروشگاه</button>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-md-6 col-lg-4 mb-4">
                            <div class="content-gallery bg-ff bx-sh position-relative">
                                <div class="inner-content-gallery">
                                    <div class="img-gallery">
                                        <a href="#">
                                            <img src="images/accessory/hashimian.jpg" alt="">
                                        </a>
                                    </div>
                                    <div class="inner-gallery pr-3 position-absolute">
                                        <div class="name-gallery text-right">
                                            <a href="#">
                                                <h3 class="dastnevis clr-ff fs-18">ریحانه هاشمیان</h3>
                                            </a>
                                        </div>
                                        <div class="rate-gallery text-right">
                                            <i class="fal fa-star clr-f5"></i>
                                            <i class="fal fa-star clr-f5"></i>
                                            <i class="fal fa-star clr-yellow"></i>
                                            <i class="fal fa-star clr-yellow"></i>
                                            <i class="fal fa-star clr-yellow"></i>
                                        </div>
                                        <div class="location-gallery text-right">
                                            <p class="dastnevis clr-ff">خراسان جنوبی - نوفرست</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="thumnail-shop position-absolute">
                                    <a href="#">
                                        <img class="bx-sh" src="images/accessory/shop-auther.jpg" alt="">
                                    </a>
                                </div>
                                <div class="about-shopping text-center position-relative">
                                    <button class="btn btn-secondary btn-round dastnevis fs-16 py-2  my-2 te-sh">نمایش فروشگاه</button>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-md-6 col-lg-4 mb-4">
                            <div class="content-gallery bg-ff bx-sh position-relative">
                                <div class="inner-content-gallery">
                                    <div class="img-gallery">
                                        <a href="#">
                                            <img src="images/accessory/hashimian.jpg" alt="">
                                        </a>
                                    </div>
                                    <div class="inner-gallery pr-3 position-absolute">
                                        <div class="name-gallery text-right">
                                            <a href="#">
                                                <h3 class="dastnevis clr-ff fs-18">ریحانه هاشمیان</h3>
                                            </a>
                                        </div>
                                        <div class="rate-gallery text-right">
                                            <i class="fal fa-star clr-f5"></i>
                                            <i class="fal fa-star clr-f5"></i>
                                            <i class="fal fa-star clr-yellow"></i>
                                            <i class="fal fa-star clr-yellow"></i>
                                            <i class="fal fa-star clr-yellow"></i>
                                        </div>
                                        <div class="location-gallery text-right">
                                            <p class="dastnevis clr-ff">خراسان جنوبی - نوفرست</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="thumnail-shop position-absolute">
                                    <a href="#">
                                        <img class="bx-sh" src="images/accessory/shop-auther.jpg" alt="">
                                    </a>
                                </div>
                                <div class="about-shopping text-center position-relative">
                                    <button class="btn btn-secondary btn-round dastnevis fs-16 py-2  my-2 te-sh">نمایش فروشگاه</button>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-md-6 col-lg-4 mb-4">
                            <div class="content-gallery bg-ff bx-sh position-relative">
                                <div class="inner-content-gallery">
                                    <div class="img-gallery">
                                        <a href="#">
                                            <img src="images/accessory/hashimian.jpg" alt="">
                                        </a>
                                    </div>
                                    <div class="inner-gallery pr-3 position-absolute">
                                        <div class="name-gallery text-right">
                                            <a href="#">
                                                <h3 class="dastnevis clr-ff fs-18">ریحانه هاشمیان</h3>
                                            </a>
                                        </div>
                                        <div class="rate-gallery text-right">
                                            <i class="fal fa-star clr-f5"></i>
                                            <i class="fal fa-star clr-f5"></i>
                                            <i class="fal fa-star clr-yellow"></i>
                                            <i class="fal fa-star clr-yellow"></i>
                                            <i class="fal fa-star clr-yellow"></i>
                                        </div>
                                        <div class="location-gallery text-right">
                                            <p class="dastnevis clr-ff">خراسان جنوبی - نوفرست</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="thumnail-shop position-absolute">
                                    <a href="#">
                                        <img class="bx-sh" src="images/accessory/shop-auther.jpg" alt="">
                                    </a>
                                </div>
                                <div class="about-shopping text-center position-relative">
                                    <button class="btn btn-secondary btn-round dastnevis fs-16 py-2  my-2 te-sh">نمایش فروشگاه</button>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-md-6 col-lg-4 mb-4">
                            <div class="content-gallery bg-ff bx-sh position-relative">
                                <div class="inner-content-gallery">
                                    <div class="img-gallery">
                                        <a href="#">
                                            <img src="images/accessory/hashimian.jpg" alt="">
                                        </a>
                                    </div>
                                    <div class="inner-gallery pr-3 position-absolute">
                                        <div class="name-gallery text-right">
                                            <a href="#">
                                                <h3 class="dastnevis clr-ff fs-18">ریحانه هاشمیان</h3>
                                            </a>
                                        </div>
                                        <div class="rate-gallery text-right">
                                            <i class="fal fa-star clr-f5"></i>
                                            <i class="fal fa-star clr-f5"></i>
                                            <i class="fal fa-star clr-yellow"></i>
                                            <i class="fal fa-star clr-yellow"></i>
                                            <i class="fal fa-star clr-yellow"></i>
                                        </div>
                                        <div class="location-gallery text-right">
                                            <p class="dastnevis clr-ff">خراسان جنوبی - نوفرست</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="thumnail-shop position-absolute">
                                    <a href="#">
                                        <img class="bx-sh" src="images/accessory/shop-auther.jpg" alt="">
                                    </a>
                                </div>
                                <div class="about-shopping text-center position-relative">
                                    <button class="btn btn-secondary btn-round dastnevis fs-16 py-2  my-2 te-sh">نمایش فروشگاه</button>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-md-6 col-lg-4 mb-4">
                            <div class="content-gallery bg-ff bx-sh position-relative">
                                <div class="inner-content-gallery">
                                    <div class="img-gallery">
                                        <a href="#">
                                            <img src="images/accessory/hashimian.jpg" alt="">
                                        </a>
                                    </div>
                                    <div class="inner-gallery pr-3 position-absolute">
                                        <div class="name-gallery text-right">
                                            <a href="#">
                                                <h3 class="dastnevis clr-ff fs-18">ریحانه هاشمیان</h3>
                                            </a>
                                        </div>
                                        <div class="rate-gallery text-right">
                                            <i class="fal fa-star clr-f5"></i>
                                            <i class="fal fa-star clr-f5"></i>
                                            <i class="fal fa-star clr-yellow"></i>
                                            <i class="fal fa-star clr-yellow"></i>
                                            <i class="fal fa-star clr-yellow"></i>
                                        </div>
                                        <div class="location-gallery text-right">
                                            <p class="dastnevis clr-ff">خراسان جنوبی - نوفرست</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="thumnail-shop position-absolute">
                                    <a href="#">
                                        <img class="bx-sh" src="images/accessory/shop-auther.jpg" alt="">
                                    </a>
                                </div>
                                <div class="about-shopping text-center position-relative">
                                    <button class="btn btn-secondary btn-round dastnevis fs-16 py-2  my-2 te-sh">نمایش فروشگاه</button>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-md-6 col-lg-4 mb-4">
                            <div class="content-gallery bg-ff bx-sh position-relative">
                                <div class="inner-content-gallery">
                                    <div class="img-gallery">
                                        <a href="#">
                                            <img src="images/accessory/hashimian.jpg" alt="">
                                        </a>
                                    </div>
                                    <div class="inner-gallery pr-3 position-absolute">
                                        <div class="name-gallery text-right">
                                            <a href="#">
                                                <h3 class="dastnevis clr-ff fs-18">ریحانه هاشمیان</h3>
                                            </a>
                                        </div>
                                        <div class="rate-gallery text-right">
                                            <i class="fal fa-star clr-f5"></i>
                                            <i class="fal fa-star clr-f5"></i>
                                            <i class="fal fa-star clr-yellow"></i>
                                            <i class="fal fa-star clr-yellow"></i>
                                            <i class="fal fa-star clr-yellow"></i>
                                        </div>
                                        <div class="location-gallery text-right">
                                            <p class="dastnevis clr-ff">خراسان جنوبی - نوفرست</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="thumnail-shop position-absolute">
                                    <a href="#">
                                        <img class="bx-sh" src="images/accessory/shop-auther.jpg" alt="">
                                    </a>
                                </div>
                                <div class="about-shopping text-center position-relative">
                                    <button class="btn btn-secondary btn-round dastnevis fs-16 py-2  my-2 te-sh">نمایش فروشگاه</button>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-md-6 col-lg-4 mb-4">
                            <div class="content-gallery bg-ff bx-sh position-relative">
                                <div class="inner-content-gallery">
                                    <div class="img-gallery">
                                        <a href="#">
                                            <img src="images/accessory/hashimian.jpg" alt="">
                                        </a>
                                    </div>
                                    <div class="inner-gallery pr-3 position-absolute">
                                        <div class="name-gallery text-right">
                                            <a href="#">
                                                <h3 class="dastnevis clr-ff fs-18">ریحانه هاشمیان</h3>
                                            </a>
                                        </div>
                                        <div class="rate-gallery text-right">
                                            <i class="fal fa-star clr-f5"></i>
                                            <i class="fal fa-star clr-f5"></i>
                                            <i class="fal fa-star clr-yellow"></i>
                                            <i class="fal fa-star clr-yellow"></i>
                                            <i class="fal fa-star clr-yellow"></i>
                                        </div>
                                        <div class="location-gallery text-right">
                                            <p class="dastnevis clr-ff">خراسان جنوبی - نوفرست</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="thumnail-shop position-absolute">
                                    <a href="#">
                                        <img class="bx-sh" src="images/accessory/shop-auther.jpg" alt="">
                                    </a>
                                </div>
                                <div class="about-shopping text-center position-relative">
                                    <button class="btn btn-secondary btn-round dastnevis fs-16 py-2  my-2 te-sh">نمایش فروشگاه</button>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-md-6 col-lg-4 mb-4">
                            <div class="content-gallery bg-ff bx-sh position-relative">
                                <div class="inner-content-gallery">
                                    <div class="img-gallery">
                                        <a href="#">
                                            <img src="images/accessory/hashimian.jpg" alt="">
                                        </a>
                                    </div>
                                    <div class="inner-gallery pr-3 position-absolute">
                                        <div class="name-gallery text-right">
                                            <a href="#">
                                                <h3 class="dastnevis clr-ff fs-18">ریحانه هاشمیان</h3>
                                            </a>
                                        </div>
                                        <div class="rate-gallery text-right">
                                            <i class="fal fa-star clr-f5"></i>
                                            <i class="fal fa-star clr-f5"></i>
                                            <i class="fal fa-star clr-yellow"></i>
                                            <i class="fal fa-star clr-yellow"></i>
                                            <i class="fal fa-star clr-yellow"></i>
                                        </div>
                                        <div class="location-gallery text-right">
                                            <p class="dastnevis clr-ff">خراسان جنوبی - نوفرست</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="thumnail-shop position-absolute">
                                    <a href="#">
                                        <img class="bx-sh" src="images/accessory/shop-auther.jpg" alt="">
                                    </a>
                                </div>
                                <div class="about-shopping text-center position-relative">
                                    <button class="btn btn-secondary btn-round dastnevis fs-16 py-2  my-2 te-sh">نمایش فروشگاه</button>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-md-6 col-lg-4 mb-4">
                            <div class="content-gallery bg-ff bx-sh position-relative">
                                <div class="inner-content-gallery">
                                    <div class="img-gallery">
                                        <a href="#">
                                            <img src="images/accessory/hashimian.jpg" alt="">
                                        </a>
                                    </div>
                                    <div class="inner-gallery pr-3 position-absolute">
                                        <div class="name-gallery text-right">
                                            <a href="#">
                                                <h3 class="dastnevis clr-ff fs-18">ریحانه هاشمیان</h3>
                                            </a>
                                        </div>
                                        <div class="rate-gallery text-right">
                                            <i class="fal fa-star clr-f5"></i>
                                            <i class="fal fa-star clr-f5"></i>
                                            <i class="fal fa-star clr-yellow"></i>
                                            <i class="fal fa-star clr-yellow"></i>
                                            <i class="fal fa-star clr-yellow"></i>
                                        </div>
                                        <div class="location-gallery text-right">
                                            <p class="dastnevis clr-ff">خراسان جنوبی - نوفرست</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="thumnail-shop position-absolute">
                                    <a href="#">
                                        <img class="bx-sh" src="images/accessory/shop-auther.jpg" alt="">
                                    </a>
                                </div>
                                <div class="about-shopping text-center position-relative">
                                    <button class="btn btn-secondary btn-round dastnevis fs-16 py-2  my-2 te-sh">نمایش فروشگاه</button>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-md-6 col-lg-4 mb-4">
                            <div class="content-gallery bg-ff bx-sh position-relative">
                                <div class="inner-content-gallery">
                                    <div class="img-gallery">
                                        <a href="#">
                                            <img src="images/accessory/hashimian.jpg" alt="">
                                        </a>
                                    </div>
                                    <div class="inner-gallery pr-3 position-absolute">
                                        <div class="name-gallery text-right">
                                            <a href="#">
                                                <h3 class="dastnevis clr-ff fs-18">ریحانه هاشمیان</h3>
                                            </a>
                                        </div>
                                        <div class="rate-gallery text-right">
                                            <i class="fal fa-star clr-f5"></i>
                                            <i class="fal fa-star clr-f5"></i>
                                            <i class="fal fa-star clr-yellow"></i>
                                            <i class="fal fa-star clr-yellow"></i>
                                            <i class="fal fa-star clr-yellow"></i>
                                        </div>
                                        <div class="location-gallery text-right">
                                            <p class="dastnevis clr-ff">خراسان جنوبی - نوفرست</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="thumnail-shop position-absolute">
                                    <a href="#">
                                        <img class="bx-sh" src="images/accessory/shop-auther.jpg" alt="">
                                    </a>
                                </div>
                                <div class="about-shopping text-center position-relative">
                                    <button class="btn btn-secondary btn-round dastnevis fs-16 py-2  my-2 te-sh">نمایش فروشگاه</button>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 page-number text-center">
                            <nav class="" aria-label="">
                                <ul class="pagination">
                                    <li class="page-item disabled">
                                        <a class="page-link" href="#" tabindex="-1"><i class="fal fa-chevron-right"></i></a>
                                    </li>
                                    <li class="page-item active"><a class="page-link" href="#">1</a></li>
                                    <li class="page-item "><a class="page-link" href="#">2 <span class="sr-only">(current)</span></a></li>
                                    <li class="page-item"><a class="page-link" href="#">3</a></li>
                                    <li class="page-item"><a class="page-link" href="#">4</a></li>
                                    <li class="page-item"><a class="page-link" href="#">5</a></li>
                                    <li class="page-item"><a class="page-link" href="#">6</a></li>
                                    <li class="page-item"><a class="page-link" href="#"><i class="fal fa-chevron-left"></i></a></li>
                                </ul>
                            </nav>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </section>



@endsection
