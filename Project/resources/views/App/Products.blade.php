@extends('Layouts.App.App')
@section('bodyClass','ArchiveProduct-page')

@section('content')
<!-- Start ArchiveProduct -->
<section class="container-fluid info-box">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="breadcrumb-area text-center bx-sh bg-ff py-2">
                    <nav class="d-inline-block" aria-label="breadcrumb" role="navigation">
                        <ol class="breadcrumb bg-ff dastnevis">
                            <li class="breadcrumb-item"><a class="clr-61" href="#">صفحه اصلی</a></li>
                            <li class="breadcrumb-item"><a class="clr-61" href="#">فروشگاه</a></li>
                            <li class="breadcrumb-item"><a class="clr-61" href="#">زنانه</a></li>
                            <li class="breadcrumb-item"><a class="clr-61" href="#">دکور و تزئینات</a></li>
                            <li class="breadcrumb-item active" aria-current="page">ست هفت سین</li>
                        </ol>
                    </nav>
                    <div class="title-article">
                        <h1 class="dastnevis header-gradient fs-25">ست هفت سین</h1>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="container-fluid cont-archive pt-3 pb-5">
    <div class="container">
        <div class="row">
            <div class="col-lg-3">
                <!-- Filters -->
                @include('Components.App.Product.ProductCheckBoxFillter')
                @include('Components.App.Product.SearchProduct')
                @include('Components.App.Product.BolleanFillter')
                @include('Components.App.Product.PriceRange')
            </div>
            <div class="col-lg-9 product-archive mt-5 mt-lg-0">
                <div class="row">
                    <div class="col-lg-12 text-right">
                        @include('Components.App.Product.ProductSorter')
                    </div>
                    <?php
                        $products = ["test","test","test","test","test","test","test","test","test","test","test","test"]
                    ?>
                    @foreach($products as $product)
                    <div class="col-6 col-md-4">
                        @include('Components.App.Product.ProductCard')
                    </div>
                    @endforeach
                    <div class="col-12 page-number text-center">
                        <nav class="" aria-label="">
                            <ul class="pagination">
                                <li class="page-item disabled">
                                    <a class="page-link" href="#" tabindex="-1"><i class="fal fa-chevron-right"></i></a>
                                </li>
                                <li class="page-item active"><a class="page-link" href="#">1</a></li>
                                <li class="page-item "><a class="page-link" href="#">2 <span class="sr-only">(current)</span></a></li>
                                <li class="page-item"><a class="page-link" href="#">3</a></li>
                                <li class="page-item"><a class="page-link" href="#">4</a></li>
                                <li class="page-item"><a class="page-link" href="#">5</a></li>
                                <li class="page-item"><a class="page-link" href="#">6</a></li>
                                <li class="page-item"><a class="page-link" href="#"><i class="fal fa-chevron-left"></i></a></li>
                            </ul>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection