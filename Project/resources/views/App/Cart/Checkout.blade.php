@extends('Layouts.App.App')
@section('content')


    <section class="container-fluid info-box">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="breadcrumb-area text-center bx-sh bg-ff py-5">
                        <ul class="breadcrumb-pay">
                            <li class="d-inline-block">
                                <a class="clr-61" href="#">سبد خرید</a>
                            </li>
                            <span>/</span>
                            <li class="d-inline-block">
                                <a class="clr-61 active" href="#">جزئیات خرید</a>
                            </li>
                            <span>/</span>
                            <li class="d-inline-block">
                                <a class="clr-61 " href="#">اتمام عملیات خرید</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="container-fluid details-form mb-3 py-5">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="bx-sh det-form bg-ff">
                        <div class="row">
                            <div class="d-none d-md-block col-2"></div>
                            <div class="col-12 col-md-8">
                                <div class="card-body text-right">
                                    <div class="row">
                                        <div class="col-lg-6">
                                            <div class="form-group bmd-form-group">
                                                <div class="input-group">
                                                    <div class="input-group-prepend">
                                                        <div class="input-group-text">
                                                            <i class="fal fa-user fs-20 clr-75"></i>
                                                        </div>
                                                    </div>
                                                    <input type="text" class="form-control" placeholder="نام و نام خانوادگی تحویل گیرنده">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-6">
                                            <div class="form-group bmd-form-group">
                                                <div class="input-group">
                                                    <div class="input-group-prepend">
                                                        <div class="input-group-text">
                                                            <i class="fal fa-mobile-alt fs-20 clr-75"></i>
                                                        </div>
                                                    </div>
                                                    <input type="text" class="form-control" placeholder="شماره همراه ">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-6">
                                            <div class="form-group bmd-form-group">
                                                <div class="input-group">
                                                    <div class="input-group-prepend">
                                                        <div class="input-group-text">
                                                            <i class="fal fa-map-marker-alt fs-20 clr-75"></i>
                                                        </div>
                                                    </div>
                                                    <input type="text" class="form-control" placeholder="استان">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-6">
                                            <div class="form-group bmd-form-group">
                                                <div class="input-group">
                                                    <div class="input-group-prepend">
                                                        <div class="input-group-text">
                                                            <i class="fal fa-map-marked fs-20 clr-75"></i>
                                                        </div>
                                                    </div>
                                                    <input type="text" class="form-control" placeholder="شهر">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-12">
                                            <div class="form-group bmd-form-group">
                                                <div class="input-group">
                                                    <div class="input-group-prepend">
                                                        <div class="input-group-text">
                                                            <i class="fal fa-street-view fs-20 clr-75"></i>
                                                        </div>
                                                    </div>
                                                    <input type="text" class="form-control" placeholder="آدرس پستی">
                                                </div>
                                            </div>

                                        </div>
                                        <div class="col-lg-6">
                                            <div class="form-group bmd-form-group">
                                                <div class="input-group">
                                                    <div class="input-group-prepend">
                                                        <div class="input-group-text">
                                                            <i class="fal fa-barcode-read fs-20 clr-75"></i>
                                                        </div>
                                                    </div>
                                                    <input type="text" class="form-control" placeholder="کد پستی">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-6">
                                            <div class="form-group bmd-form-group">
                                                <div class="input-group">
                                                    <div class="input-group-prepend">
                                                        <div class="input-group-text">
                                                            <i class="fal fa-envelope fs-20 clr-75"></i>
                                                        </div>
                                                    </div>
                                                    <input type="text" class="form-control" placeholder="پست الکترونیک">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-12 user-note">
                                            <div class="form-group bmd-form-group">
                                                <div class="input-group">
                                                    <div class="input-group-prepend ">
                                                        <div class="input-group-text">
                                                            <i class="fal fa-file-signature fs-20 clr-75"></i>
                                                        </div>
                                                    </div>
                                                    <textarea type="text" class="form-control" placeholder="یادداشت" rows="4"></textarea>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-12 text-center mt-5">
                                            <button class="btn btn-checkout btn-success btn-round dastnevis btn-circle position-relative fs-16">
                                                <i class="fal fa-credit-card clr-green fs-20 position-relative"></i>
                                                ادامه ثبت خرید محصول
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="d-none d-md-block col-2"></div>
                        </div>

                    </div>

                </div>

            </div>
        </div>


    </section>

@endsection
