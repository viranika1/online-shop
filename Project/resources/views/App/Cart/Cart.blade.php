@extends('Layouts.App.App')
@section('content')


    <section class="container-fluid info-box">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="breadcrumb-area text-center bx-sh bg-ff py-5">
                        <ul class="breadcrumb-pay">
                            <li class="d-inline-block">
                                <a class="clr-61 active" href="#">سبد خرید</a>
                            </li>
                            <span>/</span>
                            <li class="d-inline-block">
                                <a class="clr-61" href="#">جزئیات خرید</a>
                            </li>
                            <span>/</span>
                            <li class="d-inline-block">
                                <a class="clr-61 " href="#">اتمام عملیات خرید</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="container-fluid cart mb-3 py-5">
        <div class="container">
            <div class="row mb-3">
                <div class="col-lg-9 col-md-8">
                    <div class="bg-ff bx-sh px-2 py-3">
                        <div class="cart-list text-center">
                            <div class="cart-item-list fs-14 clr-75 text-center">
                                <div class="ci-content">
                                    <div class="ci-title">
                                        <div class="ci-item ci-delet">

                                        </div>
                                        <div class="ci-item ci-name">
                                            محصول
                                        </div>
                                        <div class="ci-item ci-price">
                                            قیمت
                                        </div>
                                        <div class="ci-item ci-count">
                                            تعداد
                                        </div>
                                        <div class="ci-item ci-final-price">
                                            قیمت کل
                                        </div>
                                    </div>
                                    <div class="ci-details pt-2">
                                        <div class="cid-item">
                                            <div class="ci-item ci-delet opd opdn">
                                                <a href="" class="fs-20 clr-75 text-center">
                                                    <i class="fal fa-trash-alt"></i>
                                                </a>
                                            </div>
                                            <div class="ci-item ci-name opd">
                                                <a href="">
                                                    <div class="wl-img bx-sh p-2 ml-3">
                                                        <img src="images/accessory/01.png">
                                                    </div>
                                                    <div class="wl-txt text-right">
                                                        <p class="wl-title clr-42">
                                                            عنوان تستی برای محصول
                                                        </p>
                                                    </div>
                                                </a>
                                            </div>
                                            <div class="ci-item ci-price opd clr-75">
                                                120000 تومان
                                            </div>
                                            <div class="ci-item ci-count opd">
                                                پرداخت موفق
                                            </div>
                                            <div class="ci-item ci-final-price opd text-danger">
                                                240000 تومان
                                            </div>
                                        </div>
                                        <div class="cid-item">
                                            <div class="ci-item ci-delet opd opdn">
                                                <a href="" class="fs-20 clr-75 text-center">
                                                    <i class="fal fa-trash-alt"></i>
                                                </a>
                                            </div>
                                            <div class="ci-item ci-name opd">
                                                <a href="">
                                                    <div class="wl-img bx-sh p-2 ml-3">
                                                        <img src="images/accessory/01.png">
                                                    </div>
                                                    <div class="wl-txt text-right">
                                                        <p class="wl-title clr-42">
                                                            عنوان تستی برای محصول
                                                        </p>
                                                    </div>
                                                </a>
                                            </div>
                                            <div class="ci-item ci-price opd clr-75">
                                                120000 تومان
                                            </div>
                                            <div class="ci-item ci-count opd">
                                                پرداخت موفق
                                            </div>
                                            <div class="ci-item ci-final-price opd text-danger">
                                                240000 تومان
                                            </div>
                                        </div>
                                        <div class="cid-item">
                                            <div class="ci-item ci-delet opd opdn">
                                                <a href="" class="fs-20 clr-75 text-center">
                                                    <i class="fal fa-trash-alt"></i>
                                                </a>
                                            </div>
                                            <div class="ci-item ci-name opd">
                                                <a href="">
                                                    <div class="wl-img bx-sh p-2 ml-3">
                                                        <img src="images/accessory/01.png">
                                                    </div>
                                                    <div class="wl-txt text-right">
                                                        <p class="wl-title clr-42">
                                                            عنوان تستی برای محصول
                                                        </p>
                                                    </div>
                                                </a>
                                            </div>
                                            <div class="ci-item ci-price opd clr-75">
                                                120000 تومان
                                            </div>
                                            <div class="ci-item ci-count opd">
                                                پرداخت موفق
                                            </div>
                                            <div class="ci-item ci-final-price opd text-danger">
                                                240000 تومان
                                            </div>
                                        </div>
                                        <div class="cid-item">
                                            <div class="ci-item ci-delet opd opdn">
                                                <a href="" class="fs-20 clr-75 text-center">
                                                    <i class="fal fa-trash-alt"></i>
                                                </a>
                                            </div>
                                            <div class="ci-item ci-name opd">
                                                <a href="">
                                                    <div class="wl-img bx-sh p-2 ml-3">
                                                        <img src="images/accessory/01.png">
                                                    </div>
                                                    <div class="wl-txt text-right">
                                                        <p class="wl-title clr-42">
                                                            عنوان تستی برای محصول
                                                        </p>
                                                    </div>
                                                </a>
                                            </div>
                                            <div class="ci-item ci-price opd clr-75">
                                                120000 تومان
                                            </div>
                                            <div class="ci-item ci-count opd">
                                                پرداخت موفق
                                            </div>
                                            <div class="ci-item ci-final-price opd text-danger">
                                                240000 تومان
                                            </div>
                                        </div>
                                        <div class="cid-item">
                                            <div class="ci-item ci-delet opd opdn">
                                                <a href="" class="fs-20 clr-75 text-center">
                                                    <i class="fal fa-trash-alt"></i>
                                                </a>
                                            </div>
                                            <div class="ci-item ci-name opd">
                                                <a href="">
                                                    <div class="wl-img bx-sh p-2 ml-3">
                                                        <img src="images/accessory/01.png">
                                                    </div>
                                                    <div class="wl-txt text-right">
                                                        <p class="wl-title clr-42">
                                                            عنوان تستی برای محصول
                                                        </p>
                                                    </div>
                                                </a>
                                            </div>
                                            <div class="ci-item ci-price opd clr-75">
                                                120000 تومان
                                            </div>
                                            <div class="ci-item ci-count opd">
                                                پرداخت موفق
                                            </div>
                                            <div class="ci-item ci-final-price opd text-danger">
                                                240000 تومان
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>


                            <button class="btn btn-danger btn-round btn-circle circle-1 position-relative fs-16 dastnevis py-2 mt-3">
                                <i class="fal fa-store clr-red fs-20 position-relative"></i>
                                ادامه خرید
                            </button>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-4">
                    <div class="bg-ff bx-sh py-3 px-2">
                        <div class="cart-left-item pb-3">
                            <div class="title-products text-center">
                                <h3 class="pro-title fs-14 iransans header-gradient d-inline-block px-4 px-md-4 mb-0 mt-0 pb-2">جمع کل سبد خرید</h3>
                                <hr class="under-title">
                            </div>
                            <div class="final-cart-price fs-14 text-center pt-3">
                                <div class="fp py-2">
                                    مبلغ کل  <span>(3 کالا)</span> :  <span class="clr-75">15000</span><span class="clr-75">تومان</span>
                                </div>
                                <div class="fp py-2 ">
                                    سود شما از خرید : <span class="clr-75">15000</span><span class="clr-75">تومان</span>(<span class="clr-75">12%</span>)
                                </div>
                                <div class="fp py-2">
                                    هزینه ارسال : <span class="clr-75">10000</span><span class="clr-75">تومان</span>
                                </div>
                            </div>
                        </div>
                        <div class="cart-left-item pb-3">
                            <div class="title-products text-center">
                                <h3 class="pro-title fs-14 iransans header-gradient d-inline-block px-4 px-md-4 mb-0 mt-0 pb-2">مبلغ قابل پرداخت</h3>
                                <hr class="under-title">
                            </div>
                            <div class="final-cart-price text-danger text-center pt-3">
                                139000 تومان
                                <button class="btn btn-success btn-round btn-circle circle-1 position-relative fs-16 dastnevis py-2 mt-3">
                                    <i class="fal fa-credit-card clr-green fs-20 position-relative"></i>
                                    نهایی کردن خرید
                                </button>
                            </div>
                        </div>
                        <div class="cart-left-item pb-3">
                            <div class="title-products text-center">
                                <h3 class="pro-title fs-14 iransans header-gradient d-inline-block px-4 px-md-4 mb-0 mt-0 pb-2">کد تخفیف</h3>
                                <hr class="under-title">
                            </div>
                            <form>
                                <div class="form-group bmd-form-group">
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <div class="input-group-text">
                                                <i class="fal fa-percent"></i>
                                            </div>
                                        </div>
                                        <input type="text" class="form-control" placeholder="کد تخفیف را وارد کنید">
                                    </div>
                                    <button type="submit" class="btn btn-block pro-frm-btn">اعمال تخفیف</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>



@endsection
