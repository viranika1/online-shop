@extends('Layouts.App.App')
@section('bodyClass','SProduct-page')
@section('pageStyle')
    <link rel="stylesheet" type="text/css" href="css/lightgallery.css">
    <link rel="stylesheet" type="text/css" href="css/justifiedGallery.min.css">
    <link rel="stylesheet" type="text/css" href="css/animate.css">
@endsection
@section('pageScript')
    <script type="text/javascript" src="js/lg/jquery.justifiedGallery.min.js"></script>
    <script type="text/javascript" src="js/lg/collapse.js"></script>
    <script type="text/javascript" src="js/lg/lightgallery.js"></script>
    <script type="text/javascript" src="js/lg/lg-fullscreen.js"></script>
    <script type="text/javascript" src="js/lg/lg-thumbnail.js"></script>
    <script type="text/javascript" src="js/lg/lg-zoom.js"></script>
    <script type="text/javascript" src="js/lg/demos.js"></script>


    <!-- Script -->
    <script type="text/javascript">
        $('#aniimated-thumbnials').lightGallery({
            thumbnail:true
        });
    </script>
    <!-- Script -->
@endsection

@section('content')
    <!-- Start SingleProduct -->
    <section class="body SingleProduct">
        <section class="container-fluid breadcrumb-product">
            <div class="container">
                <div class="row">
                    <div class="col-12 text-right">
                        <nav aria-label="breadcrumb" role="navigation">
                            <ol class="breadcrumb bg-fd dastnevis">
                                <li class="breadcrumb-item"><a class="clr-61" href="#">صفحه اصلی</a></li>
                                <li class="breadcrumb-item"><a class="clr-61" href="#">فروشگاه</a></li>
                                <li class="breadcrumb-item"><a class="clr-61" href="#">ست هفت سین</a></li>
                                <li class="breadcrumb-item active" aria-current="page">کاسه خوشکل موشگل</li>
                            </ol>
                        </nav>
                    </div>
                </div>
            </div>
        </section>
        @include('Sections.App.Product.MainProduct')
        @include('Sections.App.Product.Tabs')
        <!-- row-product -->
        <?php
        $slider =
            '{
            "name" : "محصولات مرتبط000000",
            "cssClass" : "bg-product related-product",
            "link" : "#000",
            "products" : ["test","test","test","test","test","test"]
            }';
        $slider = json_decode($slider);
        ?>
        @include('Sections.App.Product.ProductSlider')
    </section>
@endsection