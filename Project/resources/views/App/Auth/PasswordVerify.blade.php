@extends('Layouts.App.Auth')
@section('content')

        <h1 class="clr-42 text-center fs-18 iransans mb-3 mt-1"> تایید شماره تلفن همراه</h1>
        <p class="p-3 iransans text-center message-text">برای شماره همراه
            <span>09150848985</span>
            کد تایید ارسال گردید
        </p>
        <div class="form-group bmd-form-group">
            <div class="input-group">
                <div class="input-group-prepend">
                    <div class="input-group-text pr-0">
                        <i class="fal fa-hashtag fs-20 clr-92"></i>
                    </div>
                </div>
                <input type="text" class="form-control" placeholder="به طور مثال : 232125">
            </div>
        </div>
        <div class="btn-filter text-center mt-4">
            <button class="btn btn-info btn-round dastnevis btn-circle position-relative fs-16">
                <i class="fal fa-lock-alt clr-gold fs-18 position-relative"></i>
                یادآور کلمه عبور
            </button>
        </div>
        <hr>
        <p class="fs-13 clr-61 text-center px-3">کد تایید تا حداکثر یک دقیقه دیگر برایتان ارسال خواهد شد.</p>

@endsection
