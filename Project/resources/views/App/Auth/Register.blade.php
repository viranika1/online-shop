@extends('Layouts.App.Auth')
@section('content')
    <form action="{{route("register.store")}}" method="post">
        @csrf
        @if($errors->any())
            <div class="alert alert-danger" style="text-align: right">
                <ul>
                    @foreach($errors->all() as $error)
                        <li>{{$error}}</li>
                    @endforeach
                </ul>

            </div>
        @endif
        <div class="form-group bmd-form-group">
            <div class="input-group">
                <div class="input-group-prepend">
                    <div class="input-group-text pr-0">
                        <i class="fal fa-envelope fs-20 clr-92"></i>
                    </div>
                </div>
                <input name="email" type="text" class="form-control" placeholder="پست الکترونیک یا شماره همراه">
            </div>
        </div>
        <div class="form-group bmd-form-group">
            <div class="input-group">
                <div class="input-group-prepend">
                    <div class="input-group-text pr-0">
                        <i class="fal fa-lock-alt fs-20 clr-92"></i>
                    </div>
                </div>
                <input name="password" type="text" class="form-control" placeholder="رمز عبور">
            </div>
        </div>
        <div class="form-check remember">
            <label class="form-check-label d-block my-3 text-right fs-12">
                <p class="fs-12">
                    <a class="clr-blue2 Ho-roles" href="#">قوانین و مقررات</a>
                    را مطالعه نمودم و با آن موافقم
                </p>
                <input class="form-check-input" name="rules" type="checkbox" >
                <span class="form-check-sign">
										<span class="check"></span>
									</span>
            </label>
        </div>

        <div class="btn-filter text-center mt-4">
            <button class="btn btn-info btn-round dastnevis btn-circle position-relative fs-16">
                <i class="fal fa-user-plus clr-gold fs-18 position-relative"></i>
                عضویت در هنرچی
            </button>
        </div>
    </form>
    <hr>
    <p class="fs-12 text-center">قبلا در هنرچی ثبت نام کرده اید ؟
        <a class="clr-blue2" href="#">وارد شوید</a>

    </p>

@endsection
