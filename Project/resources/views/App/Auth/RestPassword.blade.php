@extends('Layouts.App')
@section('content')

        <form method="post"  action="{{ route('password.update') }}">
            @csrf

            <div class="form-group bmd-form-group">
                <div class="input-group">
                    <div class="input-group-prepend">
                        <div class="input-group-text pr-0">
                            <i class="fal fa-envelope fs-20 clr-92"></i>
                        </div>
                    </div>
                    <input type="text" class="form-control @error('email') is-invalid @enderror" placeholder="پست الکترونیک یا شماره همراه" name="email" value="{{ $email ?? old('email') }}">
                    @error('email')
                    <span class="invalid-feedback" role="alert" style="text-align: right">
                                        <strong>{{ $message }}</strong>
                                    </span>
                    @enderror
                </div>
            </div>


            <div class="btn-filter text-center mt-4">

                <button class="btn btn-info btn-round dastnevis btn-circle position-relative fs-16">
                    <i class="fal fa-lock-alt clr-gold fs-18 position-relative"></i>
                    ارسال کلمه عبور
                </button>
            </div>
            <hr>
            <p class="fs-13 clr-61 text-center px-3">رمز عبور به پست الکترونیک یا شماره همراه وارد شده ارسال میگردد</p>



        </form>


@endsection
