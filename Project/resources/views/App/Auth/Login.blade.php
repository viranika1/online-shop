@extends('Layouts.App.Auth')
@section('content')

<form action="{{route('login')}}" method="post" >
    @csrf
    @if($errors->any())
        <div class="alert alert-danger" style="text-align: right">
            <ul>
                @foreach($errors->all() as $error)
                    <li>{{$error}}</li>
                @endforeach
            </ul>

        </div>
    @endif
    <div class="form-group bmd-form-group">
        <div class="input-group">
            <input type="text" name="email"  class="form-control" placeholder="پست الکترونیک یا شماره همراه">
        </div>
    </div>
    <div class="form-group bmd-form-group">
        <div class="input-group">
            <div class="input-group-prepend">
                <div class="input-group-text pr-0">
                    <i class="fal fa-lock-alt fs-20 clr-92"></i>
                </div>
            </div>
            <input type="text" class="form-control" name="password" placeholder="رمز عبور">
        </div>
    </div>
    <div class="form-check remember">
        <label class="form-check-label d-block my-3 text-right fs-12">	مرا بخاطر بسپار
            <input class="form-check-input" type="checkbox" checked name="remember">
            <span class="form-check-sign">
										<span class="check"></span>
									</span>
        </label>
    </div>
    <small id="emailHelp" class="form-text text-muted">
        <a class="clr-blue2" href="#">رمز عبور خود را فراموش کرده ام</a>
    </small>
    <div class="btn-filter text-center mt-4">
        <button class="btn btn-info btn-round dastnevis btn-circle position-relative fs-16">
            <i class="fal fa-sign-in clr-gold fs-18 position-relative"></i>
            ورود به هنرچی
        </button>
    </div>
</form>

@endsection
