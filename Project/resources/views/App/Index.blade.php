@extends('Layouts.App.App')
@section('bodyClass','home-page')
@section('content')
    <!-- Start Slider -->
    @include('Components.App.Index.Slider')
    <!-- End Slider -->
    <!-- Start Services & real chanse -->
    @include('Sections.App.Index.ServisesAndChances')
    <!-- End Services & real chanse -->
    <!-- Start Gifts -->
    @include('Sections.App.Index.Gifts')
    <!-- End Gifts -->
    <?php
    $slider=
        [
         "name" => "آخرین محصولات",
            "cssClass" => "bg-product related-product",
            "link" => "#000",
            "products"=>$products
        ];
    $slider = (object) $slider;

    ?>
    @include('Sections.App.Product.ProductSlider')

    <?php
    $slider =
        '{
            "name" : "پرفروش ترین محصولات",
            "cssClass" : "bg-product related-product",
            "link" : "#000",
            "products" : ["test","test","test","test","test","test","test","test","test","test","test","test"]
            }';
    $slider = json_decode($slider);
    ?>
    @include('Sections.App.Product.ProductSlider')
@endsection
