<?php
$user= Auth::user();
function userData(string $filed ){
    $user= Auth::user();
    if(old($filed) == null)
    {
        return $user->$filed;
    }
    return old($filed);
}
?>

@extends('Layouts.App.Profile')
@section('profile-content')
    <div class="title-products text-right">
        <h3 class="pro-title fs-18 dastnevis header-gradient d-inline-block pl-4 pl-md-4 pr-2 mb-0 mt-0 pb-2">
            <a href="" class="px-2">
                <i class="fal fa-arrow-right"></i>
            </a>
            ویرایش اطلاعات شخصی </h3>
        <hr class="under-title">
    </div>
    <div class="pbox bg-ff bx-sh px-3 py-3">
        <form method="post" action="{{route('profile.updateUser')}}">
            @csrf
            {{method_field('PATCH')}}
            <div class="form-group bmd-form-group my-4">
                <label class="text-right bmd-label-floating">نام</label>
                <input type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{userData('name')}}" >
                @error('name')
                <span class="invalid-feedback" role="alert" style="text-align: right">
                                        <strong>{{ $message }}</strong>
                                    </span>
                @enderror
            </div>
            <div class="form-group bmd-form-group my-4">
                <label class="text-right bmd-label-floating">نام خانوادگی</label>
                <input type="text" class="form-control @error('lastName') is-invalid @enderror" name="lastName" value="{{userData('lastName')}}">
                @error('lastName')
                <span class="invalid-feedback" role="alert" style="text-align: right">
                                        <strong>{{ $message }}</strong>
                                    </span>
                @enderror
            </div>
            <div class="form-group bmd-form-group my-4">
                <label class="text-right bmd-label-floating">کد ملی</label>
                <input type="text" class="form-control @error('nationalCode') is-invalid @enderror" name="nationalCode" value="{{userData('nationalCode')}}">
                @error('nationalCode')
                <span class="invalid-feedback" role="alert" style="text-align: right">
                                        <strong>{{ $message }}</strong>
                                    </span>
                @enderror
            </div>
            <div class="form-group bmd-form-group my-4">
                <label class="text-right bmd-label-floating">شماره کارت</label>
                <input type="text" name="cardNumber" class="form-control @error('cardNumber') is-invalid @enderror" value="{{userData('cardNumber')}}">
                @error('cardNumber')
                <span class="invalid-feedback" role="alert" style="text-align: right">
                                        <strong>{{ $message }}</strong>
                                    </span>
                @enderror
            </div>
            <div class="form-group bmd-form-group my-4">
                <label class="text-right bmd-label-floating">شماره موبایل</label>
                <input type="text" class="form-control @error('mobile') is-invalid @enderror" name="mobile" value="{{userData('mobile')}}">
                @error('mobile')
                <span class="invalid-feedback" role="alert" style="text-align: right">
                                        <strong>{{ $message }}</strong>
                                    </span>
                @enderror
            </div>
            <div class="form-group bmd-form-group my-4">
                <label class="text-right bmd-label-floating">پست الکترونیک</label>
                <input type="email" class="form-control text-left @error('email') is-invalid @enderror" name="email" value="{{userData('email')}}">
                @error('email')
                <span class="invalid-feedback" role="alert" style="text-align: right">
                                        <strong>{{ $message }}</strong>
                                    </span>
                @enderror
            </div>
            <div class="form-check py-2">
                <label class="form-check-label">
                    عضویت در خبرنامه هنرچی
                    <input class="form-check-input @error('newsletter') is-invalid @enderror" type="checkbox" {{$user->newsletter === 1 ? 'checked':""}}>
                    <span class="form-check-sign">
				              <span class="check"></span>
				          </span>
                </label>
                @error('newsletter')
                <span class="invalid-feedback" role="alert" style="text-align: right">
                                        <strong>{{ $message }}</strong>
                                    </span>
                @enderror
            </div>
            <div class="form-group bmd-form-group my-2 text-left my-3">
                <button type="submit" class="btn pro-frm-btn">ثبت اطلاعات کاربر</button>

                  <button type="reset" class="btn pro-frm-btn">انصراف</button>


            </div>
        </form>
    </div>

@endsection
