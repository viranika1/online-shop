<?php
$user = Auth::user();
function userData(string $filed)
{
	$user = Auth::user();
	if (old($filed) == null) {
		return $user->$filed;
	}
	return old($filed);
}

?>
@extends('Layouts.App.Profile')
@section('profile-content')

	<div class="profile-box">
		<div class="title-products text-right">
			<h3 class="pro-title fs-18 dastnevis header-gradient d-inline-block px-4 px-md-4 mb-0 mt-0 pb-2">آدرس ها</h3>
			<hr class="under-title">
		</div>
		<div class="pbox bg-ff bx-sh px-3 py-3">
			@foreach($addresses as $address)
				<div class="address_body">
					<div class="row text-right">
						<div class="col-lg-12 col-md-12 col-sm-12 my-1">
							گیرنده:<span class="fs-16 m-0 address_receiver">{{$address->receiver}}</span>
						</div>

						<div class="col-lg-12 col-md-12 col-sm-12 my-1">
                            <span class="address_state">{{ $address->state}}</span> -
                            <span class="address_city">{{ $address->city}}</span>
							<p class="fs-16 m-0 address_detail">{{$address->city}}<br>{{$address->address}}</p>
						</div>
					</div>
					<div class="ab-hr d-none d-lg-block"></div>
					<div class="col-lg-12 col-md-12 col-sm-12 my-2">
						<i class="fal fa-barcode-read fs-20 d-inline-block"></i>
						<p class="fs-14 m-0 mr-2 d-inline-block">کد پستی :</p>
						<p class="fs-14 m-0 mr-2 d-inline-block address_post_code">{{$address->postCode}}</p>
					</div>
					<div class="col-lg-12 col-md-12 col-sm-12 my-2">
						<i class="fal fa-mobile-alt fs-25 mr-1 d-inline-block"></i>
						<p class="fs-14 m-0 mr-2 d-inline-block">تلفن همراه :</p>
						<p class="fs-14 m-0 mr-2 d-inline-block address_mobile">{{$address->mobile}}</p>
					</div>
					<div class="text-left">
						<a class="editAddressAjax" href="javascript:;" editUrl="{{route('profile.address.edit',['address'=>$address->id])}}">
							<button class="btn pro-frm-btn">
								ویرایش
							</button>
						</a>
						<form action="{{route('profile.address.destroy',['address'=>$address->id])}}" method="post" style="display: contents">
							@csrf
							{{method_field('DELETE')}}
							<a>
								<button class="btn pro-frm-btn">حذف</button>
							</a>
						</form>


					</div>
				</div>
			@endforeach
		</div>

	</div>
	<button class="btn btn-round add-address-btn p-4 my-4 clr-75 fs-18" data-toggle="modal" data-target="#add-address">
		<i class="fal fa-map-marker-plus d-block my-1"></i>
		افزودن آدرس جدید
	</button>
	<div class="modal fade" id="add-address" tabindex="-1" role="">
		<div class="modal-dialog modal-login modal-lg" role="document">
			<div class="modal-content">
				<div class="card card-signup card-plain">
					<div class="modal-header">
						<button type="button" class="close mt-0" data-dismiss="modal" aria-label="Close">
							<i class="fal fa-times"></i>
						</button>
					</div>
					<div class="modal-body">
						<form class="form" method="post" action="{{route('profile.address.store')}}" enctype="multipart/form-data">
							@csrf
							<p class="text-center">
								<img src="images/logo.svg" alt="هنرچی" style="width: 250px;">
							</p>
							<div class="card-body text-right">
								<div class="col-lg-12 col-md-12 col-sm-12 my-2">
									<i class="fal fa-map-marked-alt fs-20 d-inline-block"></i>
									<p class="fs-14 m-0 mr-2 d-inline-block">افزودن آدرس جدید</p>
								</div>

								<div class="form-group bmd-form-group">
									<div class="input-group">
										<div class="input-group-prepend">
											<div class="input-group-text">
												<i class="fal fa-user"></i>
											</div>
										</div>
										<input type="text" name="receiver" class="form-control @error('receiver') is-invalid @enderror" value="{{old('receiver')}}" placeholder="نام و نام خانوادگی گیرنده">
										@error('receiver')
										<span class="invalid-feedback" role="alert" style="text-align: right">
                                            <strong>{{ $message }}</strong>
                                        </span>
										@enderror
                                    </div>
								</div>
								<div class="form-group bmd-form-group">
									<div class="input-group">
										<div class="input-group-prepend">
											<div class="input-group-text">
												<i class="fal fa-mobile-alt"></i>
											</div>
										</div>
										<input type="text" name="mobile" class="form-control @error('mobile') is-invalid @enderror" value="{{old('mobile')}}" placeholder="شماره همراه">
										@error('mobile')
										<span class="invalid-feedback" role="alert" style="text-align: right">
                                            <strong>{{ $message }}</strong>
                                        </span>
										@enderror
                                    </div>
								</div>
								<div class="form-group bmd-form-group">
									<div class="input-group">
										<div class="input-group-prepend">
											<div class="input-group-text">
												<i class="fal fa-mobile-alt"></i>
											</div>
										</div>
										<input type="text" name="phone" class="form-control @error('phone') is-invalid @enderror" value="{{old('phone')}}" placeholder="تلفن ثابت">
										@error('phone')
										<span class="invalid-feedback" role="alert" style="text-align: right">
                                        <strong>{{ $message }}</strong>
                                        </span>
										@enderror
                                    </div>
								</div>
								<div class="form-group bmd-form-group">
									<div class="input-group">
										<div class="input-group-prepend">
											<div class="input-group-text">
												<i class="fal fa-map-marker-alt"></i>
											</div>
										</div>
										<input type="text" name="state" class="form-control @error('state') is-invalid @enderror" value="{{old('state')}}" placeholder="استان">
										@error('state')
										<span class="invalid-feedback" role="alert" style="text-align: right">
                                            <strong>{{ $message }}</strong>
                                        </span>
										@enderror
                                    </div>
								</div>
								<div class="form-group bmd-form-group">
									<div class="input-group">
										<div class="input-group-prepend">
											<div class="input-group-text">
												<i class="fal fa-map-marked"></i>
											</div>
										</div>
										<input type="text" name="city" class="form-control @error('city') is-invalid @enderror" value="{{old('city')}}" placeholder="شهر">
										@error('city')
										<span class="invalid-feedback" role="alert" style="text-align: right">
                                            <strong>{{ $message }}</strong>
                                        </span>
										@enderror
                                    </div>
								</div>
								<div class="form-group bmd-form-group">
									<div class="input-group">
										<div class="input-group-prepend">
											<div class="input-group-text">
												<i class="fal fa-street-view"></i>
											</div>
										</div>
										<input type="text" name="address" class="form-control @error('address') is-invalid @enderror" value="{{old('address')}}" placeholder="آدرس گیرنده">
										@error('address')
										<span class="invalid-feedback" role="alert" style="text-align: right">
                                            <strong>{{ $message }}</strong>
                                        </span>
										@enderror
                                    </div>
								</div>
								<div class="form-group bmd-form-group">
									<div class="input-group">
										<div class="input-group-prepend">
											<div class="input-group-text">
												<i class="fal fa-barcode-read"></i>
											</div>
										</div>
										<input type="text" name="postCode" class="form-control @error('postCode') is-invalid @enderror" value="{{old('postCode')}}" placeholder="کد پستی">
										@error('postCode')
										<span class="invalid-feedback" role="alert" style="text-align: right">
                                            <strong>{{ $message }}</strong>
                                        </span>
										@enderror
                                    </div>
								</div>
								<div class="form-group bmd-form-group">
									<div class="input-group">
										<div class="input-group-prepend">
											<div class="input-group-text">
												<i class="fal fa-text-width" style="margin-bottom: auto;margin-top: 5px"></i>
											</div>
										</div>
										<textarea class="form-control" name="description" rows="3" placeholder="توضیحات اضافه" id="des"></textarea>
									</div>
								</div>
							</div>
							<div class="modal-footer justify-content-center">
								<button type="submit" class="btn pro-frm-btn px-4">ثبت آدرس</button>
							</div>
						</form>
					</div>

				</div>
			</div>
		</div>
	</div>







@endsection
@section('pageScript')
	<script>
        var currentEdit;
        var createAddress = $("#add-address form").clone();
        var ajaxPost = function (f) {
            $.post(f.attr("action"), f.serialize(), function (data) {
                let formData = new FormData($(f)[0]);
                $("#add-address .modal-body").empty();
                let form = $(data)[0];
                if (data === 'Ok') {
                    currentEdit.find(".address_receiver").text(formData.get('receiver'));
                    currentEdit.find(".address_state").text(formData.get('state'));
                    currentEdit.find(".address_city").text(formData.get('city'));
                    currentEdit.find(".address_detail").text(formData.get('address'));
                    currentEdit.find(".address_mobile").text(formData.get('mobile'));
                    currentEdit.find(".address_post_code").text(formData.get('postCode'));
                    $("#add-address div.modal-header > button").click();
                    $("#add-address .modal-body").append(createAddress);
                } else {
                    $("#add-address .modal-body").html(form);
                }


            })
        };
        $(document).ready(function () {
            $(".editAddressAjax").on("click", function () {
                let url = $(this).attr('editUrl');
                currentEdit = $(this).closest("div.address_body")
                let xhttp = new XMLHttpRequest();
                xhttp.onreadystatechange = function () {
                    if (this.readyState == 4 && this.status == 200) {
                        $("#add-address .modal-body").empty();
                        let form = $(this.responseText)[0];
                        $("#add-address .modal-body").html(form);
                        $("body > section.container-fluid.profile-page > div > div > div.col-lg-9.col-md-8.mt-2.p-left > button").click();
                        $("#add-address form").on("submit", function (e) {
                            e.preventDefault();
                            ajaxPost($(form))
                        })
                    }
                };
                xhttp.open('GET', url, true);
                xhttp.send();
            });

            $("#add-address form").on("submit", function (e) {
                e.preventDefault();
            })
        })
	</script>
@endsection
