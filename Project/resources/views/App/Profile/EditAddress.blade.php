@php
	function data($filed , $data , $address){
		if (!array_key_exists($filed, $data))
			{
				return $address->$filed;
			}
		return $data[$filed];
	}
@endphp


<form class="form" method="post" action="{{ $route }}" enctype="multipart/form-data">
	@csrf
	@isset($method)
		@method($method)
	@endisset
	<p class="text-center">
		<img src="images/logo.svg" alt="هنرچی" style="width: 250px;">
	</p>
	<div class="card-body text-right">
		<div class="col-lg-12 col-md-12 col-sm-12 my-2">
			<i class="fal fa-map-marked-alt fs-20 d-inline-block"></i>
			<p class="fs-14 m-0 mr-2 d-inline-block">ویرایش آدرس</p>
		</div>

		<div class="form-group bmd-form-group">
			<div class="input-group">
				<div class="input-group-prepend">
					<div class="input-group-text">
						<i class="fal fa-user"></i>
					</div>
				</div>
				<input type="text" name="receiver" class="form-control @error('receiver') is-invalid @enderror" value="{{data('receiver', $data , $address)}}" placeholder="نام و نام خانوادگی گیرنده">
				@error('receiver')
				<span class="invalid-feedback" role="alert" style="text-align: right">
                                        <strong>{{ $message }}</strong>
                                    </span>
				@enderror                                        </div>
		</div>
		<div class="form-group bmd-form-group">
			<div class="input-group">
				<div class="input-group-prepend">
					<div class="input-group-text">
						<i class="fal fa-mobile-alt"></i>
					</div>
				</div>
				<input type="text" name="mobile" class="form-control @error('mobile') is-invalid @enderror" value="{{data('mobile', $data , $address)}}" placeholder="شماره همراه">
				@error('mobile')
				<span class="invalid-feedback" role="alert" style="text-align: right">
                                        <strong>{{ $message }}</strong>
                                    </span>
				@enderror                                        </div>
		</div>
		<div class="form-group bmd-form-group">
			<div class="input-group">
				<div class="input-group-prepend">
					<div class="input-group-text">
						<i class="fal fa-mobile-alt"></i>
					</div>
				</div>
				<input type="text" name="phone" class="form-control @error('phone') is-invalid @enderror" value="{{data('phone', $data , $address)}}" placeholder="تلفن ثابت">
				@error('phone')
				<span class="invalid-feedback" role="alert" style="text-align: right">
                                        <strong>{{ $message }}</strong>
                                    </span>
				@enderror                                        </div>
		</div>
		<div class="form-group bmd-form-group">
			<div class="input-group">
				<div class="input-group-prepend">
					<div class="input-group-text">
						<i class="fal fa-map-marker-alt"></i>
					</div>
				</div>
				<input type="text" name="state" class="form-control @error('state') is-invalid @enderror" value="{{data('state', $data , $address)}}" placeholder="استان">
				@error('state')
				<span class="invalid-feedback" role="alert" style="text-align: right">
                                        <strong>{{ $message }}</strong>
                                    </span>
				@enderror                                        </div>
		</div>
		<div class="form-group bmd-form-group">
			<div class="input-group">
				<div class="input-group-prepend">
					<div class="input-group-text">
						<i class="fal fa-map-marked"></i>
					</div>
				</div>
				<input type="text" name="city" class="form-control @error('city') is-invalid @enderror" value="{{data('city', $data , $address)}}" placeholder="شهر">
				@error('city')
				<span class="invalid-feedback" role="alert" style="text-align: right">
                                        <strong>{{ $message }}</strong>
                                    </span>
				@enderror                                        </div>
		</div>
		<div class="form-group bmd-form-group">
			<div class="input-group">
				<div class="input-group-prepend">
					<div class="input-group-text">
						<i class="fal fa-street-view"></i>
					</div>
				</div>
				<input type="text" name="address" class="form-control @error('address') is-invalid @enderror" value="{{data('address', $data , $address)}}" placeholder="آدرس گیرنده">
				@error('address')
				<span class="invalid-feedback" role="alert" style="text-align: right">
                                        <strong>{{ $message }}</strong>
                                    </span>
				@enderror                                        </div>
		</div>
		<div class="form-group bmd-form-group">
			<div class="input-group">
				<div class="input-group-prepend">
					<div class="input-group-text">
						<i class="fal fa-barcode-read"></i>
					</div>
				</div>
				<input type="text" name="postCode" class="form-control @error('postCode') is-invalid @enderror" value="{{data('postCode', $data , $address)}}" placeholder="کد پستی">
				@error('postCode')
				<span class="invalid-feedback" role="alert" style="text-align: right">
                                        <strong>{{ $message }}</strong>
                                    </span>
				@enderror                                        </div>
		</div>
		<div class="form-group bmd-form-group">
			<div class="input-group">
				<div class="input-group-prepend">
					<div class="input-group-text">
						<i class="fal fa-text-width" style="margin-bottom: auto;margin-top: 5px"></i>
					</div>
				</div>
				<textarea class="form-control" name="description" rows="3" placeholder="توضیحات اضافه" id="des">{{data('description', $data , $address)}}</textarea>

			</div>
		</div>
	</div>
	<div class="modal-footer justify-content-center">
		<button type="submit" class="btn pro-frm-btn px-4">ثبت آدرس</button>
		<button class="btn pro-frm-btn px-4">انصراف</button>
	</div>

</form>
