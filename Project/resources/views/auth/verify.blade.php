@extends('layouts.app')

@section('content')
    <div class="card-header">{{ __('آدرس ایمیل خود را تأیید کنید') }}</div>

    <div class="card-body" style="text-align: right">
        @if (session('resent'))
            <p class="p-3 iransans text-center message-text">
                {{ __('لینک تأیید جدید به آدرس ایمیل شما ارسال شده است.') }}
            </p>
        @endif
        <small class="form-text text-muted">
        {{ __('قبل از ادامه ، لطفاً ایمیل خود را برای لینک تأیید بررسی کنید.') }}
        </small>
        <small class="form-text text-muted">
        {{ __('اگر ایمیل دریافت نکردید') }},
        </small>
        <form class="d-inline" method="POST" action="{{ route('verification.resend') }}">
            @csrf
            <div class="btn-filter text-center mt-4">
                <button type="submit" class="btn btn-info btn-round dastnevis btn-circle position-relative fs-16">
                    <i class="fal fa-sign-in clr-gold fs-18 position-relative"></i>
                    {{ __('برای درخواست دیگری اینجا را کلیک کنید') }}
                </button>
            </div>
        </form>
    </div>
@endsection
