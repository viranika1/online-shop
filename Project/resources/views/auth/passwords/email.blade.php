@extends('layouts.app')

@section('content')

    <h1 class="clr-42 text-center fs-18 iransans mb-3 mt-1">بازیابی رمز عبور</h1>
    @if (session('status'))
        <p class="p-3 iransans text-center message-text">
            {{ session('status') }}
        </p>
    @endif

    <form method="POST" action="{{ route('password.email') }}">
        @csrf
        <div class="form-group bmd-form-group">
            <div class="input-group">
                <div class="input-group-prepend">
                    <div class="input-group-text pr-0">
                        <i class="fal fa-envelope fs-20 clr-92"></i>
                    </div>
                </div>
                <input name="email" type="email" class="form-control @error('email') is-invalid @enderror " placeholder="ایمیل خود را وارد کنید">

                @error('email')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
                @enderror
            </div>
            <div class="btn-filter text-center mt-4">
                <button type="submit" class="btn btn-info btn-round dastnevis btn-circle position-relative fs-16">
                    <i class="fal fa-lock-alt clr-gold fs-18 position-relative"></i>
                    بازیابی پسورد
                </button>
            </div>
        </div>
@endsection
