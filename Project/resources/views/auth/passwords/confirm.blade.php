@extends('Layouts.App.Auth')
@section('content')


    <h1 class="clr-42 text-center fs-18 iransans mb-3 mt-1">تایید کلمه عبور</h1>
    <form method="POST" action="{{ route('password.confirm') }}" style="text-align: right">
        @csrf
        <div class="form-group bmd-form-group">
            <div class="input-group">
                <div class="input-group-prepend">
                    <div class="input-group-text pr-0">
                        <i class="fal fa-envelope fs-20 clr-92"></i>
                    </div>
                </div>
                <input name="password" type="password" class="form-control" placeholder="رمز عبور خود را وارد کنید">

                @error('password')
                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                @enderror
            </div>
        </div>

        <div class="btn-filter text-center mt-4">
            <button type="submit" class="btn btn-info btn-round dastnevis btn-circle position-relative fs-16">
                <i class="fal fa-lock-alt clr-gold fs-18 position-relative"></i>
                تایید پسورد
            </button>
        </div>
        <hr>
        @if (Route::has('password.request'))
            <a class="btn btn-link" href="{{ route('password.request') }}">
                {{ __('رمز عبور خود را فراموش کرده ام') }}
            </a>
    @endif



@endsection
