@extends('layouts.app')
@section('content')
    <form action="{{ route('register') }}" method="post">
        @csrf
{{--        <div class="form-group bmd-form-group">--}}
{{--            <div class="input-group">--}}
{{--                <div class="input-group-prepend">--}}
{{--                    <div class="input-group-text pr-0">--}}
{{--                        <i class="fal fa-envelope fs-20 clr-92"></i>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--                <input name="name" value="{{ old('name') }}" type="text" class="form-control  @error('name') is-invalid @enderror" placeholder="نام خود را وارد نمایید">--}}
{{--                @error('name')--}}
{{--                <span class="invalid-feedback" role="alert" style="text-align: right">--}}
{{--                    <strong>{{ $message }}</strong>--}}
{{--                </span>--}}
{{--                @enderror--}}
{{--            </div>--}}
{{--        </div>--}}
        <div class="form-group bmd-form-group">
            <div class="input-group">
                <div class="input-group-prepend">
                    <div class="input-group-text pr-0">
                        <i class="fal fa-envelope fs-20 clr-92"></i>
                    </div>
                </div>
                <input name="email" value="{{ old('email') }}" type="text" class="form-control @error('email') is-invalid @enderror" placeholder="ایمیل خود را وارد نمایید">
                @error('email')
                <span class="invalid-feedback" role="alert" style="text-align: right">
                    <strong>{{ $message }}</strong>
                </span>
                @enderror
            </div>
        </div>
        <div class="form-group bmd-form-group">
            <div class="input-group">
                <div class="input-group-prepend">
                    <div class="input-group-text pr-0">
                        <i class="fal fa-lock-alt fs-20 clr-92"></i>
                    </div>
                </div>
                <input name="password" type="text" class="form-control @error('password') is-invalid @enderror" placeholder="رمز عبور">
                @error('password')
                <span class="invalid-feedback" role="alert" style="text-align: right">
                    <strong>{{ $message }}</strong>
                </span>
                @enderror
            </div>
        </div>
{{--        <div class="form-group bmd-form-group">--}}
{{--            <div class="input-group">--}}
{{--                <div class="input-group-prepend">--}}
{{--                    <div class="input-group-text pr-0">--}}
{{--                        <i class="fal fa-lock-alt fs-20 clr-92"></i>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--                <input name="password_confirmation" type="text" class="form-control @error('password_confirmation') is-invalid @enderror" placeholder="رمز عبور را تایید کنید">--}}
{{--                @error('password_confirmation')--}}
{{--                <span class="invalid-feedback" role="alert" style="text-align: right">--}}
{{--                    <strong>{{ $message }}</strong>--}}
{{--                </span>--}}
{{--                @enderror--}}
{{--            </div>--}}
{{--        </div>--}}
        <div class="form-group bmd-form-group">
            <div class="input-group">
                <div class="input-group-prepend">
                    <div class="input-group-text pr-0">
                        <i class="fal fa-lock-alt fs-20 clr-92"></i>
                    </div>
                </div>
                <input name="introducingCode" value="{{old('introducingCode')}}" type="text" class="form-control @error('introducingCode') is-invalid @enderror" placeholder="کد معرف را وارد نمایید">
                @error('introducingCode')
                <span class="invalid-feedback" role="alert" style="text-align: right">
                    <strong>{{ $message }}</strong>
                </span>
                @enderror
            </div>
        </div>
        <div class="form-check remember">
            <label class="form-check-label d-block my-3 text-right fs-12">
                <p class="fs-12">
                    <a class="clr-blue2 Ho-roles" href="#">قوانین و مقررات</a>
                    را مطالعه نمودم و با آن موافقم
                </p>
                <input class="form-check-input" name="rules" type="checkbox" >
                <span class="form-check-sign">
				    <span class="check"></span>
				</span>

            </label>
        </div>

        <div class="btn-filter text-center mt-4">
            <button class="btn btn-info btn-round dastnevis btn-circle position-relative fs-16">
                <i class="fal fa-user-plus clr-gold fs-18 position-relative"></i>
                عضویت در هنرچی
            </button>
        </div>
    </form>
    <hr>
    <p class="fs-12 text-center">قبلا در هنرچی ثبت نام کرده اید ؟
        <a class="clr-blue2" href="{{route('login')}}">وارد شوید</a>
    </p>
@endsection