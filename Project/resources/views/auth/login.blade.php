@extends('Layouts.app')
@section('content')

    <form action="{{route('login')}}" method="post" >
        @csrf


        <div class="form-group bmd-form-group">
            <div class="input-group">
                <div class="input-group-prepend">
                    <div class="input-group-text pr-0">
                        <i class="fal fa-envelope fs-20 clr-92"></i>
                    </div>
                </div>
                <input type="text" name="email" id="email" class="form-control @error('email') is-invalid @enderror " value="{{ old('email') }}" placeholder="پست الکترونیک یا شماره همراه">
                @error('email')
                <span class="invalid-feedback" role="alert" style="text-align: right">
                                        <strong>{{ $message }}</strong>
                                    </span>
                @enderror
            </div>
        </div>
        <div class="form-group bmd-form-group">
            <div class="input-group">
                <div class="input-group-prepend">
                    <div class="input-group-text pr-0">
                        <i class="fal fa-lock-alt fs-20 clr-92"></i>
                    </div>
                </div>
                <input type="text" id="password" class="form-control @error('password') is-invalid @enderror" name="password" placeholder="رمز عبور">
                @error('password')
                <span class="invalid-feedback" role="alert" style="text-align: right">
                                        <strong>{{ $message }}</strong>
                                    </span>
                @enderror
            </div>
        </div>
        <div class="form-check remember">
            <label class="form-check-label d-block my-3 text-right fs-12">	مرا بخاطر بسپار
                <input class="form-check-input" type="checkbox" checked name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>
                <span class="form-check-sign">
										<span class="check"></span>
									</span>
            </label>
        </div>
        <small id="emailHelp" class="form-text text-muted">

            @if (Route::has('password.request'))

                <a class="clr-blue2" href="{{ route('password.request') }}">
                    رمز عبور خود را فراموش کرده ام</a>
            @endif
        </small>
        <div class="btn-filter text-center mt-4">
            <button class="btn btn-info btn-round dastnevis btn-circle position-relative fs-16">
                <i class="fal fa-sign-in clr-gold fs-18 position-relative"></i>
                ورود به هنرچی
            </button>

        </div>
    </form>
    <hr>
    <p class="fs-12 text-center">در هنرچی ثبت نام نکرده اید ؟
        <a class="clr-blue2" href="{{route('register')}}">ثبت نام کنید.</a>
    </p>
@endsection
