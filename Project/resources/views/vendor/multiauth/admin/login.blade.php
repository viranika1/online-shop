@extends('Layouts.Admin.Auth')
@section('content')


<!-- BEGIN LOGIN FORM -->
<form class="login-form" action="{{ route('admin.login') }}" method="post">
    @csrf
    <h3 class="form-title">ورود</h3>
    <div class="alert alert-danger display-hide" style={{ ($errors->any()) ? "display:block" : '' }}>
        <button class="close" data-close="alert"></button>
        @if ($errors->any())
        @foreach($errors->all() as $error)
        <span style="display: block;">{{$error}}</span>
        @endforeach
        @else
        <span>ایمیل و کلمه عبور خود را وارد کنید.</span>
        @endif
    </div>
    <div class="form-group">
        <!--ie8, ie9 does not support html5 placeholder, so we just show field title for that-->
        <label class="control-label visible-ie8 visible-ie9">ایمیل</label>
        <input id="email" type="text" class="form-control form-control-solid placeholder-no-fix" placeholder="ایمیل" name="email" required>
    </div>
    <div class="form-group">
        <label class="control-label visible-ie8 visible-ie9">کلمه عبور</label>
        <input id="password" type="password" class="form-control form-control-solid placeholder-no-fix" placeholder="کلمه عبور" name="password" >
    </div>
    <div class="form-actions">
        <button type="submit" class="btn btn-success uppercase">ورود</button>
        <label class="rememberme check">
            <input type="checkbox" name="remember" value="1"/>مرا به خاطر داشته باش</label>
        <a href="javascript:;" id="forget-password" class="forget-password">پسورد را فراموش کرده اید</a>
    </div>
    {{--<div class="login-options">
        <h4>Or login with</h4>
        <ul class="social-icons">
            <li>
                <a class="social-icon-color facebook" data-original-title="facebook" href="javascript:;"></a>
            </li>
            <li>
                <a class="social-icon-color twitter" data-original-title="Twitter" href="javascript:;"></a>
            </li>
            <li>
                <a class="social-icon-color googleplus" data-original-title="Goole Plus" href="javascript:;"></a>
            </li>
            <li>
                <a class="social-icon-color linkedin" data-original-title="Linkedin" href="javascript:;"></a>
            </li>
        </ul>
    </div>--}}
    <div class="create-account">
        <p>
            <a href="javascript:;" id="register-btn" class="uppercase">ساخت اکانت</a>
        </p>
    </div>
</form>
<!-- END LOGIN FORM -->
@endsection
