@extends('Layouts.Admin.Auth')
@section('content')
@include('multiauth::message')
<h2>ثبت نام جدید ادمین</h2>
<form method="POST" class="login-form" action="{{ route('admin.register') }}">
    @csrf
    <div class="form-group">
        <input id="name" type="text" class="form-control form-control-solid placeholder-no-fix" name="name" placeholder="نام را وارد نمایید" value="{{ old('name') }}" required autofocus>
    </div>
    <div class="form-group">
        <input id="email" type="email" class="form-control form-control-solid placeholder-no-fix" name="email" placeholder="ایمیل را وارد نمایید" value="{{ old('email') }}" required>
    </div>
    <div class="form-group">
        <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" placeholder="کلمه عبور را وارد نمایید" name="password" required>
    </div>
    <div class="form-group">
        <input id="password-confirm" type="password" class="form-control" name="password_confirmation" placeholder="تایید کلمه عبور را وارد نمایید" required>
    </div>
    <div class="form-group">
        <select name="role_id[]" id="role_id" class="form-control" multiple>
            <option selected disabled>نقش کاربر را انتخاب کنید</option>
            @foreach ($roles as $role)
                <option value="{{ $role->id }}">{{ $role->name }}</option>
            @endforeach
        </select>
    </div>
    <div class="btn-group" style="margin: -1px -31px -32px -31px;width: -webkit-fill-available;width: -moz-fill-available;">
        <a href="{{ route('admin.show') }}" class="btn btn-danger btn-lg col-md-6">
            برگشت
        </a>
        <button type="submit" class="btn btn-lg btn-primary col-md-6">
            ثبت نام
        </button>
    </div>
</form>
@endsection
