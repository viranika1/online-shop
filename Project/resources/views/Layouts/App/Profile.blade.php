@extends('Layouts.App.App')
@section('content')
    <section class="container-fluid profile-page">
        <div class="container">
            <div class="row row-profile mb-3">
                <div class="col-lg-3 col-md-4 pt-5 p-right">
                  @include('Components.App.Profile.sidebare1')

                        @include('Components.App.Profile.sidebare2')

                </div>
                <div class="col-lg-9 col-md-8 mt-2 p-left">

                    @yield('profile-content')
                </div>
            </div>
        </div>
    </section>



@endsection
