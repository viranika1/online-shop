<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <!-- Start App Style -->
    <link rel="stylesheet" href="/css/material-kit.min.css" />
    <link rel="stylesheet" type="text/css" href="/css/fontawesome.min.css">
    <link rel="stylesheet" type="text/css" href="/css/light.min.css">
    <link rel="stylesheet" type="text/css" href="/css/brands.min.css">
    <link rel="stylesheet" type="text/css" href="/css/jasny-bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="/css/owl.carousel.min.css">
    <link rel="stylesheet" type="text/css" href="/css/owl.theme.default.min.css">
    <link rel="stylesheet" type="text/css" href="/css/style.css">
    <link rel="stylesheet" type="text/css" href="/css/responsive.css">
    <!-- End App Style -->
@hasSection('pageStyle')
    <!-- Start Page Style -->
@yield('pageStyle')
    <!-- End Page Style -->
@endif
    <title>هنرچی ،دست ساخته های ایرانی</title>
</head>
<body class="@yield('bodyClass')">

@include('Sections.App.Header')
@yield('content')
@include('Sections.App.Footer')
<!-- Start App Script -->
<script type="text/javascript" src="/js/buttons.js"></script>
<script type="text/javascript" src="/js/jquery-min.js"></script>
<script type="text/javascript" src="/js/jquery-3.5.1.min.js"></script>
<script type="text/javascript" src="/js/bootstrap.min.js"></script>
<script type="text/javascript" src="/js/popper.min.js"></script>
<script type="text/javascript" src="/js/bootstrap-material-design.min.js"></script>
<script type="text/javascript" src="/js/material-kit.min.js"></script>
<script type="text/javascript" src="/js/moment.min.js"></script>
<script type="text/javascript" src="/js/nouislider.min.js"></script>
<script type="text/javascript" src="/js/jasny-bootstrap.min.js"></script>
<script type="text/javascript" src="/js/owl.carousel.min.js"></script>
<script type="text/javascript" src="/js/custom.js"></script>
<!-- End App Script -->
@hasSection('pageScript')
<!-- End Page Script -->
@yield('pageScript')
<!-- End page Script -->
@endif
</body>
</html>
