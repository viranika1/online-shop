<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en" class="no-js">
<!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
    <meta charset="utf-8"/>
    <title>Metronic | Admin Dashboard Template</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1" name="viewport"/>
    <meta content="" name="description"/>
    <meta content="" name="author"/>
    <!-- BEGIN GLOBAL MANDATORY STYLES -->
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css"/>
    <link href="/AdminDashboard/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
    <link href="/AdminDashboard/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css"/>
    <link href="/AdminDashboard/plugins/bootstrap/css/bootstrap-rtl.min.css" rel="stylesheet" type="text/css"/>
    <link href="/AdminDashboard/plugins/uniform/css/uniform.default.css" rel="stylesheet" type="text/css"/>
    <link href="/AdminDashboard/plugins/bootstrap-switch/css/bootstrap-switch-rtl.min.css" rel="stylesheet" type="text/css"/>
    <!-- END GLOBAL MANDATORY STYLES -->
    <!-- BEGIN PAGE LEVEL PLUGIN STYLES -->
    @yield('PagePluginsStyle')
    <!-- END PAGE LEVEL PLUGIN STYLES -->
    <!-- BEGIN PAGE STYLES -->
    @yield('PageStyles')
    <!-- END PAGE STYLES -->
    <!-- BEGIN THEME STYLES -->
    <!-- DOC: To use 'rounded corners' style just load 'components-rounded.css' stylesheet instead of 'components.css' in the below style tag -->
    <link href="/AdminDashboard/css/components-md-rtl.css" id="style_components" rel="stylesheet" type="text/css"/>
    <link href="/AdminDashboard/css/plugins-md-rtl.css" rel="stylesheet" type="text/css"/>
    <link href="/AdminDashboard/layout/css/layout-rtl.css" rel="stylesheet" type="text/css"/>
    <link href="/AdminDashboard/layout/css/themes/light-rtl.css" rel="stylesheet" type="text/css" id="style_color"/>
    <link href="/AdminDashboard/layout/css/custom-rtl.css" rel="stylesheet" type="text/css"/>
    <!-- END THEME STYLES -->
    <link rel="shortcut icon" href="favicon.ico"/>
</head>

<body class="page-md page-header-fixed page-sidebar-closed-hide-logo page-sidebar-closed-hide-logo">
<!-- BEGIN HEADER -->
@include('Sections.Admin.Header')
<!-- END HEADER -->


<!-- BEGIN CONTAINER -->
<div class="page-container">
    <!-- BEGIN SIDEBAR -->
    <div class="page-sidebar-wrapper">
        <!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing -->
        <!-- DOC: Change data-auto-speed="200" to adjust the sub menu slide up/down speed -->
        <div class="page-sidebar md-shadow-z-2-i  navbar-collapse collapse">
            <!-- BEGIN SIDEBAR MENU -->
            @include('Sections.Admin.Sidebar')
            <!-- END SIDEBAR MENU -->
        </div>
    </div>
    <!-- END SIDEBAR -->
    <!-- BEGIN CONTENT -->
    <div class="page-content-wrapper">
        <div class="page-content">
            <!-- Beg PAGE CONTENT INNER -->
            @yield('Content')
            <!-- END PAGE CONTENT INNER -->
        </div>
    </div>
    <!-- END CONTENT -->
</div>
<!-- END CONTAINER -->
<!-- BEGIN FOOTER -->
<div class="page-footer">
    <div class="page-footer-inner">
        2014 &copy; Metronic by keenthemes. <a href="http://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes" title="Purchase Metronic just for 27$ and get lifetime updates for free" target="_blank">Purchase Metronic!</a>
    </div>
    <div class="scroll-to-top">
        <i class="icon-arrow-up"></i>
    </div>
</div>
<!-- END FOOTER -->
<!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
<!-- BEGIN CORE PLUGINS -->
<!--[if lt IE 9]>
<script src="/AdminDashboard/plugins/respond.min.js"></script>
<script src="/AdminDashboard/plugins/excanvas.min.js"></script>
<![endif]-->
<script src="/AdminDashboard/plugins/jquery.min.js" type="text/javascript"></script>
<script src="/AdminDashboard/plugins/jquery-migrate.min.js" type="text/javascript"></script>
<!-- IMPORTANT! Load jquery-ui.min.js before bootstrap.min.js to fix bootstrap tooltip conflict with jquery ui tooltip -->
<script src="/AdminDashboard/plugins/jquery-ui/jquery-ui.min.js" type="text/javascript"></script>
<script src="/AdminDashboard/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<script src="/AdminDashboard/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js" type="text/javascript"></script>
<script src="/AdminDashboard/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
<script src="/AdminDashboard/plugins/jquery.blockui.min.js" type="text/javascript"></script>
<script src="/AdminDashboard/plugins/jquery.cokie.min.js" type="text/javascript"></script>
<script src="/AdminDashboard/plugins/uniform/jquery.uniform.min.js" type="text/javascript"></script>
<script src="/AdminDashboard/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>
<!-- END CORE PLUGINS -->
<!-- BEGIN PAGE LEVEL PLUGINS -->
@yield('PagePlugins')
<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="/AdminDashboard/scripts/metronic.js" type="text/javascript"></script>
<script src="/AdminDashboard/layout/scripts/layout.js" type="text/javascript"></script>
<script src="/AdminDashboard/layout/scripts/quick-sidebar.js" type="text/javascript"></script>
<!-- END PAGE LEVEL SCRIPTS -->
<script>
    jQuery(document).ready(function() {
        Metronic.init(); // init metronic core componets
        Layout.init(); // init layout
        QuickSidebar.init(); // init quick sidebar
    });
</script>
@yield('PageScripts')
<!-- END JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>
