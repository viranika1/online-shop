var addProduct =
    {
        init:{
            addOptions: function () {
                $( function() {
                    $( "#OptionsAccordion" ).sortable({
                        revert: true
                    });
                } );
                var cloneGroup = $("#accordion1 > .panel-default").clone();
                $("#accordion1").remove();

                $("#addGroupOption").click(function(){
                    var optionGroupID = "";
                    var optionGroupIdArray = [];
                    if ($("#OptionsAccordion > div").length == 0){
                        optionGroupID = "optionGroup-1";
                    }else{
                        $("#OptionsAccordion > div").each(function(x , y){
                            optionGroupIdArray.push(y.getAttribute("optionGroupID"))
                        });
                        optionGroupIdArray.sort();
                        optionGroupID = optionGroupIdArray.pop();
                        optionGroupID = Number(optionGroupID.replace("optionGroup-",""))+1;
                        optionGroupID = "optionGroup-"+optionGroupID;
                    }
                    cloneGroup.attr("optionGroupID",optionGroupID);
                    $("#OptionsAccordion").append(cloneGroup.clone().fadeIn());
                    $("#OptionsAccordion > div:last-Child > div.panel-heading > h4 > a").attr({'data-parent':"#OptionsAccordion" , href:"#"+optionGroupID});
                    $("#OptionsAccordion > div:last-Child > div:nth-child(2)").attr("id",optionGroupID);
                    $('div[optionGroupID="' + optionGroupID +'"] > div.panel-heading > h4 > div > a.btn.red').click(function () {
                        $('div[optionGroupID="' + optionGroupID +'"]').fadeOut(function () {
                            ///java script cod for remove from data base///
                            $('div[optionGroupID="' + optionGroupID +'"]').remove();
                        });
                    });
                    $('div[optionGroupID="' + optionGroupID +'"] > div.panel-heading > h4 > div > a.btn.green').click(function () {
                        $('div[optionGroupID="' + optionGroupID +'"] > div.panel-heading > h4 > a > span').focus();
                    });

                    let optionInputGroup = $('#'+ optionGroupID+' > div > div.group-option-group > div.row.option-group').clone();
                    $('#'+ optionGroupID+' > div > div.group-option-group > div.row.option-group').remove();
                    $('#'+ optionGroupID+' > div > div.group-option-group ').attr("id",optionGroupID+"Sort");
                    $( function() {
                        $( "#"+optionGroupID+"Sort" ).sortable({
                            revert: true
                        });
                    } );
                    $('div[optionGroupID="' + optionGroupID +'"] > div.panel-heading > h4 > div > a.btn.blue').click(function () {
                        if ($('#'+ optionGroupID).hasClass("in") == false ){
                            $('div[optionGroupID="' + optionGroupID +'"] > div.panel-heading > h4 > a > span').click();
                        }
                        var  x  = optionInputGroup.clone().fadeIn();
                        $('#'+ optionGroupID +' > div > div.group-option-group').append(x);
                        $(x[0].childNodes[5].childNodes[1].childNodes[1]).click(function () {
                            //remove option from data base js cod//
                            $(x).fadeOut(function () {
                                $(x).remove();
                            });
                        });
                        $(x[0].children).each(function (x , xx) {
                            var xxx = $($(xx)[0].children[0]);
                            if (typeof xxx[0] !== "undefined"){
                                if (xxx[0].nodeName === "SELECT"){
                                    xxx.select2({dir: "rtl"});
                                }
                            }


                        })
                    });
                    $('div[optionGroupID="' + optionGroupID +'"] > div.panel-heading > h4 > a > span').text("تغییر نام گروه را بزنید");
                    $('div[optionGroupID="' + optionGroupID +'"] > div.panel-heading > h4 > a > span').click();
                });
            },

            mixValues: function () {
                var MixValues = $("#MixValuesAcccordion > div").clone();
                $("#MixValuesAcccordion > div").remove();
                $( function() {
                    $( "#MixValuesAcccordion" ).sortable({
                        revert: true
                    });
                } );

                $("#addMixValues").click(function(){
                    let cloneMixValues = MixValues.clone();
                    let panelbody = "3,1,1,";
                    let rightbody = panelbody + "1,3,3,";
                    let leftbody = panelbody + "3,3,3,";

                    let selectValues = rightbody + "1,1,3";

                    let takhfif = leftbody + "3,1,3,";
                    let vije = leftbody + "3,3,3,";

                    let startdate = "3,3";
                    let stopdate =  "5,3";

                    $(selectElement(selectValues , cloneMixValues[0])).select2({dir: "rtl"});
                    var MixValuesGroupID = "";
                    var MixValuesGroupIDArray = [];
                    if ($("#MixValuesAcccordion > div").length == 0){
                        MixValuesGroupID = "MixValuesID-1";
                    }else{
                        $("#MixValuesAcccordion > div").each(function(x , y){
                            MixValuesGroupIDArray.push(y.getAttribute("mixvaluesgroupid"))
                        });
                        MixValuesGroupIDArray.sort();
                        MixValuesGroupID = MixValuesGroupIDArray.pop();
                        MixValuesGroupID = Number(MixValuesGroupID.replace("MixValuesID-",""))+1;
                        MixValuesGroupID = "MixValuesID-"+MixValuesGroupID;
                    }
                    cloneMixValues.attr("MixValuesGroupID",MixValuesGroupID);

                    $(selectElement("1,1,1",cloneMixValues[0])).attr({'data-parent':"#MixValuesAcccordion" , href:"#"+MixValuesGroupID});
                    $(selectElement("3",cloneMixValues[0])).attr("id",MixValuesGroupID);
                    $(selectElement("1,1,1,0",cloneMixValues[0])).text("متغیر ها را انتخاب کنید");

                    $(selectElement("1,1,3,1",cloneMixValues[0])).click(function () {
                        $(cloneMixValues).fadeOut(function () {
                            ///java script cod for remove from data base///
                            $(cloneMixValues).remove();
                        });
                    });
                    persianDatePickerConfiged(null , $(selectElement(takhfif+startdate , cloneMixValues[0])) , $(selectElement(takhfif+stopdate , cloneMixValues[0])));
                    persianDatePickerConfiged(null , $(selectElement(vije+startdate , cloneMixValues[0])) , $(selectElement(vije+stopdate , cloneMixValues[0])));


                    $("#MixValuesAcccordion").append(cloneMixValues.fadeIn());
                    $(selectElement(rightbody+"3,1",cloneMixValues[0])).append($(selectElement(rightbody+"3,1,3,0,0",cloneMixValues[0])));
                    $(selectElement(rightbody+"5,1",cloneMixValues[0])).append($(selectElement(rightbody+"5,1,3,0,0",cloneMixValues[0])));
                    $(selectElement(rightbody+"3,1,3",cloneMixValues[0])).remove();
                    $(selectElement(rightbody+"5,1,3",cloneMixValues[0])).remove();
                    $(selectElement(rightbody+"3,1,4",cloneMixValues[0])).bootstrapSwitch();
                    $(selectElement(rightbody+"5,1,4",cloneMixValues[0])).bootstrapSwitch();

                    $(selectElement("1,1,1,0",cloneMixValues[0])).click();
                });
            },

            addValues: function () {
                $( function() {
                    $( "#values" ).sortable({
                        revert: true
                    });
                } );
                var cloneValue = $("#values > div").clone();
                $("#values > div").remove();
                $("#tab3   div.btn-group > a").click(function () {
                    let valueC = cloneValue.clone().fadeIn();
                    $(valueC[0].childNodes[1].childNodes[1]).select2();
                    $(valueC[0].childNodes[3].childNodes[1]).select2();
                    $(valueC[0].childNodes[5].childNodes[1]).click(function () {
                        valueC.fadeOut(function () {
                            valueC.remove();
                        })
                    });
                    $("#values").append(valueC);
                });
            },

            addCategories: function () {
                var cat = [
                    {
                        id : 1,
                        text : "کالای دجیتال",
                        parent : 0
                    },
                    {
                        id : 2,
                        text : "گوشی موبایل",
                        parent : 1
                    },
                    {
                        id : 3,
                        text : "نوکیا",
                        parent : 2
                    },
                    {
                        id : 4,
                        text : "سامسونگ",
                        parent : 2
                    },
                    {
                        id : 5,
                        text : "اپل",
                        parent : 2
                    },
                    {
                        id : 6,
                        text : "آرایشی و بهداشتی",
                        parent : 0
                    },
                    {
                        id : 7,
                        text : "آرایشی",
                        parent : 6
                    },
                    {
                        id : 8,
                        text : "آرایش لب",
                        parent : 7
                    },
                    {
                        id : 9,
                        text : "آرایش صورت",
                        parent : 7
                    },
                    {
                        id : 10,
                        text : "بهداشتی",
                        parent : 6
                    },
                    {
                        id : 11,
                        text : "شامپو",
                        parent : 10
                    },
                    {
                        id : 12,
                        text : "دوربین",
                        parent : 1
                    },
                    {
                        id : 13,
                        text : "عکاسی",
                        parent : 12
                    },
                    {
                        id : 14,
                        text : "ورزشی",
                        parent : 12
                    },
                    {
                        id : 15,
                        text : "چاپ سریع",
                        parent : 12
                    },
                    {
                        id : 16,
                        text : "فیلم برداری",
                        parent : 12
                    }
                ]

                var catnew = [] ;

                function insert (array , parent ){
                    if (parent == 0) {
                        let x = cat.filter(cato => cato.parent == parent).sort((a , b) => a -b);
                        catnew =catnew.concat(x);
                    }
                    let x = cat.filter(cato => cato.parent == parent).sort((a , b) => a -b);
                    array.forEach(function(a){
                        if (a.id == parent ){
                            a.inc = x
                        }else if(a.inc){
                            insert(a.inc , parent )
                        }
                    })
                }
                function createCat (){
                    var catparent = [];
                    cat.forEach(function(a){
                        catparent.push(a.parent)
                    });
                    catparent = [... new Set(catparent)];
                    catparent.forEach(function(a){
                        insert (catnew , a )
                    })
                }
                createCat();
                $("#category").select2ToTree({treeData: {dataArr: catnew},width: 'resolve',dir: "rtl"});
            },

            all: function () {
                this.addOptions();
                this.mixValues();
                this.addValues();
                this.addCategories();
            }
        }
    }