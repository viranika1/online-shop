<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Property extends Model
{
    protected $fillable=[
        'id',
        'name',
        'value',
        'typeProperty',
        'linked',
        'slug',
        'variable',
        'typeVariable',
        'metaTitle',
        'metaKeyword',
        'metaDescription',
    ];


}
