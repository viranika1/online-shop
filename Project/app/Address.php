<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Address extends Model
{
    protected $fillable=[
        'postCode','phone','mobile','country','state',
        'city','address','receiver','description','userId',
    ];

    public function User(){
        return $this->belongsTo('App\User','userId');
    }
    public function Orders(){
        return $this->hasMany('App\Order','addressId');
    }
}
