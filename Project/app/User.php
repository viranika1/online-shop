<?php

namespace App;

use GuzzleHttp\Psr7\Request;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Auth;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable=[
        'id',
        'avatar',
        'name',
        'lastName',
        'email',
        'phone',
        'nationalCode',
        'password',
        'code',
        'introducingId',
        'level',
        'score',
        'newsletter',
        'cardNumber',
        'confirmPhone',
        'confirmEmail',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];
    public function DiscountCodes(){
        return $this->belongsToMany('App\DiscountCode','discountCode_User','userId')->withPivot('count');
    }
   public function Introducing(){
        return $this->belongsTo('App\User' , 'introducingId');
   }
   public function Introduced(){
        return $this->hasMany('App\User' , 'introducingId');
   }
   public function Orders(){
        return $this->hasMany('App\Order','userId');
   }
   public function Tickets(){
        return $this->hasmany('App\Ticket','userId');
   }
   public function Store(){
       return $this->hasOne('App\Store','userId');
   }
   public function addresses(){
        return $this->hasMany('App\Address','userId');
   }
    public function Products(){
        return $this->belongsToMany('App\Product','product_user','productId','userId');

    }
}



