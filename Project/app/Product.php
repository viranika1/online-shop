<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    public function DiscountCodes(){
        return $this->belongsToMany('App\DiscountCode');
    }
    public function Category(){
        return $this->belongsTo('App\ProductCategory','categoryId');
    }
    public function Festival(){
        return $this->belongsTo('App\Festival','festivalId');
    }
    public function Registrar(){
        return $this->belongsTo('App\Admin','registrarId');
    }
    public function Store(){
        return $this->belongsTo('App\Store','storeId');
    }
    public function ProductDetails(){
        return $this->hasMany('App\ProductDetail','productId');
    }
    public function Users(){
        return $this->belongsToMany('App\User','product_user','userId','productId');
    }
    public function liked(){
        return ($this->Users()->find(\Auth::user()->id)== null)? false : true ;
    }

}
