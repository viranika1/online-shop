<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Store extends Model
{
    protected $fillable=[
        'id',
        'userId',
        'coverId',
        'logoId',
        'name',
        'location',
        'description',
        'metaTitle',
        'metaKeyword',
        'metaDescription',

    ];
    public function Products(){
        return $this->hasMany('App\Product');
    }
    public function Users(){
        return $this->belongsTo('App\User','userId');
    }
}
