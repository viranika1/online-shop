<?php

namespace App\Http\Controllers\App;

use App\Address;
use App\Http\Controllers\Controller;
use App\User;
use Exception;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;

class AddressController extends Controller
{
	protected $roll = [
		'receiver'=>'required|max:50',
		'postCode'=>'required|Numeric|min:10',
		'mobile'=>'required|regex:/^((0)9\d{9})$/',
		'address'=>'required|max:255|min:5',
		'city'=>'required|max:20|min:2',
		'state'=>'required|max:20|min:2'
	];



	/**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $addresses =\Auth::user()->addresses;
        return view('App.Profile.Address',compact('addresses'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        $addresses = Address::all();
        return view('App.Profile.Address',compact('addresses'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
		$data = Validator::make($request->all() , $this->roll );
		if ($data->fails()){
			$errors = $data->errors()->toArray();
			$data = $request->all();
			$route = route('profile.address.store');
			return view('App.Profile.EditAddress',['route' => $route , 'data'=> $data])->withErrors($errors);
		}
		$user=\Auth::user();
		$user->addresses()->create([
			'receiver'=>$data->receiver,
			'postCode'=>$data->postCode,
			'address'=>$data->address,
			'city'=>$data->city,
			'state'=>$data->state,
			'mobile'=>$data->mobile,
			'phone'=>$data->phone,

		]);
		return redirect(route('profile.address.index'));
    }

	/**
	 * Display the specified resource.
	 *
	 * @param Address $address
	 * @return void
	 */
    public function show(Address $address)
    {
        //
    }

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param Address $address
	 * @return Response
	 */
    public function edit(Address $address)
    {
    	$route = route("profile.address.update",["address"=>$address]);

        return view('App.Profile.EditAddress')->with(['address'=>$address , "route" => $route , "method"=>"PATCH" , "data" => ["aaaaaaaaaaa"] ]);
    }

	/**
	 * Update the specified resource in storage.
	 *
	 * @param Address $address
	 * @param Request $request
	 * @return Response
	 */
    public function update(Address $address , Request $request)
    {
		$data = Validator::make($request->all() , $this->roll );
		if ($data->fails()){
			$errors = $data->errors();
			$data = $request->all();
			$route = route('profile.address.update' , ["address" => $address]);
			return view('App.Profile.EditAddress',['method'=>'PATCH' , 'route' => $route , 'data'=> $data ,"address" => $address])->withErrors($errors);
		}
		$data = $data->getData();
		$address->update([
			'receiver'=>$data["receiver"],
			'postCode'=>$data["postCode"],
			'address'=>$data["address"],
			'city'=>$data["city"],
			'state'=>$data["state"],
			'mobile'=>$data["mobile"],
			'phone'=>$data["phone"],
		]);
		return response("Ok");
    }

	/**
	 * Remove the specified resource from storage.
	 * @param Address $address
	 * @return RedirectResponse
	 * @throws Exception
	 */
    public function destroy(Address $address)
    {
      $address ->delete();

      return back();
    }
}
