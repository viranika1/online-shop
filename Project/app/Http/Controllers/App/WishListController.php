<?php

namespace App\Http\Controllers\App;

use App\Http\Controllers\Controller;
use App\Product;
use Illuminate\Http\Request;

class WishListController extends Controller
{
    public function index () {
        $products=\Auth::user()->products;
        return view ('App.Profile.Wishlist',compact('products'));
    }

}
