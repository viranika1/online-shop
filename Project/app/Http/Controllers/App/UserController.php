<?php

namespace App\Http\Controllers\App;

use Alexusmai\LaravelFileManager\Controllers\FileManagerController;
use Alexusmai\LaravelFileManager\Requests\RequestValidator;
use App\Http\Controllers\Controller;
use App\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

class UserController extends Controller
{
    public function index() {

    }

    public function avatarUpdate (Request $request) {
		$valid = Validator::make($request->all(),[
			'urlAvatar' => 'required',
		]);
		if (!$valid->fails()) {
			Auth::user()->update(['avatar' => $valid->getData()['urlAvatar']]);
			return response('عکس پروفایل آپدیت شد.');
		}else{

		}
    }

    public function profile() {

    }

    public function profileIndex () {
        return view('App.Profile.Profile');
    }
    public function edit(){
        $user= \Auth::user();
        return view('App.Profile.EditPersonalinfo',compact('user'));
    }

	public function updateUser(Request $request){
	$user = \Auth::user();
	$request->validate([
		'name'=>'required|max:50',
		'lastName'=>'required|max:50',
		'nationalCode'=>'required|numeric|digits:10',
		'cardNumber'=>'required|integer|digits:16',
		'mobile'=>'required|regex:/^((0)9\d{9})$/',
		'email' => ['required','email',Rule::unique('users','email')->ignore($user->id)]
	]);
		$user->update([
			'name'=>$request -> name,
			'lastName'=>$request ->lastName,
			'nationalCode'=>$request->nationalCode,
			'cardNumber'=>$request->cardNumber,
			'mobile'=>$request->phone,
			'email'=>$request->email
		]);

	return redirect()->route('profile.index');
	}

	public function showChangePassword () {
		return view('App.Profile.EditPersonalinfo');
	}

	public function changePassword () {

	}

    public function ProductLiked(Product $product){
        $products=Auth::user()->Products();
        if ($products->find($product->id) ===null  ){
            $products->attach($product);
            return response('محصول لایک شد');

        }
        else
        {
            $products->detach($product);
            return response('محصول از لیست علاقه مندی ها حذف شد');
        }
    }

}
