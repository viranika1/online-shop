<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Property;
use Illuminate\Http\Request;

class PropertyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('Admin.Properties.Create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
    	$link = 0 ;
		if(isset($request->link)) {
		$link = 1;
		}
    	foreach ($request->variables as $val){
    		Property::create([
    			'name' 				=>	$request->name,
				'slug'				=>	$request->slug,
				'typeProperty'		=>	$request->typeOption,
				'value'				=>	$val,
				'linked'			=>	$link,
				'variable'			=>	0,
				'typeVariable'		=>	'',
				'metaTitle'			=>	$request->meta_title,
				'metaKeyword'		=>	$request->meta_keyword,
				'metaDescription'	=>	$request->meta_description
			]);
		}

    	dd(Property::where('slug' , $request->slug)->get());
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
