<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\ProductCategory;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
		$categories = ProductCategory::CategoriesSorted();

		return view('Admin.Categories.Index')->with("categories" , $categories);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {

    	$categories = ProductCategory::CategoriesSorted();

        return view('Admin.Categories.Create')->with("categories" , $categories);
    }

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param Request $request
	 * @return void
	 */
    public function store(Request $request)
    {
    	$request->validate([
    		'name'				=>	'required|min:4|max:30',
			'slug'				=>	'required|min:4|max:30|unique:product_categories',
			'meta-title'		=>	'required',
			'meta-keyword'		=>	'required',
			'meta-description'	=>	'required'
		]);
    	$cat = ProductCategory::create([
    		'name'				=>	$request->input('name'),
			'slug'				=>	$request->input('slug'),
			'parentId'			=>	$request->input('parentCat'),
			'metaTitle'			=>	$request->input('meta-title'),
			'metaKeyword'		=>	$request->input('meta-keyword'),
			'metaDescription'	=>	$request->input('meta-description')
		]);
    	return redirect()->route("admin.categories.index");
    }

	/**
	 * Display the specified resource.
	 *
	 * @param int $id
	 * @return void
	 */
    public function show($id)
    {
        //
    }

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param ProductCategory $category
	 * @return void
	 */
    public function edit(ProductCategory $category)
    {
		$categories = ProductCategory::CategoriesSorted();
    	return view("Admin.Categories.Edit")->with(["category" => $category , "categories" => $categories]);
    }

	/**
	 * Update the specified resource in storage.
	 *
	 * @param Request $request
	 * @param ProductCategory $category
	 * @return void
	 */
    public function update(Request $request, ProductCategory $category)
    {
    	$slugRol = 'required|min:4|max:30';
		($request->slug !== $category->slug)? $slugRol .='|unique:product_categories' : '';
		$request->validate([
			'name'				=>	'required|min:4|max:30',
			'slug'				=>	$slugRol,
			'meta-title'		=>	'required',
			'meta-keyword'		=>	'required',
			'meta-description'	=>	'required'
		]);

		function checkInChildes ($childes , int $id){
			if (!empty($childes)){
				foreach ($childes as $child){
					if ($child->parentId == $id){
						return false;
					}
					if ($child->Children->isNotEmpty()){
						checkInChildes($child->Children , $id);
					}
				}
			}
			return true;
		}

		$chi = checkInChildes($category->Children , $category->id);
		if ($request->input('parentCat') ==  $category->id  or $chi == false){

			return back();

		}
		$category->update([
			'name'				=>	$request->input('name'),
			'slug'				=>	$request->input('slug'),
			'parentId'			=>	$request->input('parentCat'),
			'metaTitle'			=>	$request->input('meta-title'),
			'metaKeyword'		=>	$request->input('meta-keyword'),
			'metaDescription'	=>	$request->input('meta-description')
		]);
		return redirect()->route('admin.categories.index');
    }

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param int $id
	 * @return void
	 */
    public function destroy($id)
    {

    	$cat = ProductCategory::find($id);

    	if ($cat->Children->isNotEmpty()){
    		return back()->with("errorDelete" , "دسته مورد نظر با دسته یا محصولاتی در ارتباط است. نمیتوانید حذف نمایید.");
		}
    	$cat->delete();
        return back();
    }
}
