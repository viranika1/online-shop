<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    public function User(){
        return $this->belongsTo("App\User",'userId');
    }
    public function Address(){

        return $this->belongsTo('App\Address','addressId');


    }
}
