<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Arr;

class ProductCategory extends Model
{
	protected $fillable = [
		'name' , 'slug' , 'parentId' , 'metaTitle' , 'metaKeyword' , 'metaDescription'
	];

	public function DiscountCodes(){
        return $this->belongsToMany('App\DiscountCode','discountCode_productCategory','categoryId');
    }
    public function Products(){
        return $this->hasMany('App\Product','categoryId');
    }
    public function Parent(){
        return $this->belongsTo('App\ProductCategory','parentId');
    }
    public function  Children(){
        return $this->hasMany(ProductCategory::class,'parentId' , "id" );
    }

	public static function CategoriesSorted()
	{
		$catsSorted = [];
		global $catsT;
		$catsT = self::all()->sortBy('parentId')->toArray();
		function givSetNull(){
			global $catsT;
			$cc = array_filter($catsT , function ($i){
				global $catsT;
				$pId = [];
				foreach ($catsT as $catT){
					if ($catT['parentId'] !== null){
						$pId[]=  $catT['parentId'];
					}
				}
				if (!empty($pId)){
					if ($i['parentId'] !== null){
						return !in_array($i['id'] , $pId);
					}else{
						return false;
					}

				}else{
					return false;
				}
			});
			foreach ($cc as $c){
				foreach ($catsT as $key => $it){
					if ($it['id'] == $c['parentId']){
						$catsT[$key]['childes'][] = $c;
					}
					if ($c['id'] == $it['id'] ){
						Arr::forget($catsT , $key);
					}
				}
			}
			return $cc;
		}
		function TreeCats(){
			global $catsT;
			$cc =  givSetNull();
			$qq = array_filter($cc , function ($x){
				return $x['parentId'] !== null;
			});
			if (!empty($qq)){
				TreeCats();
			}
			return $catsT;
		}
		$catsSorted = TreeCats();
		return $catsSorted;
    }
}
