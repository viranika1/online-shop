<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Festival extends Model
{

    public function Products(){
        return $this->hasMany('App\Product','festivalId');
    }
}
