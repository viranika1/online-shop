<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ticket extends Model
{
    protected $fillable=[
        'id',
        'title',
        'description',
        'parentId',
        'userId',
        'supportAdminID',
        'files',
    ];
}
