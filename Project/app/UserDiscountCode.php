<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserDiscountCode extends Model
{
    protected $fillable=[
        'id',
        'userId',
        'discountCodeId',
        'count'

    ];
}
