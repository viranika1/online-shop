<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DiscountCode extends Model
{
    public  function  Products(){
        return $this->belongsToMany('App\Product');
    }
    public function Categories(){
        return $this->belongsToMany('App\Category');
    }
    public function Users(){
        return $this->belongsToMany('App\User')->withPivot('count');
    }
    public function Orders(){
        return $this->hasMany('App\Order','discountCodeId');
    }

}
