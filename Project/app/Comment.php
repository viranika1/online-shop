<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    public function User(){
        return $this->belongsTo('App\User','userId');
    }

    public function Product(){
        return $this->belongsTo('App\Product','productId');
    }
    public function Parent(){
     return $this->belongsTo('App\Comment','parentId');
    }
    public function Children(){
        return $this->hasMany('App\Comment','parentId');
    }
}
